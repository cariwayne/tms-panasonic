<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmsEmpGroupRaw extends Model
{
    protected $table = 'tms_emp_group_raw';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'import_id', 'group_code', 'emp_no'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
