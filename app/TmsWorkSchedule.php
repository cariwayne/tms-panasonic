<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmsWorkSchedule extends Model
{
    protected $table = 'tms_workschedule';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'import_id', 'code', 'description', 'time_in', 'time_in_from', 'time_in_to', 
      'time_out', 'time_out_from', 'time_out_to', 'next_day'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
