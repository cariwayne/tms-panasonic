<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmsCardPower extends Model
{
    protected $table = 'tms_t_c_cardpower';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'import_id', 'card_no', 'door_no', 'controller_no', 
      'door_begin_zone', 'door_end_zone', 'card_door_zone', 'card_door_state',
      'report_state', 'remarks'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
