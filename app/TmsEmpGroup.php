<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmsEmpGroup extends Model
{
    protected $table = 'tms_emp_group';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'import_id', 'code', 'description'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
