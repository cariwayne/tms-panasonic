<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TmsEmails extends Model
{
    use SoftDeletes;
    protected $table = 'tms_emails';

    protected $fillable = [
      'id', 'emp_emails', 'emp_reports', 'emp_date',
    ];
}
