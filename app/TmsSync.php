<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmsSync extends Model
{
    //
    protected $table = 'tms_sync';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'reference', 'last_record'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
