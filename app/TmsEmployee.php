<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmsEmployee extends Model
{
    protected $table = 'tms_employee';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'import_id', 'emp_no', 'name', 'email', 'plant_id', 'department_id', 'group_id', 'attendance_group_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * Get the phone record associated with the user.
     */
    public function badgeCard()
    {
        return $this->hasMany('App\TmsEmpBadgeCard','emp_no','emp_no');
    }

    /**
     *
     */
    public function department()
    {
        return $this->hasOne('App\TmsEmpDepartment','id','department_id');
    }

    /**
     *
     */
    public function empgroup()
    {
        return $this->hasOne('App\TmsEmpGroup','id','group_id');
    }

    /**
     *
     */
    public function empAttendancegroup()
    {
        return $this->hasOne('App\TmsEmpAttendanceGroup','id','attendance_group_id');
    }
}
