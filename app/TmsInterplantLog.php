<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmsInterplantLog extends Model
{
    protected $table = 'tms_interplant_log';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'batch', 'controller_id', 'controller_ip', 'record_time', 'card_no', 'pass', 'sync_datetime', 'sync_request_ip'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
