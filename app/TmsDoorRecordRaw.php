<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmsDoorRecordRaw extends Model
{
    
    
    protected $table = 'tms_door_record_raw';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'import_id', 'card_no', 'door_no', 'controller_no', 'record_time', 
      'record_state', 'open_type', 'pass_flag', 
      'hand_value', 'lfeet_value', 'rfeet_value'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
