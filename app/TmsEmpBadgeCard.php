<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmsEmpBadgeCard extends Model
{
    protected $table = 'tms_emp_badge_card';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'import_id', 'emp_no', 'card_no'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * Get the post that owns the comment.
     */
    public function employee()
    {
        return $this->belongsTo('App\TmsEmployee','emp_no');
    }
}
