<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmsAccessControllerAttendanceGroup extends Model
{
    protected $table = 'tms_access_controller_attendance_group';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'controller_id', 'attendance_group_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
