<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmsAccessController extends Model
{
    protected $table = 'tms_access_controller';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'import_id', 'controller_no', 'controller_name', 'controller_ip', 'last_online'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function attendance_group(){
        return $this->belongsToMany('App\TmsEmpAttendanceGroup', 'tms_access_controller_attendance_group', 'controller_id', 'attendance_group_id')
    	->withTimestamps();
    }
}
