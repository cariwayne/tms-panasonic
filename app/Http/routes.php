<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/


Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::resource('/api/v1/getDataFromPi', 'Api\v1\ApiController@getDataFromPi');

    // Route::get('/api/v1/syncpi', 'ERP\Tms\SyncPiController@index');
    Route::post('/api/v1/syncpi/listen', 'ERP\Tms\SyncPiController@listen');
    Route::get('/api/v1/syncpi/getEmpData', 'ERP\Tms\SyncPiController@getEmpData');
    Route::get('/api/v1/syncpi/getEmpCardData', 'ERP\Tms\SyncPiController@getEmpCardData');
    Route::post('/api/v1/syncpi/syncDoorRecordData', 'ERP\Tms\SyncPiController@syncDoorRecordData');


//    Route::get('/', 'HomeController@index');
    Route::get('/', 'ERP\Tms\ImportDataController@index');
    Route::get('/home', 'HomeController@index');
    Route::get('/home/controller_status', 'HomeController@controllerStatus');

    Route::resource('/emp/employee', 'ERP\Emp\EmployeeController');

    Route::resource('/tms/accesscontroller', 'ERP\Tms\AccessController');
    Route::resource('/tms/workschedule', 'ERP\Tms\WorkScheduleController');
    //Route::post('/tms/sendemail/send', 'ERP\Tms\SendEmailController@send_email');
    Route::resource('/tms/sendemail', 'ERP\Tms\SendEmailController');
    Route::get('/tms/manageemail/delete', 'ERP\Tms\ManageEmailController@destroy');
    Route::resource('/tms/manageemail', 'ERP\Tms\ManageEmailController');

    Route::get('/tms/accesscontroller/{id}/updateEmpData',
        'ERP\Tms\AccessController@updateEmpData');
    Route::get('/tms/accesscontroller/{id}/updateEmpCardData',
        'ERP\Tms\AccessController@updateEmpCardData');

    Route::resource('/auth/user', 'UserController');
    Route::resource('/auth/role', 'RoleController');


    Route::get('/tms/importdata', 'ERP\Tms\ImportDataController@index');
    Route::get('/tms/importdata/process/accdb', 'ERP\Tms\ImportDataController@import_accdb');
    Route::get('/tms/importdata/process/hrms', 'ERP\Tms\ImportDataController@import_dbf');
    Route::get('/tms/importdata/process/ping', 'ERP\Tms\ImportDataController@ping_controller');
    Route::post('/tms/importdata/process/reset', 'ERP\Tms\ImportDataController@reset_sync');

    Route::get('/tms/report/esd', 'ERP\Tms\EsdReportController@index');
    Route::get('/tms/report/esd/export', 'ERP\Tms\EsdReportController@export');
    Route::get('/tms/report/esd/status', 'ERP\Tms\EsdReportController@testStatus');
    Route::get('/tms/report/esd/status/export', 'ERP\Tms\EsdReportController@exportTestStatus');
    Route::get('/tms/report/esd/status/export_mail', 'ERP\Tms\SendEmailController@Export_Mail');
    Route::get('/tms/report/esd/status/exporttomail', 'ERP\Tms\SendEmailController@ExportToMail');

    Route::get('/tms/report/hrms', 'ERP\Tms\HrmsReportController@index');
    Route::get('/tms/report/hrms/exportdat', 'ERP\Tms\HrmsReportController@exportdat');
    Route::get('/tms/report/hrms/exportxls', 'ERP\Tms\HrmsReportController@exportxls');
    Route::get('/tms/report/hrms/rawdata', 'ERP\Tms\HrmsReportController@rawdata');
    Route::get('/tms/report/hrms/rawdata/exportdat', 'ERP\Tms\HrmsReportController@rawdata_exportdat');
    Route::get('/tms/report/hrms/rawdata/exportxls', 'ERP\Tms\HrmsReportController@rawdata_exportxls');

    Route::get('/tms/report/hrms_old', 'ERP\Tms\HrmsReportControllerOld@index');
    Route::get('/tms/report/hrms_old/export', 'ERP\Tms\HrmsReportControllerOld@export');
    Route::get('/tms/report/hrms_old/rawdata', 'ERP\Tms\HrmsReportControllerOld@rawdata');
    Route::get('/tms/report/hrms_old/rawdata/export', 'ERP\Tms\HrmsReportControllerOld@rawdata_export');

    Route::get('/tms/report/graph/{graph}', 'ERP\Tms\GraphReportController@index');
    Route::get('/tms/report/graph/{graph}/ajax', 'ERP\Tms\GraphReportController@ajax');
    Route::get('/tms/report/interplant', 'ERP\Tms\InterplantReportController@index');
    Route::get('/tms/report/interplant/exportxls', 'ERP\Tms\InterplantReportController@exportxls');

});
