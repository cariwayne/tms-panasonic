<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class RoleController extends Controller
{
    //
    /**
       * Create a new controller instance.
       *
       * @return void
       */
      public function __construct(){
          $this->middleware('auth');
      }

      public function index(){
        $tbl_records = array();
        $lists = \App\Role::paginate(50);
        return view('Role_list', [
              'tbl_records'=>$lists
            ]);
      }

      /**
       * Show the form for creating a new resource.
       *
       * @return Response
       */
      public function create()
      {
          $permissions = \App\Permission::all();
          return view('Role_form', ['permissions'=>$permissions]);
      }

      /**
       * Store a newly created resource in storage.
       *
       * @return Response
       */
      public function store(Request $request)
      {
          //
          $validator = \Validator::make($request->all(), [
            'name' => 'required|max:255|unique:roles,name',
            'display_name' => 'required|max:255',
          ]);
          if ($validator->fails()) {
              return redirect('auth/role/create')
                          ->withErrors($validator)
                          ->withInput();
          }else{
            $role = new \App\Role();
            $role->name = $request->input('name');
            $role->display_name = $request->input('display_name');
            $role->description = $request->input('description');
            $role->save();

            $role->perms()->sync([]);
            foreach($request->input('perms') as $perms_id){
              $role->attachPermission($perms_id);
            }
            if($role->save()){
                return \Redirect::to('auth/role');
            }

          }

      }

      /**
       * Display the specified resource.
       *
       * @param  int  $id
       * @return Response
       */
      public function show($id)
      {
          //
      }

      /**
       * Show the form for editing the specified resource.
       *
       * @param  int  $id
       * @return Response
       */
      public function edit($id)
      {
          //
          $post = \App\Role::find($id);
          $permissions = \App\Permission::all();
          return view('role_form', ['post'=>$post, 'permissions'=>$permissions]);
      }

      /**
       * Update the specified resource in storage.
       *
       * @param  int  $id
       * @return Response
       */
      public function update($id, Request $request)
      {
          //
          $validator = \Validator::make($request->all(), [
            'name' => 'required|max:255|unique:roles,name,'.$id,
            'display_name' => 'required|max:255',
          ]);
          if ($validator->fails()) {
              return redirect('auth/role/'.$id.'/edit')
                          ->withErrors($validator)
                          ->withInput();
          }else{
            $role = \App\Role::find($id);
            $role->name = $request->input('name');
            $role->display_name = $request->input('display_name');
            $role->description = $request->input('description');

            $role->perms()->sync([]);
            foreach($request->input('perms') as $perms_id){
              $role->attachPermission($perms_id);
            }
            if($role->save()){
                return \Redirect::to('auth/role');
            }

          }
      }

      /**
       * Remove the specified resource from storage.
       *
       * @param  int  $id
       * @return Response
       */
      public function destroy($id)
      {
          //
          $role = \App\Role::find($id);

          $role->users()->sync([]);
          $role->perms()->sync([]);
          $role->forceDelete();
          return \Redirect::to('auth/role');
      }

}
