<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Libraries\XBaseTable;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function controllerStatus(){
        $controllers = \App\TmsAccessController::all();
        return response()->json([
            'controllers' => $controllers
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      // Read Shift
      /*
      $table = new XBaseTable('C:/Users/cw/Desktop/20160323/TMSCHTYP.DBF');
      $table->open();
      $sql = 'INSERT INTO tms_workschedule (`code`, `description`,
      `time_in`, `time_in_from`, `time_in_to`, `time_out`, `time_out_from`, `time_out_to`, `next_day`) VALUES ';
      $sql_val = array();
      $i = 0;
      while ($record=$table->nextRecord()) {
        $sql_val[$i] = "(";
        $sql_val[$i] .= "'".$record->getStringByName('CODE')."', ";
        $sql_val[$i] .= "'".$record->getStringByName('DESC')."', ";
        $sql_val[$i] .= "'".$record->getStringByName('TIME_FR')."', ";
        $sql_val[$i] .= "'".$record->getStringByName('TIME_FRIN')."', ";
        $sql_val[$i] .= "'".$record->getStringByName('TIME_TOIN')."', ";
        $sql_val[$i] .= "'".$record->getStringByName('TIME_FROUT')."', ";
        $sql_val[$i] .= "'".$record->getStringByName('TIME_FROUT')."', ";
        $sql_val[$i] .= "'".$record->getStringByName('TIME_TOOUT')."', ";
        if($record->getStringByName('NEXTDAY') == 'Y'){
          $sql_val[$i] .= "'1'";
        }else{
          $sql_val[$i] .= "'0'";
        }
        $sql_val[$i] .= ")";
        $i++;

      }
      $sql = $sql.implode(',',$sql_val);
      // echo $sql;
      $table->close();
      return view('home');*/

      $lists = \App\TmsAccessController::all();
      return view('home', ['controllers'=>$lists]);
    }
}
