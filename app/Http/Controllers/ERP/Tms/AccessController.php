<?php

namespace App\Http\Controllers\ERP\Tms;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\TooManyRedirectsException;

class AccessController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $tbl_records = array();
        $lists = \App\TmsAccessController::with('attendance_group')->paginate(50);
        return view('erp.tms.accesscontroller_list', [
              'tbl_records'=>$lists
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $attendancegroup = \App\TmsEmpAttendanceGroup::all();
        return view('erp.tms.accesscontroller_form', ['attendancegroup'=>$attendancegroup]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $post = $request->all();
        $validator = \Validator::make($post['controller'], [
            'controller_no' => 'required',
            'controller_name' => 'required',
            'controller_ip' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('tms/accesscontroller/create')
                        ->withErrors($validator)
                        ->withInput();
        }else{

            $update = \App\TmsAccessController::create($post['controller']);
            $attn = (array)$request->input('attn');
            $update->attendance_group()->sync($attn);

            return \Redirect::to('tms/accesscontroller');


        }
    }

    public function updateEmpData($id, Request $request){
        $controller = \App\TmsAccessController::find($id);

        $return = '';
        try {

            $empData = app('App\Http\Controllers\ERP\Tms\SyncPiController')->getEmpData($request);
            $data = $empData->content();

            $client = new Client();
            $response = $client->post(
                $controller->controller_ip.'/pitms/public/api/v1/updateEmpData',
                ['json' => json_decode($data), 'timeout'=>5]
            );
            $return = (string)$response->getBody();
			$return = $controller->controller_ip.' Push Emp Data Completed'; 

        }catch(RequestException $e){
            $return .= "\n=============";
            $return .= "\nRequestException";
            $return .= "\n=============\n";
            $return .=  Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                $return .= "\n=============";
                $return .= "\nResponse";
                $return .= "\n=============\n";
                $return .=  Psr7\str($e->getResponse());
            }
        }catch(ConnectException $e){
            $return .= "\n=============";
            $return .= "\nConnectException";
            $return .= "\n=============\n";
            $return .= Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                $return .= "\n=============";
                $return .= "\nResponse";
                $return .= "\n=============\n";
                $return .=  Psr7\str($e->getResponse());
            }
        }catch(ClientException $e){
            $return .= "\n=============";
            $return .= "\nClientException";
            $return .= "\n=============\n";
            $return .=  Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                $return .= "\n=============";
                $return .= "\nResponse";
                $return .= "\n=============\n";
                $return .=  Psr7\str($e->getResponse());
            }
        }catch(ServerException $e){
            $return .= "\n=============";
            $return .= "\nServerException";
            $return .= "\n=============\n";
            $return .=  Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                $return .= "\n=============";
                $return .= "\nResponse";
                $return .= "\n=============\n";
                $return .=  Psr7\str($e->getResponse());
            }
        }catch(TooManyRedirectsException $e){
            $return .= "\n=============";
            $return .= "\nTooManyRedirectsException";
            $return .= "\n=============\n";
            $return .=  Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                $return .= "\n=============\n";
                $return .= "\nResponse\n";
                $return .= "\n=============\n";
                $return .=  Psr7\str($e->getResponse());
            }
        }
        return response()->json(['return'=> $return]);
    }


    public function updateEmpCardData($id, Request $request){
        $controller = \App\TmsAccessController::find($id);

        $return = '';
        try {

            $empCardData = app('App\Http\Controllers\ERP\Tms\SyncPiController')->getEmpCardData($request);
            $data = $empCardData->content();

            $client = new Client();
            $response = $client->post(
                $controller->controller_ip.'/pitms/public/api/v1/updateEmpCardData',
                ['json' => json_decode($data), 'timeout'=>5]
            );
            $return = (string)$response->getBody();
			$return = $controller->controller_ip.' Push Card Data Completed'; 

        }catch(RequestException $e){
            $return .= "\n=============";
            $return .= "\nRequestException";
            $return .= "\n=============\n";
            $return .=  Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                $return .= "\n=============";
                $return .= "\nResponse";
                $return .= "\n=============\n";
                $return .=  Psr7\str($e->getResponse());
            }
        }catch(ConnectException $e){
            $return .= "\n=============";
            $return .= "\nConnectException";
            $return .= "\n=============\n";
            $return .= Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                $return .= "\n=============";
                $return .= "\nResponse";
                $return .= "\n=============\n";
                $return .=  Psr7\str($e->getResponse());
            }
        }catch(ClientException $e){
            $return .= "\n=============";
            $return .= "\nClientException";
            $return .= "\n=============\n";
            $return .=  Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                $return .= "\n=============";
                $return .= "\nResponse";
                $return .= "\n=============\n";
                $return .=  Psr7\str($e->getResponse());
            }
        }catch(ServerException $e){
            $return .= "\n=============";
            $return .= "\nServerException";
            $return .= "\n=============\n";
            $return .=  Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                $return .= "\n=============";
                $return .= "\nResponse";
                $return .= "\n=============\n";
                $return .=  Psr7\str($e->getResponse());
            }
        }catch(TooManyRedirectsException $e){
            $return .= "\n=============";
            $return .= "\nTooManyRedirectsException";
            $return .= "\n=============\n";
            $return .=  Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                $return .= "\n=============\n";
                $return .= "\nResponse\n";
                $return .= "\n=============\n";
                $return .=  Psr7\str($e->getResponse());
            }
        }
        return response()->json(['return'=> $return]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id, Request $request)
    {
        //

        $controller = \App\TmsAccessController::find($id);
        /*
        $return = '';
        try {
            $empData = app('App\Http\Controllers\ERP\Tms\SyncPiController')->getEmpData($request);
            $data = $empData->content();

            $client = new Client();
            $response = $client->post(
                $controller->controller_ip.'/pitms/public/api/v1/updateEmpData',
                ['json' => json_decode($data), 'timeout'=>5]
            );
            $return = (string)$response->getBody();


            $empCardData = app('App\Http\Controllers\ERP\Tms\SyncPiController')->getEmpCardData($request);
            $data = $empCardData->content();

            $client = new Client();
            $response = $client->post(
                $controller->controller_ip.'/pitms/public/api/v1/updateEmpCardData',
                ['json' => json_decode($data), 'timeout'=>5]
            );
            $return = (string)$response->getBody();

        }catch(\GuzzleHttp\Exception\ConnectException $e){
            $return =  Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                $return .= "\n=============";
                $return .= "\nResponse\n";
                $return .= "\n=============";
                $return .=  Psr7\str($e->getResponse());
            }
        }*/

        return view('erp.tms.accesscontroller_show',['controller'=>$controller]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $post = ['id'=>$id, 'attn'=>[] ];
        $post['controller'] = \App\TmsAccessController::find($id);
        $attn = \App\TmsAccessController::find($id);
        foreach($attn->attendance_group as $attn){
            $attn_group_id = $attn->pivot->attendance_group_id;
            $post['attn'][$attn_group_id] = $attn_group_id;
        }
        $attendancegroup = \App\TmsEmpAttendanceGroup::all();

        return view('erp.tms.accesscontroller_form', [
            'post'=>$post,
            'attendancegroup'=>$attendancegroup]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        //
        $post = $request->all();
        $validator = \Validator::make($post['controller'], [
            'controller_no' => 'required',
            'controller_name' => 'required',
            'controller_ip' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('tms/accesscontroller/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $update = \App\TmsAccessController::find($id);
            $update->update($post['controller']);
            $attn = (array)$request->input('attn');
            $update->attendance_group()->sync($attn);

            return \Redirect::to('tms/accesscontroller');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $update = \App\TmsAccessController::find($id);
        $update->attendance_group()->detach();
        $update->delete();

        return \Redirect::to('tms/accesscontroller');
    }


}
