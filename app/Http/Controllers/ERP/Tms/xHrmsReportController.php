<?php

namespace App\Http\Controllers\ERP\Tms;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Carbon\Carbon;

class HrmsReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
      $this->middleware('auth');
    }
    
    public function export(){
      $results = $this->index(true);
      return view('erp.tms.export_dat', [
          'tbl_records' => $results->get()
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($export=false){ 
      $request = \Request::all(); //<-- we use global request to get the param of URI
      
      $tbl_records = array();
      if(isset($request['date'])){
        list($start_date, $end_date) = explode(' - ', $request['date']);
        
        $query = DB::table('tms_door_record_raw AS dr')
          ->leftJoin('tms_emp_badge_card AS c', 'dr.card_no', '=', 'c.card_no')
          ->leftJoin('tms_employee AS e', 'c.emp_no', '=', 'e.emp_no')
          ->leftJoin('tms_emp_group AS g', 'e.group_id', '=', 'g.id')
          ->leftJoin('tms_emp_workschedule AS ew', function($join){
            $join->on('ew.workschedule_date', '=', DB::raw('DATE_FORMAT(dr.record_time, "%Y-%m-%d")'));
            $join->on(DB::raw(
              '(ew.reference_no = c.emp_no AND ew.reference_type="emp_no")'), DB::raw(''), DB::raw(''));
          })
          ->leftJoin('tms_workschedule AS w', 'ew.workschedule_id', '=', 'w.id')
          ->leftJoin('tms_emp_workschedule AS ew2', function($join){
            $join->on('ew2.workschedule_date', '=', DB::raw('DATE_FORMAT(dr.record_time, "%Y-%m-%d")'));
            $join->on(DB::raw(
              '(ew2.reference_no = g.code AND ew2.reference_type="group_code")'), DB::raw(''), DB::raw(''));
          })
          ->leftJoin('tms_workschedule AS w2', 'ew2.workschedule_id', '=', 'w2.id')
          
          ->select('dr.*', 'e.name as employee_name', 'e.emp_no', 
            DB::raw('IF(w.code IS NOT NULL, w.code, w2.code) as shift_code'), 
            DB::raw('IF(w.code IS NOT NULL, w.time_in_from,w2.time_in_from) as time_in_from'), 
            DB::raw('MIN(dr.record_time) as early_in'), DB::raw('MAX(dr.record_time) as last_out'))
          ->whereBetween('dr.record_time',[
            Carbon::createFromFormat('d/m/Y H:i', $start_date)->format('Y-m-d H:i:s'),
            Carbon::createFromFormat('d/m/Y H:i', $end_date)->format('Y-m-d H:i:s')
          ]);

          

          if($request['card_no'] != ''){
            $query->where('dr.card_no', $request['card_no']);
          }
          if($request['emp_no'] != ''){
            $query->where('e.emp_no', $request['emp_no']);
          }
          $query->groupBy('e.emp_no');
          $query->groupBy(DB::raw('CASE 
          WHEN (w.next_day=1 OR w2.next_day=1)
            THEN DATE_FORMAT(dr.record_time,"%Y-%m-%d")
          ELSE
            DATE_FORMAT(dr.record_time,"%Y-%m-%d") 
          END'));
          $query->orderBy('dr.record_time', 'ASC');
          
          $query2 = clone $query;

          // IN - emp work schedule, group schedule, no shift pattern
          $query->where('dr.record_state',1);
          $query->whereRaw( 
          '(
            (
            
              /*  Employee shift pattern  */ 

              w.code IS NOT NULL AND 
              (
                (
                  w.next_day = 1 AND
                  dr.record_time >= DATE_FORMAT(CONCAT(DATE_FORMAT(dr.record_time,"%Y-%m-%d")," ", REPLACE( FORMAT(w.time_in_from,2), ".",":")), "%Y-%m-%d %H:%i:%s")
                ) OR (
                  w.next_day = 0 AND
                  dr.record_time >= DATE_FORMAT(CONCAT(DATE_FORMAT(dr.record_time,"%Y-%m-%d")," ", REPLACE( FORMAT(w.time_in_from,2), ".",":")), "%Y-%m-%d %H:%i:%s")
                )

              )

            ) OR (
            
              /*  Employee group shift pattern  */ 

              w.code IS NULL AND dr.record_time >= DATE_FORMAT(CONCAT(DATE_FORMAT(dr.record_time,"%Y-%m-%d")," ", REPLACE( FORMAT(w2.time_in_from,2), ".",":")), "%Y-%m-%d %H:%i:%s")
            ) OR ( 

              /*  No shift pattern  */ 

              w.code IS NULL AND w2.code IS NULL
            )
          )');

          // OUT - emp work schedule, group schedule, no shift pattern
          $query2->where('dr.record_state',2);
          $query2->whereRaw( 
          '(
            (
              w.code IS NOT NULL AND w.next_day = 1 AND dr.record_time <= DATE_FORMAT(CONCAT(DATE_FORMAT(dr.record_time,"%Y-%m-%d")," ", REPLACE( FORMAT(w.time_out_to,2), ".",":")), "%Y-%m-%d %H:%i:%s")
            ) OR (
              w.code IS NOT NULL AND w.next_day = 0 
            ) OR (
              w.code IS NULL AND dr.record_time <= DATE_FORMAT(CONCAT(DATE_FORMAT(dr.record_time,"%Y-%m-%d")," ", REPLACE( FORMAT(w2.time_out_to,2), ".",":")), "%Y-%m-%d %H:%i:%s")
            ) OR (
              w.code IS NULL AND w2.next_day = 0 
            ) OR ( 
              w.code IS NULL AND w2.code IS NULL
            )
          )'); 

          $query3 = DB::table(DB::raw("( 
           SELECT * FROM ({$query->toSql()}) a
           UNION 
           SELECT * FROM ({$query2->toSql()}) b
         ) as A"))
           ->mergeBindings($query)
           ->mergeBindings($query2);
        
          $query3->orderby('record_time', 'ASC');
          if($export == true){
            return $query3;
          }
          $tbl_records = $query3->paginate(50);
      }
   
      return view('erp.tms.hrms_report', [
          'request' => $request,
          'tbl_records' => $tbl_records
        ]);
    }
}
