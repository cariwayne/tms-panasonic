<?php

namespace App\Http\Controllers\ERP\Tms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\TmsEmails;
use App\TmsEmployee;
use \Session;

class ManageEmailController extends Controller
{
   /**
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct()
   {
       $this->middleware('auth');
   }

   /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */

    /** Laravel resource functions **/

    #GET -> /entity
   public function index(Request $request)
   {
      $view_data = array();
      $id = $request->input('tms_email');
      $view_data['tms_emails'] = TmsEmails::find($id);
      
      $view_data['reporttype'] = array(
        1 => 'ESD test log',
        2 => 'EMP test status'
      );

      $view_data['line'] = DB::table('tms_employee')->select('job')->distinct('job')->where('job', 'like', '%line%')->lists('job', 'job');

      $view_data['department'] = DB::table('tms_emp_department')->select('code')->distinct('code')->leftjoin('tms_employee', 'department_id', '=', 'tms_emp_department.id')->lists('code', 'code');

      $view_data['day'] = array(
        7 => "Sunday",
        1 => "Monday",
        2 => "Tuesday",
        3 => "Wednesday",
        4 => "Thursday",
        5 => "Friday",
        6 => "Saturday"
      );
      
      $users = TmsEmails::paginate('5');
      $view_data['users'] = $users;

      if($request->input('search_email') != null){
        $search_email = $request->input('search_email');
        $view_data['db_search_email'] = TmsEmails::where('emp_emails', 'LIKE', $search_email)->get();
      }else{
        $view_data['db_search_email'] = TmsEmails::where('emp_emails', '=', null)->get();
      }

      return view('erp.tms.manage_email', $view_data);
   }

    #GET -> /entity/create
    public function create()
   {
       return view('events/index');
   }

    #POST -> /entity
    public function store(Request $request)
    {
      $tms_email = new TmsEmails;
      $id = $request->input('id');
      if($id){
        $tms_email = TmsEmails::find($id);
      }

      $email = $request->input('email');
      $result = filter_var( $email, FILTER_VALIDATE_EMAIL );

      if($result == false){
        Session::flash('message_error', "Please enter the correct e-mail address.");
        return redirect('tms/manageemail');
      }else{
        if(isset($email)){
          $tms_email_check = TmsEmails::where('emp_emails', $email)->first();
          if(!$id){
            if($tms_email_check){
              Session::flash('message_error1', "The e-mail existed.");
              return redirect('tms/manageemail');
            }
          }
        }
      }

      $reporttype = $request->input('reporttype');
      $day = $request->input('day');
      $time = $request->input('time');
      $line = $request->input('line');
      $department = $request->input('department');
      $tms_email->emp_emails = $email;
      $tms_email->emp_reports = $reporttype;
      $tms_email->emp_line = $line;
      $tms_email->emp_department = $department;
      $tms_email->emp_days = implode(",", $day);
      $tms_email->emp_times = $time;
      $tms_email->save();
      Session::flash('message_success', "The e-mail has been saved.");
      return redirect('tms/manageemail');
    }

    #GET -> /entity/{vars}
    public function show()
   {
       return view('events/index');
   }

    #GET -> /entity/{single-var}/edit
    public function edit()
   {
       return view('events/index');
   }

    #PUT/PATCH -> /entity/{vars}
    public function update()
   {
       return view('events/index');
   }

    #DELETE -> /entity/{vars}
    public function destroy(Request $request)
    {
      $tms_del_id = $request->input('tms_del_id');
      if($tms_del_id){
        $tms_emails = TmsEmails::find($tms_del_id);
        $tms_emails->delete();
      }
       return redirect('/tms/manageemail');
    }

    /** Laravel resource functions **/

    /** Ajax functions **/

    /** Ajax functions **/

    /** Controller functions **/

    /** Controller functions **/

    /** Static functions **/
    /** Static functions **/

}


/*

{


   public function manage_email(Request $request){
  dd($request->all());
                        foreach ($em_query as $em) {
                            $insert_temp_emails[] = [
                                'emails' => $em->emails,
                                'reporttype' => $em->reporttype,
                                'frequency' => $em->frequency
                            ];

                        }

                $start_date->addDay();
                foreach(array_chunk($insert_temp_query, 1000) as $t){
                    DB::table('temp_tms_door_record_raw')->insert($t);
                }
                $insert_temp_query = array();

  }
}

*/