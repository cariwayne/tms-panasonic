<?php

namespace App\Http\Controllers\ERP\Tms;

use App\Http\Controllers\Controller;

use DB, Request;
use Carbon\Carbon;

class HrmsReportController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function exportdat()
    {
		$request = Request::all();
        $results = $this->index('dat');

        if(config('app.hrms_report_export_format') == '0'){
            $view = 'erp.tms.export_hrms_escatec_pg_dat';
        }else if(config('app.hrms_report_export_format') == '1'){
            $view = 'erp.tms.export_hrms_escatec_jb_txt';
        }

        return view($view, [
          'tbl_records' => $results,
		      'title' => 'HRMS Report '.$request['date']
        ]);
    }

    public function exportxls()
    {
		$request = Request::all();
        $results = $this->index('xls');
        return view('erp.tms.hrms_export_xls', [
          'tbl_records' => $results,
		      'title' => 'HRMS Report '.$request['date']
        ]);
    }


    public function rawdata()
    {
        $results = $this->index(false, true);
        $results['form_url'] = url('tms/report/hrms/rawdata');
        $results['export_url'] = url('tms/report/hrms/rawdata/');
        $results['page_title'] = 'HRMS Raw Data';

        $plant = \App\TmsEmpPlant::pluck('code','id');
        $results['plant'] = $plant;

        return view('erp.tms.hrms_report', $results);
    }

    public function rawdata_exportxls()
    {	
		$request = Request::all();
        $results = $this->index(true, true);
        return view('erp.tms.hrms_export_xls', [
          'tbl_records' => $results['tbl_records'],
		      'title' => 'HRMS Raw Data '.$request['date']
        ]);
    }

    public function rawdata_exportdat()
    {
		$request = Request::all();
        $results = $this->index(true, true);

        if(config('app.hrms_report_export_format') == '0'){
            $view = 'erp.tms.export_hrms_escatec_pg_dat';
        }else if(config('app.hrms_report_export_format') == '1'){
            $view = 'erp.tms.export_hrms_escatec_jb_txt';
        }

        return view($view, [
          'tbl_records' => $results['tbl_records'],
		      'title' => 'HRMS Raw Data '.$request['date']
        ]);
    }


    public function createtemporarytable(){
        $sql = '
        DROP TABLE IF EXISTS `temp_tms_door_record_raw`;';
        DB::insert(DB::raw($sql));

        $sql = 'CREATE TABLE `temp_tms_door_record_raw` (
         `door_record_raw_id` int(10) unsigned NOT NULL,
         `card_no` varchar(255) COLLATE utf8_general_ci NULL,
         `emp_no` varchar(255) COLLATE utf8_general_ci NULL,
         `employee_name` varchar(255) COLLATE utf8_general_ci NULL,
         `attendance_group_code` varchar(255) COLLATE utf8_general_ci NULL,
         `shift_code` varchar(255) COLLATE utf8_general_ci NULL,
         `door_no` varchar(255) COLLATE utf8_general_ci NULL,
         `department_code` varchar(255) COLLATE utf8_general_ci NULL,
         `controller_name` varchar(255) COLLATE utf8_general_ci NULL,
         `record_time` datetime NULL,
         `record_state` int(11) NULL,
         `open_type` int(11) NULL,
         `pass_flag` int(11) NULL,
         `hand_value` int(11) NULL,
         `lfeet_value` int(11) NULL,
         `rfeet_value` int(11) NULL,
         `exact_record_time` datetime NULL,
         KEY `door_record_raw_id` (`door_record_raw_id`),
         KEY `card_no` (`card_no`),
         KEY `emp_no` (`emp_no`)
        ) ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_general_ci';
        DB::insert(DB::raw($sql));
    }


    public function index($export=false, $rawdata = false){
        //<-- we use global request to get the param of URI
        $request = Request::all();

        $tbl_records = array();
        $emps = array();
        $attn_grp_controller = array();
        $use_attn_grp_controller = false;
        if(isset($request['attendance_group'])){
            $use_attn_grp_controller = true;
        }

        $this->createtemporarytable();

        if(isset($request['date'])){
            list($start_date, $end_date) = explode(' - ', $request['date']);

            $start_date = Carbon::createFromFormat('d/m/Y H:i', $start_date);
            $end_date = Carbon::createFromFormat('d/m/Y H:i', $end_date);

            $i = 0;
            $insert_temp_query = array();
            while($start_date<$end_date){

                $query = DB::table('tms_emp_badge_card AS c')
                ->leftJoin('tms_door_record_raw AS dr', 'c.emp_no', '=', 'dr.emp_no')
                ->leftJoin('tms_employee AS e', 'dr.emp_no', '=', 'e.emp_no')
                ->leftJoin('tms_emp_attendance_group AS ag', 'e.attendance_group_id', '=', 'ag.id')
                ->leftJoin('tms_emp_group AS g', 'e.group_id', '=', 'g.id')
                ->leftJoin('tms_emp_department AS d', 'e.department_id', '=', 'd.id')

                ->leftJoin('tms_emp_workschedule AS ewt',
                    function($join) use ($start_date) {
                        $join->on(
                            'ewt.workschedule_date', '=',
                            DB::raw('"'.$start_date->format('Y-m-d').'"')
                        );
                        $join->on(
                            DB::raw(
                            '(ewt.reference_no = c.emp_no AND ewt.reference_type="emp_no")'),
                            DB::raw(''),
                            DB::raw('')
                        );
                    }
                )
                ->leftJoin('tms_workschedule AS ew', 'ewt.workschedule_id', '=', 'ew.id')

                ->leftJoin('tms_emp_workschedule AS egwt',
                    function($join) use ($start_date) {
                        $join->on(
                            'egwt.workschedule_date', '=',
                            DB::raw('"'.$start_date->format('Y-m-d').'"')
                        );
                        $join->on(
                            DB::raw(
                            '(egwt.reference_no = g.code AND egwt.reference_type="group_code")'),
                            DB::raw(''),
                            DB::raw('')
                        );
                    })
                ->leftJoin('tms_workschedule AS egw', 'egwt.workschedule_id', '=', 'egw.id')

                ->select('c.*', 'e.name as employee_name', 'e.emp_no',
                  'e.attendance_group_id',  'ag.code as attendance_group_code',
                  'd.code as department_code',
                  DB::raw('IF(ew.code IS NOT NULL, ew.code, egw.code) as shift_code'),
                  DB::raw('IF(ew.code IS NOT NULL, ew.time_in_from, egw.time_in_from) as time_in_from'),
                  DB::raw('IF(ew.code IS NOT NULL, ew.time_out_to, egw.time_out_to) as time_out_to'),
                  DB::raw('IF(ew.code IS NOT NULL, ew.next_day, egw.next_day) as next_day')
                );

                if (Request::get('card_no') != '') {
                    $query->where('c.card_no', Request::get('card_no'));
                }
                if (Request::get('plant_id') != '') {
                    $query->where('e.plant_id', $request['plant_id']);
                }
                if (Request::get('emp_no') != '') {
                    $query->where('e.emp_no', $request['emp_no']);
                }
                if (Request::get('department') != '') {
                    $query->where('d.code', Request::get('department'));
                }

                $start_time = clone $start_date;
                $end_time = clone $start_date;
                $start_time->subDay();
                $query->whereBetween('dr.record_time', [
                $start_time->format('Y-m-d 00:00:00'),
                $end_time->format('Y-m-d 23:59:59')]);

                $query->groupBy('c.card_no');
                $query->orderBy('c.emp_no', 'ASC');

                $emps = $query->get();

                foreach ($emps as $emp) {
                    if(!isset($attn_grp_controller[$emp->attendance_group_id])){
                        $attn_grp_controller[$emp->attendance_group_id] = \App\TmsAccessControllerAttendanceGroup::where('attendance_group_id', $emp->attendance_group_id)->pluck('controller_id')->all();
                    }

                    $start_time = clone $start_date;
                    $end_time = clone $start_date;

                    if ($emp->next_day == 1) {
                        // subtract 1 day
                        if(config('app.night_shift_startday') == -1){
                            $start_time->subDay();
                        }else{
                            $end_time->addDay();
                        }
                    }
                    if ($emp->time_in_from == '') {
                        $emp->time_in_from = '00:00:00';
                    }
                    if ($emp->time_out_to == '' || $emp->time_out_to == '00:00:00') {
                        $emp->time_out_to = '23:59:59';
                    }

                    $time_in_from = explode(':',$emp->time_in_from);
                    $time_out_to = explode(':',$emp->time_out_to);

                    $start_time
                        ->hour($time_in_from[0])
                        ->minute($time_in_from[1])
                        ->second(0);
                    $end_time
                        ->hour($time_out_to[0])
                        ->minute($time_out_to[1])
                        ->second(0);

                    $dr_in_query = DB::table('tms_door_record_raw AS dr')
                      ->leftJoin('tms_access_controller AS ac', 'ac.controller_no', '=', 'dr.controller_no')
                      ->where('dr.card_no', $emp->card_no)
                      ->whereBetween('dr.record_time',[
                        $start_time->format('Y-m-d H:i:s'),
                        $end_time->format('Y-m-d H:i:s')
                      ])
                      ->orderBy('dr.record_time');

                    // attendance group
                    $dr_in_query->leftJoin('tms_access_controller_attendance_group AS ag', 'ac.id', '=', 'ag.controller_id');

                    if($use_attn_grp_controller){
                        if(count($attn_grp_controller[$emp->attendance_group_id]) > 0){
                            $dr_in_query->whereIn('ac.id', $attn_grp_controller[$emp->attendance_group_id]);
                        }else{
                            $dr_in_query->where('ac.id','=','');
                        }

                    }


                    $dr_out_query = clone $dr_in_query;

                    if ($rawdata) {
                        $dr_in_query->select(
                            'dr.*',
                            DB::raw('dr.record_time as exact_record_time')
                        );
                    } else {
                        $dr_in_query->where('dr.record_state', 1)
                        ->select(
                            'dr.*',
                            DB::raw('MIN(dr.record_time) as exact_record_time')
                        );
                    }

                    $dr_out_query->where('dr.record_state', 2)
                    ->select(
                        'dr.*',
                        DB::raw('MAX(dr.record_time) as exact_record_time')
                    );

                    if ($rawdata) {
                        /*$dr_query = DB::insert(DB::raw("INSERT INTO `temp_tms_door_record_raw`
                        SELECT * FROM (
                           SELECT * FROM ({$dr_in_query->toSql()}) a
                        ) as A"))
                        ->mergeBindings($dr_in_query);*/
                        $sql = "SELECT a.*, ac.controller_name FROM ({$dr_in_query->toSql()}) a
                            LEFT JOIN tms_door_record_raw AS dr
                                ON a.exact_record_time = dr.record_time
                                AND a.card_no = dr.card_no
                            LEFT JOIN tms_access_controller AS ac
                                ON ac.controller_no = dr.controller_no
                            WHERE a.exact_record_time IS NOT NULL";
                        $binding = $dr_in_query->getBindings();
                        if($use_attn_grp_controller){
                            if(count($attn_grp_controller[$emp->attendance_group_id]) > 0){
                                $inQuery = implode(',', array_fill(0, count($attn_grp_controller[$emp->attendance_group_id]), '?'));
                                $sql .= " AND ac.id IN(".$inQuery.")";
                                $binding = array_merge($binding, $attn_grp_controller[$emp->attendance_group_id]);
                            }else{
                                $sql .= " AND ac.id = '' ";
                            }
                        }
                        $dr_query = DB::select(DB::raw($sql), $binding);

                        if(isset($request['debug']) && (Request::get('emp_no') != '' || Request::get('card_no') != '')){
                            dump($binding);
                        }
                        foreach ($dr_query as $dr) {
                           $insert_temp_query[] = [
                                'card_no' => $emp->card_no,
                                'emp_no' => $emp->emp_no,
                                'employee_name' => $emp->employee_name,
                                'attendance_group_code' => $emp->attendance_group_code,
                                'shift_code' => $emp->shift_code,
                                'door_no' => $dr->door_no,
                                'department_code' => $emp->department_code,
                                'controller_name'=>$dr->controller_name,
                                'record_time'=>$dr->exact_record_time,
                                'record_state'=>$dr->record_state,
                                'open_type'=>$dr->open_type,
                                'pass_flag'=>$dr->pass_flag,
                                'hand_value'=>$dr->hand_value,
                                'lfeet_value'=>$dr->lfeet_value,
                                'rfeet_value'=>$dr->rfeet_value,
                                'exact_record_time'=>$dr->exact_record_time,
                            ];
                        }

                    } else {
                        $sql = "SELECT a.*, ac.controller_name FROM ({$dr_in_query->toSql()}) a
                            LEFT JOIN tms_door_record_raw AS dr
                                ON a.exact_record_time = dr.record_time
                                AND a.card_no = dr.card_no
                            LEFT JOIN tms_access_controller AS ac
                                ON ac.controller_no = dr.controller_no
                            WHERE a.exact_record_time IS NOT NULL";
                        $binding = $dr_in_query->getBindings();
                        if($use_attn_grp_controller){
                            if(count($attn_grp_controller[$emp->attendance_group_id]) > 0){
                                $inQuery = implode(',', array_fill(0, count($attn_grp_controller[$emp->attendance_group_id]), '?'));
                                $sql .= " AND ac.id IN(".$inQuery.")";
                                $binding = array_merge($binding, $attn_grp_controller[$emp->attendance_group_id]);
                            }else{
                                $sql .= " AND ac.id = '' ";
                            }
                        }
                        $dr_query = DB::select(DB::raw($sql), $binding);

                        if(isset($request['debug']) && (Request::get('emp_no') != '' || Request::get('card_no') != '')){
                            dump($binding);
                        }

                        foreach ($dr_query as $dr) {
                            /*if($emp->emp_no == '01461'){
                              print_r($dr->exact_record_time);
                              echo "<br>";
                            }*/
                            $insert_temp_query[] = [
                                'card_no' => $emp->card_no,
                                'emp_no' => $emp->emp_no,
                                'employee_name' => $emp->employee_name,
                                'attendance_group_code' => $emp->attendance_group_code,
                                'shift_code' => $emp->shift_code,
                                'door_no' => $dr->door_no,
                                'department_code' => $emp->department_code,
                                'controller_name'=>$dr->controller_name,
                                'record_time'=>$dr->exact_record_time,
                                'record_state'=>$dr->record_state,
                                'open_type'=>$dr->open_type,
                                'pass_flag'=>$dr->pass_flag,
                                'hand_value'=>$dr->hand_value,
                                'lfeet_value'=>$dr->lfeet_value,
                                'rfeet_value'=>$dr->rfeet_value,
                                'exact_record_time'=>$dr->exact_record_time,
                            ];
                        }

                        $sql = "SELECT a.*, ac.controller_name FROM ({$dr_out_query->toSql()}) a
                            LEFT JOIN tms_door_record_raw AS dr
                                ON a.exact_record_time = dr.record_time
                                AND a.card_no = dr.card_no
                            LEFT JOIN tms_access_controller AS ac
                                ON ac.controller_no = dr.controller_no
                            WHERE a.exact_record_time IS NOT NULL";
                        $binding = $dr_out_query->getBindings();
                        if($use_attn_grp_controller){
                            if(count($attn_grp_controller[$emp->attendance_group_id]) > 0){
                                $inQuery = implode(',', array_fill(0, count($attn_grp_controller[$emp->attendance_group_id]), '?'));
                                $sql .= " AND ac.id IN(".$inQuery.")";
                                $binding = array_merge($binding, $attn_grp_controller[$emp->attendance_group_id]);
                            }else{
                                $sql .= " AND ac.id = '' ";
                            }
                        }
                        $dr_query = DB::select(DB::raw($sql), $binding);

                        if(isset($request['debug']) && (Request::get('emp_no') != '' || Request::get('card_no') != '')){
                            dump($binding);
                        }

                        foreach ($dr_query as $dr) {
                            $insert_temp_query[] = [
                                'card_no' => $emp->card_no,
                                'emp_no' => $emp->emp_no,
                                'employee_name' => $emp->employee_name,
                                'attendance_group_code' => $emp->attendance_group_code,
                                'shift_code' => $emp->shift_code,
                                'door_no' => $dr->door_no,
                                'department_code' => $emp->department_code,
                                'controller_name'=>$dr->controller_name,
                                'record_time'=>$dr->exact_record_time,
                                'record_state'=>$dr->record_state,
                                'open_type'=>$dr->open_type,
                                'pass_flag'=>$dr->pass_flag,
                                'hand_value'=>$dr->hand_value,
                                'lfeet_value'=>$dr->lfeet_value,
                                'rfeet_value'=>$dr->rfeet_value,
                                'exact_record_time'=>$dr->exact_record_time,
                            ];

                        }
                    }

                }
                $start_date->addDay();
                foreach(array_chunk($insert_temp_query, 1000) as $t){
                    DB::table('temp_tms_door_record_raw')->insert($t);
                }
                $insert_temp_query = array();

            }
            // IN - emp work sche
        }

        $query = DB::table('temp_tms_door_record_raw AS tdr')
            ->select('tdr.*');
        if(Request::get('sort') != '' && Request::get('order') != ''){
            $query->orderBy(Request::get('sort'), Request::get('order'));
        }else{
            $query->orderBy('tdr.emp_no','ASC');
            $query->orderBy('tdr.record_time','ASC');
        }

        if ($rawdata) {
            if($export){
                $tbl_records = $query->get();
            }else{
                $tbl_records = $query->paginate(50);
            }
            return [
              'request' => $request,
              'tbl_records' => $tbl_records,
              'emps' => $emps
            ];
        } elseif ($export == true) {
            $tbl_records = $query->get();
            return $tbl_records;
        } else {
            $tbl_records = $query->paginate(50);
        }

        $plant = \App\TmsEmpPlant::pluck('code','id');

        return view('erp.tms.hrms_report', [
          'request' => $request,
          'tbl_records' => $tbl_records,
          'emps' => $emps,
          'form_url' => url('tms/report/hrms'),
          'export_url' => url('tms/report/hrms/'),
          'page_title' => 'HRMS Report',
          'plant' => $plant,
        ]);
    }
}
