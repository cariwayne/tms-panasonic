<?php

namespace App\Http\Controllers\ERP\Tms;

use App\Http\Controllers\Controller;

use DB, Request;
use Carbon\Carbon;

class HrmsReportControllerOld extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function export()
    {
        $results = $this->index(true);
        return view('erp.tms.export_dat', [
          'tbl_records' => $results
        ]);
    }



    public function rawdata()
    {
        $results = $this->index(false, true);
        $results['form_url'] = url('tms/report/hrms_old/rawdata');
        $results['export_url'] = url('tms/report/hrms_old/rawdata/export');
        $results['page_title'] = 'HRMS Raw Data';

        $plant = \App\TmsEmpPlant::pluck('code','id');
        $results['plant'] = $plant;

        return view('erp.tms.hrms_report_old', $results);
    }

    public function rawdata_export()
    {
        $results = $this->index(true, true);
        return view('erp.tms.hrms_export_xls', [
          'tbl_records' => $results['tbl_records']
        ]);
    }



    public function index($export=false, $rawdata = false){
        //<-- we use global request to get the param of URI
        $request = Request::all();

        $tbl_records = array();
        $emps = array();

        if(isset($request['date'])){
            list($start_date, $end_date) = explode(' - ', $request['date']);

            $start_date = Carbon::createFromFormat('d/m/Y H:i', $start_date);
            $end_date = Carbon::createFromFormat('d/m/Y H:i', $end_date);

            $i = 0;
            while($start_date<$end_date){

                $query = DB::table('tms_emp_badge_card AS c')
                ->leftJoin('tms_employee AS e', 'c.emp_no', '=', 'e.emp_no')
                ->leftJoin('tms_emp_group AS g', 'e.group_id', '=', 'g.id')
                ->leftJoin('tms_emp_department AS d', 'e.department_id', '=', 'd.id')

                ->leftJoin('tms_emp_workschedule AS ewt',
                    function($join) use ($start_date) {
                        $join->on(
                            'ewt.workschedule_date', '=',
                            DB::raw('"'.$start_date->format('Y-m-d').'"')
                        );
                        $join->on(
                            DB::raw(
                            '(ewt.reference_no = c.emp_no AND ewt.reference_type="emp_no")'),
                            DB::raw(''),
                            DB::raw('')
                        );
                    }
                )
                ->leftJoin('tms_workschedule AS ew', 'ewt.workschedule_id', '=', 'ew.id')

                ->leftJoin('tms_emp_workschedule AS egwt',
                    function($join) use ($start_date) {
                        $join->on(
                            'egwt.workschedule_date', '=',
                            DB::raw('"'.$start_date->format('Y-m-d').'"')
                        );
                        $join->on(
                            DB::raw(
                            '(egwt.reference_no = g.code AND egwt.reference_type="group_code")'),
                            DB::raw(''),
                            DB::raw('')
                        );
                    })
                ->leftJoin('tms_workschedule AS egw', 'egwt.workschedule_id', '=', 'egw.id')

                ->select('c.*', 'e.name as employee_name', 'e.emp_no',  'd.code as department_code',
                  DB::raw('IF(ew.code IS NOT NULL, ew.code, egw.code) as shift_code'),
                  DB::raw('IF(ew.code IS NOT NULL, ew.time_in_from, egw.time_in_from) as time_in_from'),
                  DB::raw('IF(ew.code IS NOT NULL, ew.time_out_to, egw.time_out_to) as time_out_to'),
                  DB::raw('IF(ew.code IS NOT NULL, ew.next_day, egw.next_day) as next_day')
                );

                if ($request['card_no'] != '') {
                    $query->where('c.card_no', $request['card_no']);
                }
                if (Request::get('plant_id') != '') {
                    $query->where('e.plant_id', $request['plant_id']);
                }
                if ($request['emp_no'] != '') {
                    $query->where('e.emp_no', $request['emp_no']);
                }
                if ($request['department'] != '') {
                    $query->where('d.code', $request['department']);
                }
                $query->groupBy('c.card_no');
                $query->orderBy('c.emp_no', 'ASC');

                if ($export == true) {
                    $emps = $query->get();
                } else {
                    $emps = $query->paginate(25);
                }

                foreach ($emps as $emp) {

                    $start_time = clone $start_date;
                    $end_time = clone $start_date;

                    if ($emp->next_day == 1) {
                        // subtract 1 day
                        $start_time->subDay();
                    }
                    if ($emp->time_in_from == '') {
                        $emp->time_in_from = '00:00:00';
                    }
                    if ($emp->time_out_to == '' || $emp->time_out_to == '00:00:00') {
                        $emp->time_out_to = '23:59:59';
                    }
                    // $time_in_from = explode('.', number_format($emp->time_in_from,2));
                    // $time_out_to = explode('.', number_format($emp->time_out_to,2));

                    $time_in_from = explode(':',$emp->time_in_from);
                    $time_out_to = explode(':',$emp->time_out_to);

                    $start_time
                        ->hour($time_in_from[0])
                        ->minute($time_in_from[1])
                        ->second(0);
                    $end_time
                        ->hour($time_out_to[0])
                        ->minute($time_out_to[1])
                        ->second(0);

                    $dr_in_query = DB::table('tms_door_record_raw AS dr')
                      ->leftJoin('tms_access_controller AS ac', 'ac.controller_no', '=', 'dr.controller_no')
                      ->where('dr.card_no', $emp->card_no)
                      ->whereBetween('dr.record_time',[
                        $start_time->format('Y-m-d H:i:s'),
                        $end_time->format('Y-m-d H:i:s')
                      ])
                      ->orderBy('dr.record_time');

                    $dr_out_query = clone $dr_in_query;

                    if ($rawdata) {
                        $dr_in_query->select(
                            'dr.*',
                            'ac.controller_name',
                            DB::raw('dr.record_time as exact_record_time')
                        );
                    } else {
                        $dr_in_query->where('dr.record_state', 1)
                        ->select(
                            'dr.*',
                            'ac.controller_name',
                            DB::raw('MIN(dr.record_time) as exact_record_time')
                        );
                    }

                    $dr_out_query->where('dr.record_state', 2)
                    ->select(
                        'dr.*',
                        'ac.controller_name',
                        DB::raw('MAX(dr.record_time) as exact_record_time')
                    );

                    if ($rawdata) {
                        $dr_query = DB::table(DB::raw("(
                           SELECT * FROM ({$dr_in_query->toSql()}) a
                        ) as A"))
                        ->mergeBindings($dr_in_query);
                    } else {
                        $dr_query = DB::table(DB::raw("(
                           SELECT * FROM ({$dr_in_query->toSql()}) a
                           UNION
                           SELECT * FROM ({$dr_out_query->toSql()}) b
                        ) as A"))
                        ->mergeBindings($dr_in_query)
                        ->mergeBindings($dr_out_query);
                    }

//dump($dr_query->toSql());
//dump($start_time->format('Y-m-d H:i:s'));
//dump($end_time->format('Y-m-d H:i:s'));
                    $dr_query = $dr_query->get();
                    foreach ($dr_query as $dr) {
                        if ($dr->exact_record_time) {
                            $tbl_records[$i] = new \stdClass();
                            $tbl_records[$i]->door_no = $dr->door_no;
                            $tbl_records[$i]->record_state = $dr->record_state;
                            $tbl_records[$i]->record_time = $dr->exact_record_time;
                            $tbl_records[$i]->early_in = $dr->exact_record_time;
                            $tbl_records[$i]->last_out = $dr->exact_record_time;
                            $tbl_records[$i]->card_no = $emp->card_no;
                            $tbl_records[$i]->emp_no =  $emp->emp_no;
                            $tbl_records[$i]->employee_name = $emp->employee_name;
                            $tbl_records[$i]->shift_code = $emp->shift_code;
                            $tbl_records[$i]->department_code = $emp->department_code;
                            $tbl_records[$i]->controller_name = $dr->controller_name;
                            $tbl_records[$i]->time_in_from = '';
                            $i++;
                        }
                    }
                }
                $start_date->addDay();
            }
              // IN - emp work sche
        }

        if ($rawdata) {
            return [
              'request' => $request,
              'tbl_records' => $tbl_records,
              'emps' => $emps
            ];
        } elseif ($export == true) {
            return $tbl_records;
        }

        $plant = \App\TmsEmpPlant::pluck('code','id');

        return view('erp.tms.hrms_report_old', [
          'request' => $request,
          'tbl_records' => $tbl_records,
          'emps' => $emps,
          'form_url' => url('tms/report/hrms_old'),
          'export_url' => url('tms/report/hrms_old/export'),
          'page_title' => 'HRMS Report',
          'plant' => $plant,
        ]);
    }
}
