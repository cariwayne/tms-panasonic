<?php

namespace App\Http\Controllers\ERP\Tms;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input, Session, Redirect;

class WorkScheduleController extends Controller
{

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct(){
    $this->middleware('auth');
  }

  public function index(){
    $tbl_records = array();
    $query = \DB::table('tms_workschedule AS w')
        ->select('w.*');

    if(isset($_GET['code']) && $_GET['code'] !=''){
      $query->where('w.code', '=' ,  $_GET['code']);
    }
    if(isset($_GET['next_day']) && $_GET['next_day'] !=''){
      $query->where('w.next_day', '=' ,  $_GET['next_day']);
    }
    $employees = $query->paginate(50);
    return view('erp.tms.workschedule_list', [
          'tbl_records'=>$employees
        ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
      return view('erp.tms.workschedule_form', []);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
      /*
      $validator = \Validator::make($request->all(), [
        'name' => 'required|max:255',
        'email' => 'required|email|max:255',
      ]);
      if ($validator->fails()) {
          return redirect('emp/user/create')
                      ->withErrors($validator)
                      ->withInput();
      }else{
        $post = array();
        $post['emp_no'] = $request->input('emp_no');
        $post['name'] = $request->input('name');
        $post['email'] = $request->input('email');
        $post['password'] = bcrypt($request->input('password'));

        $user = \App\TmsEmployee::create($post);
        $user->save();

        foreach($request->input('roles') as $role_id){
          $role = \App\Role::findOrFail($role_id);
          $user->attachRole($role);
        }
        if($user->save()){
            return \Redirect::to('emp/user');
        }

      }*/

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
      //
      $post = \App\TmsWorkSchedule::find($id);
      dump($post);
      return view('erp.tms.workschedule_form', ['post'=>$post]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id, Request $request)
  {
      //
      $validator = \Validator::make($request->all(), [
        'name' => 'required|max:255',
        'email' => 'required|email|max:255',
      ]);
      if ($validator->fails()) {
          return redirect('emp/employee/'.$id.'/edit')
                      ->withErrors($validator)
                      ->withInput();
      }else{
        $emp = \App\TmsEmployee::find($id);
        $emp->name = $request->input('name');
        $emp->email = $request->input('email');
        $emp->department_id = $request->input('department_id');
        $emp->group_id = $request->input('group_id');

        if($emp->save()){
          Session::flash('success', $emp->name.' Saved');
          return Redirect::to('emp/employee');
        }

      }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
      //
      $workschedule = \App\TmsWorkSchedule::find($id);
      $workschedule->delete();
      return \Redirect::to('tms/workschedule');
  }

}
