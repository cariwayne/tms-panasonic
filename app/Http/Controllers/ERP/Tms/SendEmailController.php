<?php

namespace App\Http\Controllers\ERP\Tms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB, Storage;
use Mail;
use App\TmsEmails;
use Carbon\Carbon;

class sendEmailController extends Controller
{
  public function Export_Mail($export=false){
    $request = \Request::all(); //<-- we use global request to get the param of URI
    $tday = date('N');
    $times = date('H:i');

    //compare result from database
    $val_emails = TmsEmails::where('emp_days', 'LIKE', '%'.$tday.'%')
                  ->where('emp_times', '=', $times)
                  ->pluck('emp_emails', 'id');
    if(sizeof($val_emails)>0){
      $start_date = $end_date = date('05/04/2018 H:i');

      $query = DB::table('tms_door_record_raw AS dr')
        ->leftJoin('tms_emp_badge_card AS c', 'dr.card_no', '=', 'c.card_no')
        ->leftJoin('tms_employee AS e', 'c.emp_no', '=', 'e.emp_no')
        ->leftJoin('tms_emp_department AS d', 'e.department_id', '=', 'd.id')
        ->leftJoin('tms_t_c_cardpower AS cp', function($join){
          $join->on('cp.card_no', '=', 'dr.card_no')
               ->on('cp.controller_no', '=', 'dr.controller_no');
        })
        ->leftJoin('tms_access_controller AS ac', 'ac.controller_no', '=', 'dr.controller_no')
        ->leftJoin('tms_t_p_power AS p', 'cp.card_door_zone', '=', 'p.power_number')
        ->leftJoin('tms_t_h_hand AS h', 'dr.hand_value', '=', 'h.import_id')
        ->leftJoin('tms_t_h_hand AS lh', 'dr.lfeet_value', '=', 'lh.import_id')
        ->leftJoin('tms_t_h_hand AS rh', 'dr.rfeet_value', '=', 'rh.import_id')
        ->select('dr.*', 'e.name AS emp_name', 'e.emp_no', 'p.power_name',
          'e.job',
          'd.code as department',
          'ac.controller_name',
          'h.hvalue_en AS hvalue_en', 'lh.hvalue_en AS lhvalue_en', 'rh.hvalue_en as rhvalue_en')
        ->whereBetween('dr.record_time',[
          Carbon::createFromFormat('d/m/Y H:i', $start_date)->format('Y-m-d 00:00:00'),
          Carbon::createFromFormat('d/m/Y H:i', $end_date)->format('Y-m-d 23:59:59')
        ]);

      $query->leftJoin('tms_emails AS te', 'te.emp_line', '=', 'e.job')->where('te.emp_emails', '=', 'hisakiknight@gmail.com');

      $tbl_records = $query->paginate(50);
      return $tbl_records;
    }
  }

/*
  public function ExportToMail(){
    $results = $this->Export_Mail(true);
    $ex_results = $this->ExportToMail();
    dd($ex_results);
    Storage::put( 'my_file.xls', $ex_results );
  }
*/


  public function ExportToMail(){
    $results = $this->Export_Mail(true);
    return view('erp.tms.export_xls', [
      'tbl_records' => $results,
      'title' => 'ESD report '.date('d/m/Y H:i:s')
    ]);
  }

  public function index(Request $request)
  {
   $days = date('N', strtotime('now'));
   $today = Carbon::now();
   $auto_email = array();
   //check days, times and deleted item
   $auto_email['users'] = DB::table('tms_emails')
     ->where('emp_days', 'like', '%'.$days.'%')
     ->where('deleted_at', '=', NULL)
     ->where('emp_times', '=', substr($today, 11, -3))
     ->get();
 
   // for loop
   foreach($auto_email['users'] as $individual_email){
     $report_types = ' ';
     if($individual_email->emp_reports == 1){
       $report_types = 'Auditor';
     }elseif($individual_email->emp_reports == 2){
       $report_types = 'Director';
     }else{
       $report_types = 'Manager';
     }
     // array var that you write earlier, I just modify
     $view_data = array(
       'receiver' => $individual_email->id,
       'blade_emails' => $individual_email->emp_emails,
       'blade_reports' => $report_types,
     );

     // this is call closure function 
     // use ($individual_email) is pass variable to closure function
     // this is working, if the tms_emails not have much data, if you got 1000 records, then this code might got timeout.

     if($auto_email['users']){
       $excel = $this->ExportToMail();
       Storage::put( 'esd_report\ESD report '.str_replace(['/',':'], '_', date('d/m/Y H:i:s')).'.xls', $excel );
       Mail::send('erp.mail.send', $view_data, function ($message) use ($individual_email, $excel){
        $message->from('me@gmail.com', 'Christian Nwamba');
        // need to validate if valid email
        // $message->to('yapfeng@caritech.com');
        $message->to($individual_email->emp_emails);
        //attachment
        $message->attach('C:\xampp\htdocs\apps\tms\storage\app\esd_report\ESD report '.str_replace(['/',':'], '_', date('d/m/Y H:i:s')).'.xls');
       });
     }
    }
    return '
    Sent
    <script>
      setTimeout(function(){
        location.reload()
      }, 59000)
    </script>
    ';
    // return view('erp.tms.send_email', $auto_email);
   }

/*
   public function send_email(Request $request){
      
      $title = $request->input('title');
      $content = $request->input('content');

      $view_data = array(
        'count' => request('count'),
        'receiver' => request('receiver'),
        'blade_emails' => request('blade_emails'),
        'blade_reports' => request('blade_reports'),
        'test'  => 'asdfsadfs',
      );
      Mail::send('erp.mail.send', $view_data, function ($message)
      {

          $message->from('me@gmail.com', 'Christian Nwamba');

          $message->to('chrisn@scotch.io');

      });

      return response()->json(['message' => 'Request completed']);
   }
*/

}
?>