<?php

namespace App\Http\Controllers\ERP\Tms;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Log;
use Mail;
use Session;
use Carbon\Carbon;
use App\Libraries\XBaseTable;

class ImportDataController extends Controller
{

    private $accdb_conn;

    private $source_accdb = 'AccessControlData.accdb';
    private $backup_accdb = 'AccessControlData_backup.accdb';
    private $dbf_file_path;
	
	public function processCardNo($card_no){
		$card_no = substr($card_no, -5);
		return sprintf("%05d",$card_no);
	}
	

    public function copydb(){
      if (file_exists($this->source_accdb) && !copy($this->source_accdb, $this->backup_accdb)) {
        Log::error('failed to copy file: '.$this->source_accdb);
      }else{
        $tmsSyncObj = \App\TmsSync::firstOrNew(array('reference'=>'Clone AccessControlData.accdb'));
        $tmsSyncObj->reference = 'Clone AccessControlData.accdb';
        $tmsSyncObj->last_record = date('Y-m-d H:i:s');
        $tmsSyncObj->save();
      }
    }

    public function connectACCDB(){

      if(config('app.sqlserver') != ''){
        // $dsn = "odbc:Driver={SQL Server};Server=(local);Database=AccessControlData;Uid=sa;Pwd=12345678;";
        $dsn = config('app.sqlserver');

      }else{
        $this->copydb();

        if(!file_exists($this->backup_accdb)){
            $this->copydb();
        }else{
            $db = \App\TmsSync::where('reference', 'Clone AccessControlData.accdb')->first();
            if(!$db || ($db && date('Y-m-d', strtotime($db->last_record)) < date('Y-m-d'))){
              $this->copydb();
            }
        }
        // Connection to ms access
        $dsn = "odbc:Driver={Microsoft Access Driver (*.mdb, *.accdb)};Dbq=".$this->backup_accdb.";Uid=; Pwd=123;";
      }
      $this->accdb_conn = new \PDO($dsn);

      return $this;
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
      // $this->middleware('auth');
      $this->source_accdb = html_entity_decode(config('app.accdb_file_path')).$this->source_accdb;
      $this->backup_accdb = html_entity_decode(config('app.accdb_file_path')).$this->backup_accdb;
      $this->dbf_file_path = html_entity_decode(config('app.dbf_file_path'));

    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

      $this->connectACCDB();

      $sql = 'SELECT MAX(RecordID) as lastRecord FROM T_D_DoorRecord';
      $stmt = $this->accdb_conn->prepare($sql);
      $stmt->execute();
      $T_D_DoorRecord = $stmt->fetchColumn();
      $T_D_DoorRecord_sync = DB::table('tms_sync')
        ->where('reference','T_D_DoorRecord')->first();
      if($T_D_DoorRecord_sync === null){
        $T_D_DoorRecord_sync = 0;
      }else{
        $T_D_DoorRecord_sync = $T_D_DoorRecord_sync->last_record;
      }

      $sql = 'SELECT MAX(CardInfoID) as lastRecord FROM T_C_CardInfo';
      $stmt = $this->accdb_conn->prepare($sql);
      $stmt->execute();
      $T_C_CardInfo = $stmt->fetchColumn();
      $T_C_CardInfo_sync = DB::table('tms_sync')
        ->where('reference','T_C_CardInfo')->first();
      if($T_C_CardInfo_sync === null){
        $T_C_CardInfo_sync = 0;
      }else{
        $T_C_CardInfo_sync = $T_C_CardInfo_sync->last_record;
      }

      $sql = 'SELECT MAX(CardPowerID) as lastRecord FROM T_C_CardPower';
      $stmt = $this->accdb_conn->prepare($sql);
      $stmt->execute();
      $T_C_CardPower = $stmt->fetchColumn();
      $T_C_CardPower_sync = DB::table('tms_sync')
        ->where('reference','T_C_CardPower')->first();
      if($T_C_CardPower_sync === null){
        $T_C_CardPower_sync = 0;
      }else{
        $T_C_CardPower_sync = $T_C_CardPower_sync->last_record;
      }

      $sql = 'SELECT MAX(PowerID) as lastRecord FROM T_P_Power';
      $stmt = $this->accdb_conn->prepare($sql);
      $stmt->execute();
      $T_P_Power = $stmt->fetchColumn();
      $T_P_Power_sync = DB::table('tms_sync')
        ->where('reference','T_P_Power')->first();
      if($T_P_Power_sync === null){
        $T_P_Power_sync = 0;
      }else{
        $T_P_Power_sync = $T_P_Power_sync->last_record;
      }

      $sql = 'SELECT MAX(ID) as lastRecord FROM Hand';
      $stmt = $this->accdb_conn->prepare($sql);
      $stmt->execute();
      $T_H_Hand = $stmt->fetchColumn();
      $T_H_Hand_sync = DB::table('tms_sync')
        ->where('reference','T_H_Hand')->first();
      if($T_H_Hand_sync === null){
        $T_H_Hand_sync = 0;
      }else{
        $T_H_Hand_sync = $T_H_Hand_sync->last_record;
      }
      if(is_dir($this->dbf_file_path)){
          $table = new XBaseTable($this->dbf_file_path.'TMDS.DBF');
          $table->open();
          $TMDS = $table->recordCount;
            $table->close();
          $TMDS_sync = DB::table('tms_sync')
            ->where('reference','TMDS_SYNC')->first();
          if($TMDS_sync === null){
            $TMDS_sync = 0;
          }else{
            $TMDS_sync = $TMDS_sync->last_record;
          }

          $table = new XBaseTable($this->dbf_file_path.'TMEMPGP.DBF');
          $table->open();
          $TMEMPGP = $table->recordCount;
            $table->close();
          $TMEMPGP_sync = DB::table('tms_sync')
            ->where('reference','TMEMPGP_sync')->first();
          if($TMEMPGP_sync === null){
            $TMEMPGP_sync = 0;
          }else{
            $TMEMPGP_sync = $TMEMPGP_sync->last_record;
          }

          $table = new XBaseTable($this->dbf_file_path.'TMSCHTYP.DBF');
          $table->open();
          $TMSCHTYP = $table->recordCount;
            $table->close();
          $TMSCHTYP_sync = DB::table('tms_sync')
            ->where('reference','TMSCHTYP_sync')->first();
          if($TMSCHTYP_sync === null){
            $TMSCHTYP_sync = 0;
          }else{
            $TMSCHTYP_sync = $TMSCHTYP_sync->last_record;
          }
      }else{
        $TMDS = 0;
        $TMDS_sync = 0;
        $TMEMPGP = 0;
        $TMEMPGP_sync = 0;
        $TMSCHTYP = 0;
        $TMSCHTYP_sync = 0;
      }
      // Ping
      $controllers = \App\TmsAccessController::all();

      return view('erp.tms.importdata', [
          'source_accdb' => $this->source_accdb,
          'T_D_DoorRecord' => $T_D_DoorRecord,
          'T_D_DoorRecord_sync' => $T_D_DoorRecord_sync,
          'T_C_CardInfo' => $T_C_CardInfo,
          'T_C_CardInfo_sync' => $T_C_CardInfo_sync,
          'T_C_CardPower' => $T_C_CardPower,
          'T_C_CardPower_sync' => $T_C_CardPower_sync,
          'T_P_Power' => $T_P_Power,
          'T_P_Power_sync' => $T_P_Power_sync,
          'T_H_Hand' => $T_H_Hand,
          'T_H_Hand_sync' => $T_H_Hand_sync,
          'TMDS' => $TMDS,
          'TMDS_sync' => $TMDS_sync,
          'TMEMPGP' => $TMEMPGP,
          'TMEMPGP_sync' => $TMEMPGP_sync,
          'TMSCHTYP' => $TMSCHTYP,
          'TMSCHTYP_sync' => $TMSCHTYP_sync,
          'controllers' => $controllers
        ]);
    }


    public function format_time($time)
    {
        $time_from = explode('.', number_format($time,2));

        $date = Carbon::now();
        $date
            ->hour($time_from[0])
            ->minute($time_from[1])
            ->second(0);
        return $date->format('H:i:s');

    }


    public function import_dbf(){


      /* Work Schedule  */
      $table = new XBaseTable($this->dbf_file_path.'TMSCHTYP.DBF');
      $table->open();
      $sql_val = array();
      $i = 0;

      $TMSCHTYP_sync = DB::table('tms_sync')
        ->where('reference','TMSCHTYP_sync')->first();
      if($TMSCHTYP_sync !== null && $TMSCHTYP_sync->last_record > 0){
        $table->moveTo( $TMSCHTYP_sync->last_record);
      }
      $batch_limit = 600;
      for ($i = 0; $i < $batch_limit; $i++) {
        $record=$table->nextRecord();
        if($record && $record->getStringByName('TYPE')== 'W'){

            $obj = \App\TmsWorkSchedule::firstOrNew(array('CODE'=>$record->getStringByName('CODE')));

            $obj->code = $record->getStringByName('CODE');
            $obj->description = $record->getStringByName('DESC');
            $obj->time_in = $this->format_time($record->getStringByName('TIME_FR'));
            $obj->time_in_from = $this->format_time($record->getStringByName('TIME_FRIN'));
            $obj->time_in_to = $this->format_time($record->getStringByName('TIME_TOIN'));
            $obj->time_out = $this->format_time($record->getStringByName('TIME_TOIN'));
            $obj->time_out_from = $this->format_time($record->getStringByName('TIME_FROUT'));
            $obj->time_out_to = $this->format_time($record->getStringByName('TIME_TOOUT'));
            if($record->getStringByName('NEXTDAY') == 'Y'){
              $obj->next_day = 1;
            }else{
              $obj->next_day = 0;
            }
            $obj->save();

        }
        // complete the batch process - end loop
        if($table->recordPos+1>=$table->recordCount) {
          $table->recordPos = $table->recordCount;
          break;
        }
      }
      $tmsSyncObj = \App\TmsSync::firstOrNew(array('reference'=>'TMSCHTYP_sync'));
      $tmsSyncObj->reference = 'TMSCHTYP_sync';
      $tmsSyncObj->last_record = $table->recordPos;
      $tmsSyncObj->save();
      $tms_workschedule = $table->recordPos;


      /* Employee Work Schedule  */

      $table = new XBaseTable($this->dbf_file_path.'TMDS.DBF');
      $table->open();
      $sql_val = array();
      $i = 0;

      $TMDS_SYNC = DB::table('tms_sync')
        ->where('reference','TMDS_SYNC')->first();
      if($TMDS_SYNC !== null && $TMDS_SYNC->last_record > 0){
        $table->moveTo( $TMDS_SYNC->last_record);
      }
      $batch_limit = 600;
      for ($i = 0; $i < $batch_limit; $i++) {
        $record=$table->nextRecord();
        if($record){

          $workschedule_id = DB::table('tms_workschedule')->where('code', $record->getStringByName('SCHE_CODE'))->first();
          if($workschedule_id){

            $sch_date = Carbon::parse($record->getStringByName('SCHE_DATE'))->format('Y-m-d');
            if($sch_date >= '2016-04-01'){
              //$obj = new \App\TmsEmpWorkSchedule();
			  $obj = \App\TmsEmpWorkSchedule::firstOrNew(array('reference_no'=>$record->getStringByName('EMP_NO'),'workschedule_date'=>$sch_date));
              if( $record->getStringByName('SCHE_SEL') == 'G' ){
                $obj->reference_type = 'group_code';
              }else{
                $obj->reference_type = 'emp_no';
              }
              $obj->reference_no = $record->getStringByName('EMP_NO');
              $obj->workschedule_id = $workschedule_id->id;
              $obj->workschedule_date = $sch_date;
              $obj->save();
            }
          }
        }
        // complete the batch process - end loop
        if($table->recordPos+1>=$table->recordCount) {
          $table->recordPos = $table->recordCount;
          break;
        }
      }
      $tmsSyncObj = \App\TmsSync::firstOrNew(array('reference'=>'TMDS_SYNC'));
      $tmsSyncObj->reference = 'TMDS_SYNC';
      $tmsSyncObj->last_record = $table->recordPos;
      $tmsSyncObj->save();
      $tms_emp_workschedule = $table->recordPos;

      /* Employee & emp group  */

      $table = new XBaseTable($this->dbf_file_path.'TMEMPGP.DBF');
      $table->open();
      $sql_val = array();
      $i = 0;

      $sync = DB::table('tms_sync')
        ->where('reference','TMEMPGP_sync')->first();
      if($sync !== null && $sync->last_record > 0){
        $table->moveTo( $sync->last_record);
      }
      $batch_limit = 200;
      for ($i = 0; $i < $batch_limit; $i++) {
        $record=$table->nextRecord();
		// echo $sync->last_record;
		// echo $record->getStringByName('EMP_NO');
		// dd($record);exit();
        if($record){

          // all record
          $obj = \App\TmsEmpGroupRaw::firstOrNew(array(
              // 'group_code'=>$record->getStringByName('GROUP_CODE'),
              'emp_no'=>$record->getStringByName('EMP_NO'),
          ));
          $obj->group_code = $record->getStringByName('GROUP_CODE');
          $obj->emp_no = $record->getStringByName('EMP_NO');
          $obj->save();

          // new emp group
          $obj = \App\TmsEmpGroup::firstOrNew(array(
              'code'=>$record->getStringByName('GROUP_CODE')
          ));
          $obj->code = $record->getStringByName('GROUP_CODE');
          $obj->description = $record->getStringByName('GROUP_CODE');
          $obj->save();

          $empObj = \App\TmsEmployee::where('emp_no', '=', $record->getStringByName('EMP_NO'))->first();
          if($empObj){
            $empObj->group_id = $obj->id;
            $empObj->save();
          }
        }
        // complete the batch process - end loop
        if($table->recordPos+1>=$table->recordCount) {
          $table->recordPos = $table->recordCount;
          break;
        }
      }
      $tmsSyncObj = \App\TmsSync::firstOrNew(array('reference'=>'TMEMPGP_sync'));
      $tmsSyncObj->reference = 'TMEMPGP_sync';
      $tmsSyncObj->last_record = $table->recordPos;
      $tmsSyncObj->save();
      $tms_emp_group = $table->recordPos;

      DB::statement("UPDATE `tms_employee` e LEFT JOIN `tms_emp_group_raw` gr ON e.emp_no = gr.emp_no LEFT JOIN `tms_emp_group` g ON gr.group_code = g.code SET e.group_id = g.id WHERE g.id IS NOT NULL ");

      return response()->json([
          'tms_workschedule' => $tms_workschedule,
          'tms_emp_workschedule' => $tms_emp_workschedule,
          'tms_emp_group' => $tms_emp_group,
          'last_sync' => date('Y-m-d H:i:s'),
        ]);
    }

    /**
     * The exec method uses the possibly insecure exec() function, which passes
     * the input to the system. This is potentially VERY dangerous if you pass in
     * any user-submitted data. Be SURE you sanitize your inputs!
     *
     * @return int
     *   Latency, in ms.
     */
    private function ping($host, $ttl=255, $timeout=5) {
        $latency = false;

        $ttl = escapeshellcmd($ttl);
        $timeout = escapeshellcmd($timeout);
        $host = escapeshellcmd($host);

        // Exec string for Windows-based systems.
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
          // -n = number of pings; -i = ttl; -w = timeout (in milliseconds).
          $exec_string = 'ping -n 1 -i ' . $ttl . ' -w ' . ($timeout * 1000) . ' ' . $host;
        }
        // Exec string for Darwin based systems (OS X).
        else if(strtoupper(PHP_OS) === 'DARWIN') {
          // -n = numeric output; -c = number of pings; -m = ttl; -t = timeout.
          $exec_string = 'ping -n -c 1 -m ' . $ttl . ' -t ' . $timeout . ' ' . $host;
        }
        // Exec string for other UNIX-based systems (Linux).
        else {
          // -n = numeric output; -c = number of pings; -t = ttl; -W = timeout
          $exec_string = 'ping -n -c 1 -t ' . $ttl . ' -W ' . $timeout . ' ' . $host;
        }

        exec($exec_string, $output, $return);

        // Strip empty lines and reorder the indexes from 0 (to make results more
        // uniform across OS versions).
        $commandOutput = implode($output, '');
        $output = array_values(array_filter($output));

        // If the result line in the output is not empty, parse it.
        if (!empty($output[1])) {
          // Search for a 'time' value in the result line.
          $response = preg_match("/time(?:=|<)(?<time>[\.0-9]+)(?:|\s)ms/", $output[1], $matches);

          // If there's a result and it's greater than 0, return the latency.
          if ($response > 0 && isset($matches['time'])) {
            $latency = round($matches['time']);
          }
        }

        return $latency;
    }

    private function email_provider($controller){
        if(config('app.email_provider') != ''){
            Mail::send('erp.mail.controller_dc', [], function ($m) use($controller) {
                $m->from(config('app.email_provider'), 'TMS');
                $m->to(explode(',',config('app.email_provider')))
                    ->subject('Attn: Controller '.$controller->controller_no.' - '.$controller->controller_name.' disconnected');
            });
        }
        $tmsSyncObj = \App\TmsSync::firstOrNew(array('reference'=>'CONTROLLER_DC_EMAIL_PROVIDER'));
        $tmsSyncObj->reference = 'CONTROLLER_DC_EMAIL_PROVIDER';
        $tmsSyncObj->last_record = date('Y-m-d H:i:s');
        $tmsSyncObj->save();
        return 'email provider';
    }

    private function email_client($controller){
        if(config('app.email_client') != ''){
            Mail::send('erp.mail.controller_dc', [], function ($m) use($controller) {
                $m->from(config('app.email_provider'), 'TMS');
                $m->to(explode(',',config('app.email_client')))
                    ->subject('Attn: Controller '.$controller->controller_no.' - '.$controller->controller_name.'  disconnected');
            });
        }
        $tmsSyncObj = \App\TmsSync::firstOrNew(array('reference'=>'CONTROLLER_DC_EMAIL_CLIENT'));
        $tmsSyncObj->reference = 'CONTROLLER_DC_EMAIL_CLIENT';
        $tmsSyncObj->last_record = date('Y-m-d H:i:s');
        $tmsSyncObj->save();
        return 'email client';
    }

    public function ping_controller(){
        $controllers = \App\TmsAccessController::all();
        $email = '';
        foreach($controllers as $controller){
            if($this->ping($controller->controller_ip)){
                $update = \App\TmsAccessController::find( $controller->id);
                $update->last_online = date("Y-m-d H:i:s");
                $update->save();
            }else{
                // email provider
                $tmsSyncObj = \App\TmsSync::where(array('reference'=>'CONTROLLER_DC_EMAIL_PROVIDER'))->first();
                if($tmsSyncObj){
                    $current_time = Carbon::now();
                    $last_email_time = Carbon::parse($tmsSyncObj->last_record);
                    // 5 minutes
                    if($current_time->diffInMinutes($last_email_time) > 5){
                        $email[] = $this->email_provider($controller);
                    }
                }else{
                    $email[] = $this->email_provider($controller);
                }

                // email client
                $tmsSyncObj = \App\TmsSync::where(array('reference'=>'CONTROLLER_DC_EMAIL_CLIENT'))->first();
                if($tmsSyncObj){
                    $current_time = Carbon::now();
                    $last_email_time = Carbon::parse($tmsSyncObj->last_record);
                    // 12 hours
                    if($current_time->diffInHours($last_email_time) > 12){
                        $email[] = $this->email_client($controller);
                    }
                }else{
                    $email[] = $this->email_client($controller);
                }
            }
        }
        return response()->json([
            'email' => $email,
            'last_sync' => date('Y-m-d H:i:s'),
        ]);
    }


    public function import_accdb(){

      $this->connectACCDB();

      // T_D_DoorRecord
      $sql = 'SELECT MAX(RecordID) as lastRecord FROM T_D_DoorRecord';
      $stmt = $this->accdb_conn->prepare($sql);
      $stmt->execute();
      $lastRecord_source = $stmt->fetchColumn();

      $T_D_DoorRecord_sync = DB::table('tms_sync')
        ->where('reference','T_D_DoorRecord')->first();
      if($T_D_DoorRecord_sync === null){
        $T_D_DoorRecord_sync = 0;
      }else{
        $T_D_DoorRecord_sync = $T_D_DoorRecord_sync->last_record;
      }

      if ($lastRecord_source != $T_D_DoorRecord_sync) {
        if($T_D_DoorRecord_sync != 0){
          $sql = 'SELECT TOP 400 * FROM T_D_DoorRecord
          WHERE RecordID > '.$T_D_DoorRecord_sync.'
          ORDER BY RecordID ASC';
        } else {
          $sql = 'SELECT TOP 400 * FROM T_D_DoorRecord ORDER BY RecordID ASC';
        }
        $stmt = $this->accdb_conn->prepare($sql);
        $stmt->execute();
        while($row = $stmt->fetch()){
          try {
            $doorRecordObj = new \App\TmsDoorRecordRaw();
            $doorRecordObj->import_id = $row['RecordID'];
            $doorRecordObj->card_no = $this->processCardNo($row['CardNO']);
            $doorRecordObj->door_no = $row['DoorNO'];
            $doorRecordObj->controller_no = $row['ContrillerNO'];
            $doorRecordObj->record_time = $row['RecordTime'];
            $doorRecordObj->record_state = $row['RecordState'];
            $doorRecordObj->open_type = $row['OpenType'];
            $doorRecordObj->pass_flag = $row['PassFlag'];
            $doorRecordObj->hand_value = $row['HandValue'];
            $doorRecordObj->lfeet_value = $row['LFeetValue'];
            $doorRecordObj->rfeet_value = $row['RFeetValue'];

            $doorRecordObj->save();
          } catch(\PDOException $e){
            break;
          }
        }
      }
	  
      DB::statement( 'UPDATE tms_door_record_raw d 
      LEFT JOIN tms_emp_badge_card c 
      ON d.card_no = c.card_no 
      SET d.emp_no = c.emp_no 
      WHERE c.card_no IS NOT NULL AND d.emp_no IS NULL' );
	  
      $T_D_DoorRecord_sync = DB::table('tms_door_record_raw')->max('import_id');
      $T_D_DoorRecord_sync = ($T_D_DoorRecord_sync===null)? 0 : $T_D_DoorRecord_sync;

      $tmsSyncObj = \App\TmsSync::firstOrNew(array('reference'=>'T_D_DoorRecord'));
      $tmsSyncObj->reference = 'T_D_DoorRecord';
      $tmsSyncObj->last_record = $T_D_DoorRecord_sync;
      $tmsSyncObj->save();


      // T_C_CardInfo
      $sql = 'SELECT MAX(CardInfoID) as lastRecord FROM T_C_CardInfo';
      $stmt = $this->accdb_conn->prepare($sql);
      $stmt->execute();
      $lastRecord_source = $stmt->fetchColumn();

      $T_C_CardInfo_sync = DB::table('tms_sync')
        ->where('reference','T_C_CardInfo')->first();
      if($T_C_CardInfo_sync === null){
        $T_C_CardInfo_sync = 0;
      }else{
        $T_C_CardInfo_sync = $T_C_CardInfo_sync->last_record;
      }

      if ($lastRecord_source != $T_C_CardInfo_sync) {
        if($T_C_CardInfo_sync != 0){
          $sql = 'SELECT TOP 100 CardInfoID,EmployeeNumber,CardNO,CardName,Job,DepartmentName
          FROM T_C_CardInfo
          WHERE CardInfoID > '.$T_C_CardInfo_sync.'
          ORDER BY CardInfoID ASC';
        } else {
          $sql = 'SELECT TOP 100 CardInfoID,EmployeeNumber,CardNO,CardName,Job,DepartmentName
          FROM T_C_CardInfo ORDER BY CardInfoID ASC';
        }
        $stmt = $this->accdb_conn->prepare($sql);
        $stmt->execute();
        while($row = $stmt->fetch()){
          try {

            /*
			if(count(explode('\\',$row['DepartmentName'])) >= 2){
				list($plant,$attendanceGroup) = explode('\\',$row['DepartmentName']);
			}else{ 
				$plant='';
				$attendanceGroup = '';
			}

            $empPlantObj = \App\TmsEmpPlant::firstOrNew(array('code'=>$plant));
            $empPlantObj->import_id = $row['CardInfoID'];
            $empPlantObj->code = $plant;
            $empPlantObj->description = $plant;
            $empPlantObj->save();

            $empAttendanceGroupObj = \App\TmsEmpAttendanceGroup::firstOrNew(array('code'=>$attendanceGroup));
            $empAttendanceGroupObj->import_id = $row['CardInfoID'];
            $empAttendanceGroupObj->code = $attendanceGroup;
            $empAttendanceGroupObj->description = $attendanceGroup;
            $empAttendanceGroupObj->save();
*/

	//xx
	          $empBadgeObj = \App\TmsEmpBadgeCard::firstOrNew(array('emp_no'=>$row['EmployeeNumber'], 'card_no'=> $this->processCardNo($row['CardNO'])));
	          $empBadgeObj->import_id = $row['CardInfoID'];
	          $empBadgeObj->emp_no = $row['EmployeeNumber']==null?$row['CardInfoID']:$row['EmployeeNumber'];
	          $empBadgeObj->card_no = $this->processCardNo($row['CardNO']);
	          $empBadgeObj->save();

	          if($row['DepartmentName'] != '' && $department = \App\TmsEmpDepartment::firstOrNew(array('code'=>$row['DepartmentName']))){
	              $department->code = $row['DepartmentName'];
	              $department->description = $row['DepartmentName'];
	              $department->save();
	              $department_id = $department->id;
	          }else{
	              $department_id = 0;
	          }

              // $empObj = new \App\TmsEmployee();
              $empObj = \App\TmsEmployee::firstOrNew(array('emp_no'=>$row['EmployeeNumber']));
              $empObj->import_id = $row['CardInfoID'];
              $empObj->emp_no = $row['EmployeeNumber']==null?$row['CardInfoID']:$row['EmployeeNumber'];
              $empObj->department_id = $department_id;
              $empObj->job = $row['Job'];
              //$empObj->plant_id = $empPlantObj->id;
              //$empObj->attendance_group_id = $empAttendanceGroupObj->id;
              $empObj->name = $row['CardName']==''?null:$row['CardName'];
              $empObj->save();

          } catch(\PDOException $e){
            \Log::error($e);
            break;
          }
        }
      }

      $T_C_CardInfo_sync = DB::table('tms_emp_badge_card')->max('import_id');
      $T_C_CardInfo_sync = ($T_C_CardInfo_sync===null)? 0 : $T_C_CardInfo_sync;
      $tmsSyncObj = \App\TmsSync::firstOrNew(array('reference'=>'T_C_CardInfo'));
      $tmsSyncObj->reference = 'T_C_CardInfo';
      $tmsSyncObj->last_record = $T_C_CardInfo_sync;
      $tmsSyncObj->save();


      // T_C_CardPower
      $sql = 'SELECT MAX(CardPowerID) as lastRecord FROM T_C_CardPower';
      $stmt = $this->accdb_conn->prepare($sql);
      $stmt->execute();
      $lastRecord_source = $stmt->fetchColumn();

      $T_C_CardPower_sync = DB::table('tms_sync')
        ->where('reference','T_C_CardPower')->first();
      if($T_C_CardPower_sync === null){
        $T_C_CardPower_sync = 0;
      }else{
        $T_C_CardPower_sync = $T_C_CardPower_sync->last_record;
      }

      if ($lastRecord_source != $T_C_CardPower_sync) {
        if($T_C_CardPower_sync != 0){
          $sql = 'SELECT TOP 100 * FROM T_C_CardPower
          WHERE CardPowerID > '.$T_C_CardPower_sync.'
          ORDER BY CardPowerID ASC';
        } else {
          $sql = 'SELECT TOP 100 * FROM T_C_CardPower ORDER BY CardPowerID ASC';
        }
        $stmt = $this->accdb_conn->prepare($sql);
        $stmt->execute();
        while($row = $stmt->fetch()){
          try {
            $obj = new \App\TmsCardPower();
            $obj->import_id = $row['CardPowerID'];
            $obj->card_no = $this->processCardNo($row['CardNO']);
            $obj->door_no = $row['DoorNO'];
            $obj->controller_no = $row['ContrillerNO'];
            $obj->door_begin_zone = $row['DoorBeginZone'];
            $obj->door_end_zone = $row['DoorEndZone'];
            $obj->card_door_zone = $row['CardDoorZone'];
            $obj->card_door_state = $row['CardDoorState'];
            $obj->report_state = $row['ReportState'];
            $obj->remarks = $row['Remarks'];
            $obj->save();
          } catch(\PDOException $e){
            break;
          }
        }
      }

      $T_C_CardPower_sync = DB::table('tms_t_c_cardpower')->max('import_id');
      $T_C_CardPower_sync = ($T_C_CardPower_sync===null)? 0 : $T_C_CardPower_sync;
      $tmsSyncObj = \App\TmsSync::firstOrNew(array('reference'=>'T_C_CardPower'));
      $tmsSyncObj->reference = 'T_C_CardPower';
      $tmsSyncObj->last_record = $T_C_CardPower_sync;
      $tmsSyncObj->save();


      // T_P_Power
      $sql = 'SELECT MAX(PowerID) as lastRecord FROM T_P_Power';
      $stmt = $this->accdb_conn->prepare($sql);
      $stmt->execute();
      $lastRecord_source = $stmt->fetchColumn();

      $T_P_Power_sync = DB::table('tms_sync')
        ->where('reference','T_P_Power')->first();
      if($T_P_Power_sync === null){
        $T_P_Power_sync = 0;
      }else{
        $T_P_Power_sync = $T_P_Power_sync->last_record;
      }

      if ($lastRecord_source != $T_P_Power_sync) {
        if($T_P_Power_sync != 0){
          $sql = 'SELECT TOP 100 * FROM T_P_Power
          WHERE PowerID > '.$T_P_Power_sync.'
          ORDER BY PowerID ASC';
        } else {
          $sql = 'SELECT TOP 100 * FROM T_P_Power ORDER BY PowerID ASC';
        }
        $stmt = $this->accdb_conn->prepare($sql);
        $stmt->execute();
        while($row = $stmt->fetch()){
          try {
            $obj = new \App\TmsPower();
            $obj->import_id = $row['PowerID'];
            $obj->power_name = $row['PowerName'];
            $obj->power_number = $row['PowerNumber'];
            $obj->save();
          } catch(\PDOException $e){
            break;
          }
        }
      }

      $T_P_Power_sync = DB::table('tms_t_p_power')->max('import_id');
      $T_P_Power_sync = ($T_P_Power_sync===null)? 0 : $T_P_Power_sync;
      $tmsSyncObj = \App\TmsSync::firstOrNew(array('reference'=>'T_P_Power'));

      $tmsSyncObj->reference = 'T_P_Power';
      $tmsSyncObj->last_record = $T_P_Power_sync;
      $tmsSyncObj->save();


      // T_H_Hand
      $sql = 'SELECT MAX(ID) as lastRecord FROM Hand';
      $stmt = $this->accdb_conn->prepare($sql);
      $stmt->execute();
      $lastRecord_source = $stmt->fetchColumn();

      $T_H_Hand_sync = DB::table('tms_sync')
        ->where('reference','T_H_Hand')->first();
      if($T_H_Hand_sync === null){
        $T_H_Hand_sync = 0;
      }else{
        $T_H_Hand_sync = $T_H_Hand_sync->last_record;
      }

      if ($lastRecord_source != $T_H_Hand_sync) {
        if($T_H_Hand_sync != 0){
          $sql = 'SELECT TOP 100 * FROM Hand
          WHERE ID > '.$T_H_Hand_sync.'
          ORDER BY ID ASC';
        } else {
          $sql = 'SELECT TOP 100 * FROM Hand ORDER BY ID ASC';
        }
        $stmt = $this->accdb_conn->prepare($sql);
        $stmt->execute();
        while($row = $stmt->fetch()){
          try {
            $obj = new \App\TmsHand();
            $obj->import_id = $row['ID'];
            $obj->hvalue = $row['Hvalue'];
            $obj->hvalue_en = $row['Hvalue_EN'];
            $obj->save();
          } catch(\PDOException $e){
            break;
          }
        }
      }

      $T_H_Hand_sync = DB::table('tms_t_h_hand')->max('import_id');
      $T_H_Hand_sync = ($T_H_Hand_sync===null)? 0 : $T_H_Hand_sync;
      $tmsSyncObj = \App\TmsSync::firstOrNew(array('reference'=>'T_H_Hand'));
      $tmsSyncObj->reference = 'T_H_Hand';
      $tmsSyncObj->last_record = $T_H_Hand_sync;
      $tmsSyncObj->save();

      return response()->json([
        'tms_door_record_raw' => $T_D_DoorRecord_sync,
        'tms_emp_badge_card' => $T_C_CardInfo_sync,
        'tms_t_c_cardpower' => $T_C_CardPower_sync,
        'tms_t_p_power' => $T_P_Power_sync,
        'tms_t_h_hand' => $T_H_Hand_sync,
        'last_sync' => date('Y-m-d H:i:s'),
        ]);
    }

    public function reset_sync(Request $request)
    {

      DB::statement("SET foreign_key_checks=0");
      $class_sync = array(
        'TmsDoorRecordRaw' => 'T_D_DoorRecord',
        'TmsEmpWorkSchedule' => 'TMDS_SYNC',
        'TmsEmpBadgeCard' => 'T_C_CardInfo',
        'TmsCardPower' => 'T_C_CardPower',
        'TmsPower' => 'T_P_Power',
        'TmsEmpGroup' => 'TMEMPGP_sync',
        'TmsWorkSchedule' => 'TMSCHTYP_sync',
      );
      $sync_update = array();
      $reset_elements = $request->input('reset');
      foreach($reset_elements as $reset_element => $reset_element_val){
        $elements = explode(',', $reset_element);
        foreach($elements as $element){
          if(isset($class_sync[$element])){
            $sync_update[] = $class_sync[$element];
          }
          $element = '\App\\'.$element;
          $element::truncate();
        }
      }
      $collection = DB::table('tms_sync')->whereIn('reference', $sync_update);
      $collection->update(array("last_record" => "0"));
      DB::statement("SET foreign_key_checks=1");
      return response()->json([
        'update' => $sync_update,
        ]);
    }
}
