<?php

namespace App\Http\Controllers\ERP\Tms;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Carbon\Carbon;

class EsdReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }


    public function export()
    {	
  		$request = \Request::all();
      $results = $this->index(true);
      return view('erp.tms.export_xls', [
        'tbl_records' => $results->get(),
        'title' => 'ESD Test Log '.$request['date']
      ]);
    }

    public function testStatus($export=false)
    {
        // Employee list and last test IN result
        $request = \Request::all(); //<-- we use global request to get the param of URI

        $tbl_records = array();
        if(isset($request['date'])){
            $start_date = $end_date = $request['date'];

            //search function
            $sqljoin = function(&$query) use ($request) {
                $query
                ->leftJoin('tms_emp_department AS d', 'e.department_id', '=', 'd.id')
                ->leftJoin('tms_t_c_cardpower AS cp', function($join){
                    $join->on('cp.card_no', '=', 'dr.card_no')
                         ->on('cp.controller_no', '=', 'dr.controller_no');
                })
                ->leftJoin('tms_access_controller AS ac', 'ac.controller_no', '=', 'dr.controller_no')
                ->leftJoin('tms_t_p_power AS p', 'cp.card_door_zone', '=', 'p.power_number');

                $query
                ->leftJoin('tms_t_h_hand AS h', 'dr.hand_value', '=', 'h.import_id')
                ->leftJoin('tms_t_h_hand AS lh', 'dr.lfeet_value', '=', 'lh.import_id')
                ->leftJoin('tms_t_h_hand AS rh', 'dr.rfeet_value', '=', 'rh.import_id');

                if(isset($request['department']) && $request['department'] != ''){
                  $query->where('d.id', 'like', $request['department']);
                }
                if(isset($request['job']) && $request['job'] != ''){
                  $query->where('e.job', 'like', $request['job']);
                }
                if(isset($request['emp_no']) && $request['emp_no'] != ''){
                  $query->where('e.emp_no', $request['emp_no']);
                }
                if(isset($request['card_no']) && $request['card_no'] != ''){
                  $query->where('dr.card_no', $request['card_no']);
                }
                if(isset($request['plant_id']) && $request['plant_id'] != ''){
                  $query->where('e.plant_id', $request['plant_id']);
                }
                if(isset($request['test_result']) && $request['test_result'] != ''){
                    if( $request['test_result'] == '-1'){
                        $query->whereNull('dr.pass_flag');
                    }else{
                        $query->where('dr.pass_flag', $request['test_result']);
                    }
                }
            };

            //get data from latest record time
            $last_status_query = DB::table('tms_door_record_raw AS dr')
              ->leftJoin('tms_emp_badge_card AS c', 'dr.card_no', '=', 'c.card_no')
              ->leftJoin('tms_employee AS e', 'c.emp_no', '=', 'e.emp_no');

            $last_status_query->where('dr.record_state', 1);

             /*
            $last_status_query->whereBetween('dr.record_time',[
                Carbon::createFromFormat('d/m/Y H:i', $start_date)->format('Y-m-d 00:00:00'),
                Carbon::createFromFormat('d/m/Y H:i', $end_date)->format('Y-m-d H:i:s')
              ]);
             */
            
            $last_status_query->where('dr.record_time', '>=',
                Carbon::createFromFormat('d/m/Y H:i', $start_date)->format('Y-m-d 00:00:00')
              ); 

            $last_status_query->select(
                DB::raw('MAX(dr.record_time) as max_record_time'),
                DB::raw('MAX(dr.id) as max_id'),
                'c.emp_no',
                'c.card_no'
            );

            $last_status_query->groupBy('c.emp_no');

            //compare $query and $last_status_query
            $query = DB::table('tms_employee AS e')
              ->leftJoin('tms_emp_badge_card AS c', 'c.emp_no', '=', 'e.emp_no');

            $query->leftJoin(
                DB::raw("({$last_status_query->toSql()}) ldr"),
                function($join){
                    $join->on('ldr.emp_no', '=', 'c.emp_no');
                }
            )->mergeBindings($last_status_query);


            $query->leftJoin('tms_door_record_raw AS dr',  function($join){
                $join->on('ldr.emp_no', '=', 'c.emp_no')
                     ->on('ldr.max_id', '=', 'dr.id')
                     ->on('ldr.max_record_time', '=', 'dr.record_time')
                     ->on('dr.record_state', '=', DB::raw('1'));
                }
            );
            $sqljoin($query);

            $query->select('dr.*', 'e.name AS emp_name', 'e.emp_no', 'p.power_name',
                'd.code as department',
                'ac.controller_name',
                'e.job',
                'h.hvalue_en AS hvalue_en', 'lh.hvalue_en AS lhvalue_en', 'rh.hvalue_en as rhvalue_en');

            $query->groupBy('c.emp_no');

            if(isset($request['sort'])){
                $query->orderBy($request['sort'], $request['order']);
            }else{
                $query->orderBy('dr.record_time', 'DESC');
            }

            if($export == true){
              return $query;
            }
            $tbl_records = $query->paginate(50);
        }
        $department = \App\TmsEmpDepartment::pluck('code','id');
        $job = collect(DB::table('tms_employee as e')
          ->leftJoin('tms_emp_department as d', 'e.department_id', '=', 'd.id')
          ->where('d.code', '=', 'PRODUCTION')
          ->groupBy('job')
          ->orderBy('job')->get())->pluck('job','job');

        return view('erp.tms.esd_report', [
          'request' => $request,
          'tbl_records' => $tbl_records,
          'form_url' => url('tms/report/esd/status'),
          'export_url' => url('tms/report/esd/status/export'),
          'page_title' => 'ESD Test Status',
          'department' => $department,
          'job' => $job,
        ]);
    }
    public function exportTestStatus()
    {
		$request = \Request::all(); 
        $results = $this->testStatus(true);
        return view('erp.tms.export_xls', [
          'tbl_records' => $results->get(),
		      'title' => 'ESD Test Status '.$request['date']
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($export=false){
        $request = \Request::all(); //<-- we use global request to get the param of URI

        $tbl_records = array();
        if(isset($request['date'])){
            list($start_date, $end_date) = explode(' - ', $request['date']);

            $query = DB::table('tms_door_record_raw AS dr')
              ->leftJoin('tms_emp_badge_card AS c', 'dr.card_no', '=', 'c.card_no')
              ->leftJoin('tms_employee AS e', 'c.emp_no', '=', 'e.emp_no')
              ->leftJoin('tms_emp_department AS d', 'e.department_id', '=', 'd.id')
              ->leftJoin('tms_t_c_cardpower AS cp', function($join){
                $join->on('cp.card_no', '=', 'dr.card_no')
                     ->on('cp.controller_no', '=', 'dr.controller_no');
              })
              ->leftJoin('tms_access_controller AS ac', 'ac.controller_no', '=', 'dr.controller_no')
              ->leftJoin('tms_t_p_power AS p', 'cp.card_door_zone', '=', 'p.power_number')
              ->leftJoin('tms_t_h_hand AS h', 'dr.hand_value', '=', 'h.import_id')
              ->leftJoin('tms_t_h_hand AS lh', 'dr.lfeet_value', '=', 'lh.import_id')
              ->leftJoin('tms_t_h_hand AS rh', 'dr.rfeet_value', '=', 'rh.import_id')
              ->select('dr.*', 'e.name AS emp_name', 'e.emp_no', 'p.power_name',
                'e.job',
                'd.code as department',
                'ac.controller_name',
                'h.hvalue_en AS hvalue_en', 'lh.hvalue_en AS lhvalue_en', 'rh.hvalue_en as rhvalue_en')
              ->whereBetween('dr.record_time',[
                Carbon::createFromFormat('d/m/Y H:i', $start_date)->format('Y-m-d H:i:s'),
                Carbon::createFromFormat('d/m/Y H:i', $end_date)->format('Y-m-d H:i:s')
              ]);

            if(isset($request['emp_no']) && $request['emp_no'] != ''){
              $query->where('e.emp_no', $request['emp_no']);
            }
            if(isset($request['job']) && $request['job'] != ''){
              $query->where('e.job', 'like', $request['job']);

            }
            if(isset($request['card_no']) && $request['card_no'] != ''){
              $query->where('dr.card_no', $request['card_no']);
            }
            if(isset($request['department']) && $request['department'] != ''){
              $query->where('d.id', 'like', $request['department'] );
            }
            if(isset($request['plant_id']) && $request['plant_id'] != ''){
              $query->where('e.plant_id', $request['plant_id']);
            }
            if(!isset($request['out'])){
                $query->where('dr.record_state', 1);
            }
            if(isset($request['out'])){
                $query->where('dr.record_state', 2);
            }
            if(isset($request['test_result']) && $request['test_result'] != ''){
              $query->where('dr.pass_flag', $request['test_result']);
            }
            if(isset($request['sort'])){
                $query->orderBy($request['sort'], $request['order']);
            }else{
                $query->orderBy('dr.record_time', 'DESC');
            }

            if($export == true){
              return $query;
            }
            $tbl_records = $query->paginate(50);
        }
        $sort_link = array(
          'date'=>request('date'),
          'card_no'=>request('card_no'),
          'emp_no'=>request('emp_no'),
          'department'=>request('department'),
          'out'=>request('out')
        );
        $department = \App\TmsEmpDepartment::pluck('code','id');
        $job = collect(DB::table('tms_employee as e')
          ->leftJoin('tms_emp_department as d', 'e.department_id', '=', 'd.id')
          ->where('d.code', '=', 'PRODUCTION')
          ->groupBy('job')
          ->orderBy('job')->get())->pluck('job','job');

        return view('erp.tms.esd_report', [
          'request' => $request,
          'tbl_records' => $tbl_records,
          'form_url' => url('tms/report/esd'),
          'export_url' => url('tms/report/esd/export'),
          'sort_link' => $sort_link,
          'page_title' => 'ESD Test Log',
          'department' => $department,
          'job' => $job,
        ]);
    }

}
