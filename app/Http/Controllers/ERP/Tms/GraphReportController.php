<?php

namespace App\Http\Controllers\ERP\Tms;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ERP\Tms\EsdReportController;

use DB;
use Carbon\Carbon;

class GraphReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
      $this->middleware('auth');
    }


    public function export(){
      $results = $this->index(true);
      return view('erp.tms.export_xls', [
          'tbl_records' => $results->get(),
          'tbl_records_line' => $results->get(),
          'tbl_records_department' => $results->get()
        ]);
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($graph, $export=false, $ajax=false){ 
      $request = \Request::all(); //<-- we use global request to get the param of URI
      
      $tbl_records = array();
      $tbl_records_line = array();
      $tbl_records_department = array();
      $start_date = date('d/m/Y');
      $end_date = date('d/m/Y');
      // $start_date = '05/04/2018';
      // $end_date = '05/04/2018';
 /*
      if(isset($request['date'])){
        list($start_date, $end_date) = explode(' - ', $request['date']);
        
        $query = DB::table('tms_door_record_raw AS dr')
          ->leftJoin('tms_emp_badge_card AS c', 'dr.card_no', '=', 'c.card_no')
          ->leftJoin('tms_employee AS e', 'c.emp_no', '=', 'e.emp_no')
          ->leftJoin('tms_emp_department AS d', 'e.department_id', '=', 'd.id')
          ->leftJoin('tms_t_c_cardpower AS cp', function($join){
            $join->on('cp.card_no', '=', 'dr.card_no')
                 ->on('cp.controller_no', '=', 'dr.controller_no');
          })
          ->leftJoin('tms_t_p_power AS p', 'cp.card_door_zone', '=', 'p.power_number')
          ->leftJoin('tms_t_h_hand AS h', 'dr.hand_value', '=', 'h.import_id')
          ->leftJoin('tms_t_h_hand AS lh', 'dr.lfeet_value', '=', 'lh.import_id')
          ->leftJoin('tms_t_h_hand AS rh', 'dr.rfeet_value', '=', 'rh.import_id')
          ->select('dr.*', DB::raw('COUNT(dr.id) as cnt'), 'e.name AS emp_name', 'e.emp_no', 'p.power_name',    'd.code AS department_name', 
            'h.hvalue_en AS hvalue_en', 'lh.hvalue_en AS lhvalue_en', 'rh.hvalue_en as rhvalue_en')
          ->whereBetween('dr.record_time',[
            Carbon::createFromFormat('d/m/Y', $start_date)->format('Y-m-d'),
            Carbon::createFromFormat('d/m/Y', $end_date)->format('Y-m-d')
          ])->groupBy('d.code')->groupBy('dr.pass_flag');
        
        $query->where('d.code', '<>' ,'');

        if($request['door_no'] != ''){
          $query->where('dr.door_no', $request['door_no']);
        }
        if($request['emp_no'] != ''){
          $query->where('e.emp_no', $request['emp_no']);
        }
        if($request['card_no'] != ''){
          $query->where('dr.card_no', $request['card_no']);
        }
        $query->orderBy('p.power_number', 'ASC');
        if($export == true){
          return $query;
        }
        $tbl_records = $query->paginate(50);
      }
*/
        if($graph == 'pie'){

          if(isset($request['date'])){
            list($start_date, $end_date) = explode(' - ', $request['date']);
          }else{
            $start_date = $end_date = date('d/m/Y');
          }   
            //employee - pie
            $query_pie = DB::table('tms_door_record_raw AS dr')
              ->leftJoin('tms_emp_badge_card AS c', 'dr.card_no', '=', 'c.card_no')
              ->leftJoin('tms_employee AS e', 'c.emp_no', '=', 'e.emp_no')
              ->leftJoin('tms_emp_department AS d', 'e.department_id', '=', 'd.id')
              ->leftJoin('tms_t_c_cardpower AS cp', function($join){
                $join->on('cp.card_no', '=', 'dr.card_no')
                     ->on('cp.controller_no', '=', 'dr.controller_no');
              })
              ->leftJoin('tms_t_p_power AS p', 'cp.card_door_zone', '=', 'p.power_number')
              ->leftJoin('tms_t_h_hand AS h', 'dr.hand_value', '=', 'h.import_id')
              ->leftJoin('tms_t_h_hand AS lh', 'dr.lfeet_value', '=', 'lh.import_id')
              ->leftJoin('tms_t_h_hand AS rh', 'dr.rfeet_value', '=', 'rh.import_id')
              ->select('dr.*', DB::raw('COUNT(dr.id) as cnt'), 'e.name AS emp_name', 'e.emp_no', 'p.power_name', 'dr.pass_flag AS total_name', 'h.hvalue_en AS hvalue_en', 'lh.hvalue_en AS lhvalue_en', 'rh.hvalue_en as rhvalue_en')
              ->whereBetween('dr.record_time',[
                Carbon::createFromFormat('d/m/Y', $start_date)->format('Y-m-d 00:00:00'),
                Carbon::createFromFormat('d/m/Y', $end_date)->format('Y-m-d 23:59:59')
              ])
              ->groupBy('d.code')->groupBy('dr.pass_flag');

            $query_pie->where('d.code', '<>' ,'');
            $tbl_records_total = $query_pie->paginate(50);
            if($ajax){
              return $tbl_records_total;
            }else{
              return view('erp.tms.realtime_graph_report_pie', [
                'request' => $request,
                'tbl_records_total' => $tbl_records_total
              ]);
            }
           
        }elseif($graph == 'department'){

          if(isset($request['date'])){
            list($start_date, $end_date) = explode(' - ', $request['date']);
          }else{
            $start_date = $end_date = date('d/m/Y');
          } 
          //employee - department
          $query_department = DB::table('tms_door_record_raw AS dr')
            ->leftJoin('tms_emp_badge_card AS c', 'dr.card_no', '=', 'c.card_no')
            ->leftJoin('tms_employee AS e', 'c.emp_no', '=', 'e.emp_no')
            ->leftJoin('tms_emp_department AS d', 'e.department_id', '=', 'd.id')
            ->leftJoin('tms_t_c_cardpower AS cp', function($join){
              $join->on('cp.card_no', '=', 'dr.card_no')
                   ->on('cp.controller_no', '=', 'dr.controller_no');
            })
            ->leftJoin('tms_t_p_power AS p', 'cp.card_door_zone', '=', 'p.power_number')
            ->leftJoin('tms_t_h_hand AS h', 'dr.hand_value', '=', 'h.import_id')
            ->leftJoin('tms_t_h_hand AS lh', 'dr.lfeet_value', '=', 'lh.import_id')
            ->leftJoin('tms_t_h_hand AS rh', 'dr.rfeet_value', '=', 'rh.import_id')
            ->select('dr.*', DB::raw('COUNT(dr.id) as cnt'), 'e.name AS emp_name', 'e.emp_no', 'p.power_name', 'd.code AS department_name', 'h.hvalue_en AS hvalue_en', 'lh.hvalue_en AS lhvalue_en', 'rh.hvalue_en as rhvalue_en')
            ->whereBetween('dr.record_time',[
              Carbon::createFromFormat('d/m/Y', $start_date)->format('Y-m-d 00:00:00'),
              Carbon::createFromFormat('d/m/Y', $end_date)->format('Y-m-d 23:59:59')
            ])
            ->groupBy('d.code')->groupBy('dr.pass_flag')->orderBy('d.code', 'asc')->orderBy('dr.pass_flag', 'asc');
          $query_department->where('d.code', '<>' ,'');
          $tbl_records_department = $query_department->get();
          if($ajax){
            return $tbl_records_department;
          }else{
            return view('erp.tms.realtime_graph_report_bar_department', [
              'request' => $request,
              'tbl_records_department' => $tbl_records_department
            ]);
          } 
          
        }elseif($graph == 'line'){
           /*
          if(isset($request['date'])){

            
            list($start_date, $end_date) = explode(' - ', $request['date']);
            //employee - line
            $query_line = DB::table('tms_door_record_raw AS dr')
              ->leftJoin('tms_emp_badge_card AS c', 'dr.card_no', '=', 'c.card_no')
              ->leftJoin('tms_employee AS e', 'c.emp_no', '=', 'e.emp_no')
              ->leftJoin('tms_emp_department AS d', 'e.department_id', '=', 'd.id')
              ->leftJoin('tms_emp_plant AS ep', 'dr.import_id', '=', 'ep.import_id')
              ->leftJoin('tms_t_c_cardpower AS cp', function($join){
                $join->on('cp.card_no', '=', 'dr.card_no')
                     ->on('cp.controller_no', '=', 'dr.controller_no');
              })
              ->leftJoin('tms_t_p_power AS p', 'cp.card_door_zone', '=', 'p.power_number')
              ->leftJoin('tms_t_h_hand AS h', 'dr.hand_value', '=', 'h.import_id')
              ->leftJoin('tms_t_h_hand AS lh', 'dr.lfeet_value', '=', 'lh.import_id')
              ->leftJoin('tms_t_h_hand AS rh', 'dr.rfeet_value', '=', 'rh.import_id')
              ->select('dr.*', DB::raw('COUNT(dr.id) as cnt'), 'e.name AS emp_name', 'e.emp_no', 'p.power_name', 'e.job AS job_name', 'h.hvalue_en AS hvalue_en', 'lh.hvalue_en AS lhvalue_en', 'rh.hvalue_en as rhvalue_en')
              ->whereBetween('dr.record_time',[
                Carbon::createFromFormat('d/m/Y', $start_date)->format('Y-m-d 00:00:00'),
                Carbon::createFromFormat('d/m/Y', $end_date)->format('Y-m-d 23:59:59')
              ])->where('e.job', 'like', '%Line%')
              ->groupBy('e.job')->groupBy('dr.pass_flag')
              ->orderBy('e.job', 'asc')
              ->orderBy('dr.record_time', 'desc');
          }else{
            //employee - line
            $query_line = DB::table('tms_door_record_raw AS dr')
              ->leftJoin('tms_emp_badge_card AS c', 'dr.card_no', '=', 'c.card_no')
              ->leftJoin('tms_employee AS e', 'c.emp_no', '=', 'e.emp_no')
              ->leftJoin('tms_emp_department AS d', 'e.department_id', '=', 'd.id')
              ->leftJoin('tms_emp_plant AS ep', 'dr.import_id', '=', 'ep.import_id')
              ->leftJoin('tms_t_c_cardpower AS cp', function($join){
                $join->on('cp.card_no', '=', 'dr.card_no')
                     ->on('cp.controller_no', '=', 'dr.controller_no');
              })
              ->leftJoin('tms_t_p_power AS p', 'cp.card_door_zone', '=', 'p.power_number')
              ->leftJoin('tms_t_h_hand AS h', 'dr.hand_value', '=', 'h.import_id')
              ->leftJoin('tms_t_h_hand AS lh', 'dr.lfeet_value', '=', 'lh.import_id')
              ->leftJoin('tms_t_h_hand AS rh', 'dr.rfeet_value', '=', 'rh.import_id')
              ->select('dr.*', DB::raw('COUNT(dr.id) as cnt'), 'e.name AS emp_name', 'e.emp_no', 'p.power_name', 'e.job AS job_name', 'h.hvalue_en AS hvalue_en', 'lh.hvalue_en AS lhvalue_en', 'rh.hvalue_en as rhvalue_en')
              ->where('e.job', 'like', '%Line%')
              ->groupBy('e.job')->groupBy('dr.pass_flag')
              ->orderBy('e.job', 'asc')->orderBy('dr.record_time', 'desc');
          }

          $query_line->where('d.code', '<>' ,'');
          $tbl_records_line = $query_line->paginate(50);

          */
          if(isset($request['date'])){
            $start_date = $request['date'];
          }else{
            $start_date = date('d/m/Y H:i');
          }
          $esdquery = $this->esdTestStatus($start_date); 
          $date = Carbon::createFromFormat('d/m/Y H:i', $start_date)->format('Y-m-d 00:00:00');
          $query = DB::select( 
            'SELECT a.*, a.job as job_name,COUNT(a.emp_no) as cnt 
            FROM ('.$esdquery->toSql().') as a 
            WHERE a.department LIKE ? 
            GROUP BY a.job, a.pass_flag
            ORDER BY a.job, a.pass_flag' , [1, $date, 'PRODUCTION']); 

          $tbl_records_line = $query;

          if($ajax){
            return $tbl_records_line;
          }else{
            return view('erp.tms.realtime_graph_report_bar', [
              'request' => $request,
              'tbl_records_line' => $tbl_records_line
            ]);
          }
        }
    }

    public function esdTestStatus($date){
        $request = \Request::all(); //<-- we use global request to get the param of URI
        $request['date'] = $date;
        $tbl_records = array();
        if(isset($request['date'])){
            $start_date = $end_date = $request['date'];

            //search function
            $sqljoin = function(&$query) use ($request) {
                $query
                ->leftJoin('tms_emp_department AS d', 'e.department_id', '=', 'd.id')
                ->leftJoin('tms_t_c_cardpower AS cp', function($join){
                    $join->on('cp.card_no', '=', 'dr.card_no')
                         ->on('cp.controller_no', '=', 'dr.controller_no');
                })
                ->leftJoin('tms_access_controller AS ac', 'ac.controller_no', '=', 'dr.controller_no')
                ->leftJoin('tms_t_p_power AS p', 'cp.card_door_zone', '=', 'p.power_number');

                $query
                ->leftJoin('tms_t_h_hand AS h', 'dr.hand_value', '=', 'h.import_id')
                ->leftJoin('tms_t_h_hand AS lh', 'dr.lfeet_value', '=', 'lh.import_id')
                ->leftJoin('tms_t_h_hand AS rh', 'dr.rfeet_value', '=', 'rh.import_id');

                if(isset($request['department']) && $request['department'] != ''){
                  $query->where('d.code', 'like', $request['department'].'%');
                }
                if(isset($request['emp_no']) && $request['emp_no'] != ''){
                  $query->where('e.emp_no', $request['emp_no']);
                }
                if(isset($request['card_no']) && $request['card_no'] != ''){
                  $query->where('dr.card_no', $request['card_no']);
                }
                if(isset($request['plant_id']) && $request['plant_id'] != ''){
                  $query->where('e.plant_id', $request['plant_id']);
                }
                if(isset($request['test_result']) && $request['test_result'] != ''){
                    if( $request['test_result'] == '-1'){
                        $query->whereNull('dr.pass_flag');
                    }else{
                        $query->where('dr.pass_flag', $request['test_result']);
                    }
                }
            };

            //get data from latest record time
            $last_status_query = DB::table('tms_door_record_raw AS dr')
              ->leftJoin('tms_emp_badge_card AS c', 'dr.card_no', '=', 'c.card_no')
              ->leftJoin('tms_employee AS e', 'c.emp_no', '=', 'e.emp_no');

            $last_status_query->where('dr.record_state', 1);

            /*
            $last_status_query->whereBetween('dr.record_time',[
                Carbon::createFromFormat('d/m/Y H:i', $start_date)->format('Y-m-d 00:00:00'),
                Carbon::createFromFormat('d/m/Y H:i', $end_date)->format('Y-m-d H:i:s')
              ]);
            */
              
            $last_status_query->where('dr.record_time', '>=',
                Carbon::createFromFormat('d/m/Y H:i', $start_date)->format('Y-m-d 00:00:00')
              );

            $last_status_query->select(
                DB::raw('MAX(dr.record_time) as max_record_time'),
                DB::raw('MAX(dr.id) as max_id'),
                'c.emp_no',
                'c.card_no'
            );

            $last_status_query->groupBy('c.emp_no');

            //compare $query and $last_status_query
            $query = DB::table('tms_employee AS e')
              ->leftJoin('tms_emp_badge_card AS c', 'c.emp_no', '=', 'e.emp_no');

            $query->leftJoin(
                DB::raw("({$last_status_query->toSql()}) ldr"),
                function($join){
                    $join->on('ldr.emp_no', '=', 'c.emp_no');
                }
            )->mergeBindings($last_status_query);


            $query->leftJoin('tms_door_record_raw AS dr',  function($join){
                $join->on('ldr.emp_no', '=', 'c.emp_no')
                     ->on('ldr.max_id', '=', 'dr.id')
                     ->on('ldr.max_record_time', '=', 'dr.record_time')
                     ->on('dr.record_state', '=', DB::raw('1'));
                }
            );
            $sqljoin($query);

            $query->select( DB::raw('IF(dr.pass_flag IS NULL, 2, dr.pass_flag) as pass_flag'), 'e.name AS emp_name', 'e.emp_no', 'p.power_name',
                'd.code as department',
                'ac.controller_name',
                'e.job',
                'h.hvalue_en AS hvalue_en', 'lh.hvalue_en AS lhvalue_en', 'rh.hvalue_en as rhvalue_en');

            $query->groupBy('c.emp_no');

            if(isset($request['sort'])){
                $query->orderBy($request['sort'], $request['order']);
            }else{
                $query->orderBy('dr.record_time', 'DESC');
            }

            
              return $query;
            
            
        }
          
    }

    public function ajax($graph){
      return $this->index($graph, false, true);
    }
    
}
