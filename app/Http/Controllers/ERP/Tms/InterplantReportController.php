<?php

namespace App\Http\Controllers\ERP\Tms;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Carbon\Carbon;

class InterplantReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }


    public function exportxls()
    {
        $results = $this->index(true);
        return view('erp.tms.interplant_export_xls', [
          'tbl_records' => $results->get()
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($export=false){
        $request = \Request::all(); //<-- we use global request to get the param of URI

        $tbl_records = array();
        if(isset($request['date'])){
            list($start_date, $end_date) = explode(' - ', $request['date']);

            $query = DB::table('tms_interplant_log AS dr')
              ->leftJoin('tms_emp_badge_card AS c', 'dr.card_no', '=', 'c.card_no')
              ->leftJoin('tms_employee AS e', 'c.emp_no', '=', 'e.emp_no')
              ->leftJoin('tms_emp_department AS d', 'e.department_id', '=', 'd.id')
              ->leftJoin('tms_access_controller AS ac', 'ac.id', '=', 'dr.controller_id')
              ->select('dr.*', 'e.name AS emp_name', 'e.emp_no',
                'd.code as department',
                'ac.controller_name')
              ->whereBetween('dr.record_time',[
                Carbon::createFromFormat('d/m/Y H:i', $start_date)->format('Y-m-d H:i:s'),
                Carbon::createFromFormat('d/m/Y H:i', $end_date)->format('Y-m-d H:i:s')
              ]);

            if(isset($request['emp_no']) && $request['emp_no'] != ''){
              $query->where('e.emp_no', $request['emp_no']);
            }
            if(isset($request['card_no']) && $request['card_no'] != ''){
              $query->where('dr.card_no', $request['card_no']);
            }
            if(isset($request['department']) && $request['department'] != ''){
              $query->where('d.code', 'like', $request['department'].'%');
            }
            if(isset($request['pass']) && $request['pass'] != ''){
              $query->where('dr.pass', '=', $request['pass']);
            }
            /*
            if(isset($request['sort'])){
                $query->orderBy($request['sort'], $request['order']);
            }else{*/
                $query->orderBy('dr.card_no', 'ASC');
                $query->orderBy('dr.record_time', 'ASC');
            //}

            if($export == true){
              return $query;
            }
            $tbl_records = $query->paginate(50);
        }
        $sort_link = array(
          'date'=>request('date'),
          'card_no'=>request('card_no'),
          'emp_no'=>request('emp_no'),
          'department'=>request('department'),
        );
        $plant = \App\TmsEmpPlant::pluck('code','id');

        return view('erp.tms.interplant_report', [
          'request' => $request,
          'tbl_records' => $tbl_records,
          'form_url' => url('tms/report/interplant'),
          'export_url' => url('tms/report/interplant/exportxls'),
          'sort_link' => $sort_link,
          'page_title' => 'Interplant Log',
          'plant' => $plant,
        ]);
    }

}
