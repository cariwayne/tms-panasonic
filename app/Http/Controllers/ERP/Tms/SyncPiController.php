<?php

namespace App\Http\Controllers\ERP\Tms;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Log;
use Mail;
use Session;
use Carbon\Carbon;
use GuzzleHttp\Client;

class SyncPiController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        // $this->middleware('auth');

    }

    public function index(){
        $ip = '';


    }

    public function getEmpData(Request $request){
        $groups = \App\TmsEmpAttendanceGroup::whereIn('code', ['resign', 'resigned', 'invalid'])
            ->get();

        $query = \App\TmsEmployee::where('emp_no', '!=', '');
        foreach($groups as $g){
            $query->where('attendance_group_id', '!=', $g->id);
        }
        $batch = $query->get();
        $data = [];
        foreach($batch as $b){
            $data[] = [
                'emp_no' => $b->emp_no,
                'name' => $b->name
            ];
        }
        return response()->json(['row'=> count($data), 'data'=>$data]);
    }

    public function getEmpCardData(Request $request){
        $groups = \App\TmsEmpAttendanceGroup::whereIn('code', ['resign', 'resigned', 'invalid'])
            ->get();

        $query = DB::table('tms_emp_badge_card as c')
            ->leftJoin('tms_employee as e', 'c.emp_no', '=', 'e.emp_no')
            ->leftJoin('tms_emp_attendance_group as g', 'e.attendance_group_id', '=', 'g.id')
            ->select('c.*');
        foreach($groups as $g){
            $query->where('e.attendance_group_id', $g->id, '!=');
        }
        $batch = $query->get();
        $data = [];
        foreach($batch as $b){
            $data[] = [
                'emp_no' => $b->emp_no,
                'card_no' => $b->card_no
            ];
        }
        return response()->json(['row'=> count($data), 'data'=>$data]);
    }

    public function listen(Request $request){
        $log_list = $request->input('log');
        if(!$log_list){
            return response()->json(['success'=>0, 'sync_datetime'=>'']);
        }
        $data = array();
        $sync_datetime = date('Y-m-d H:i:s');
        $batch = \App\TmsInterplantLog::max('batch');

        $access_controller = \App\TmsAccessController::where('controller_ip', $request->ip())->first();
        $access_controller_device = array('controller_id'=>0, 'controller_ip'=>'');

        if($access_controller){
            $access_controller_device['controller_id'] = $access_controller->id;
            $access_controller_device['controller_ip'] = $access_controller->controller_ip;

        }

        $batch++;
        foreach($log_list as $list){
            $list = collect($list);
            $data[] = array(
                'batch'=> $batch,
                'record_time'=> $list->get('record_time'),
                'controller_id'=> $access_controller_device['controller_id'],
                'controller_ip'=> $access_controller_device['controller_ip'],
                'card_no'=> $list->get('card_no'),
                'pass'=> $list->get('pass'),
                'sync_datetime'=> $sync_datetime,
                'sync_request_ip'=> $request->ip(),
            );
        }
        DB::transaction(function () use ($data) {
            \App\TmsInterplantLog::insert($data);
        });
        /*
        Mail::send('erp.mail.controller_dc', [], function ($m) {
            $m->from('chaiwei@caritech.com', 'TMS');
            $m->to('chaiwei@caritech.com')
              ->subject('Attn: Controller');
        });*/
        return response()->json(['success'=>1, 'sync_datetime'=>$sync_datetime]);

    }

    public function syncDoorRecordData(Request $request){
        $log_list = $request->input('log');
        if(!$log_list){
            return response()->json(['success'=>0, 'sync_datetime'=>'']);
        }
        $data = array();
        $sync_datetime = date('Y-m-d H:i:s');
        $access_controller = \App\TmsAccessController::where('controller_ip', $request->ip())->first();
        $access_controller_device = array(
          'controller_id'=>0, 
          'controller_no'=>'', 
          'controller_ip'=>'');

        if($access_controller){
            $access_controller_device['controller_id'] = $access_controller->id;
            $access_controller_device['controller_no'] = $access_controller->controller_no;
            $access_controller_device['controller_ip'] = $access_controller->controller_ip;

        }

        foreach($log_list as $list){
            $list = collect($list);
            $data[] = array(
                'import_id'=> $list->get('id'),
                'card_no'=> $list->get('card_no'),
                'door_no'=> '0',
                'controller_no'=> $access_controller_device['controller_no'],
                'record_time'=> $list->get('record_time'),
                'record_state'=> $list->get('type')==0?2:1,
                'open_type'=> 0,
                'pass_flag'=> $list->get('pass'),
                'hand_value'=> 65535,
                'lfeet_value'=> 65535,
                'rfeet_value'=> 65535,
            );
             
        }
        DB::transaction(function () use ($data) {
            \App\TmsDoorRecordRaw::insert($data);
            
            DB::statement( 'UPDATE tms_door_record_raw d 
            LEFT JOIN tms_emp_badge_card c 
            ON d.card_no = c.card_no 
            SET d.emp_no = c.emp_no 
            WHERE c.card_no IS NOT NULL AND d.emp_no IS NULL' );
        });

        return response()->json(['success'=>1, 'sync_datetime'=>$sync_datetime]);
    }

}
