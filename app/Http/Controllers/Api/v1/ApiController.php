<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class ApiController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        // $this->middleware('auth');
    }

    public function getDataFromPi(){
        $json = file_get_contents('http://192.168.0.21/pitms/public/api/v1/syncData');
        $obj = json_decode($json);
        dump($obj);
        return '';
    }

    public function storeShopperTransaction(Request $request){
        $post = $request->all();
        mail('chaiwei@caritech.com', '1stavenue - '.$post['table'].' insert', print_r($request->all(), true));
        switch($post['table']){
            case 'shopper_transaction':
                $post['transaction_date'] = date('Y-m-d H:i:s');
                \App\Models\ShopperTransaction::create($post);
                break;
            case 'shopper_transaction_receipt':
                \App\Models\ShopperTransactionReceipt::create($post);
                break;
            case 'shopper_transaction_campaign':
                \App\Models\ShopperTransactionCampaign::create($post);
                break;
            case 'shopper_transaction_product':
                \App\Models\ShopperTransactionProduct::create($post);
                break;
            case 'shopper_transaction_attachment':
                \App\Models\ShopperTransactionAttachment::create($post);
                break;
            case 'members':
                \App\Models\Member::create($post);
                break;

        }
        return response()->json($request->all());
    }



}