<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class UserController extends Controller
{
    //
    /**
       * Create a new controller instance.
       *
       * @return void
       */
      public function __construct(){
          $this->middleware('auth');
      }

      public function index(){
        $tbl_records = array();
        $lists = \App\User::paginate(50);
        return view('user_list', [
              'tbl_records'=>$lists
            ]);
      }

      /**
       * Show the form for creating a new resource.
       *
       * @return Response
       */
      public function create()
      {
          $roles = \App\Role::all();
          return view('user_form', ['roles'=>$roles]);
      }

      /**
       * Store a newly created resource in storage.
       *
       * @return Response
       */
      public function store(Request $request)
      {
          //
          $validator = \Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email',
          ]);
          if ($validator->fails()) {
              return redirect('auth/user/create')
                          ->withErrors($validator)
                          ->withInput();
          }else{
            $post = array();
            $post['name'] = $request->input('name');
            $post['email'] = $request->input('email');
            $post['password'] = bcrypt($request->input('password'));

            $user = \App\User::create($post);
            $user->save();

            foreach($request->input('roles') as $role_id){
              $role = \App\Role::findOrFail($role_id);
              $user->attachRole($role);
            }
            if($user->save()){
                return \Redirect::to('auth/user');
            }

          }
          /*
          $validator = \Validator::make($request->all(), [
              'controller_no' => 'required',
              'controller_name' => 'required',
              'controller_ip' => 'required',
          ]);
          if ($validator->fails()) {
              return redirect('tms/accesscontroller/create')
                          ->withErrors($validator)
                          ->withInput();
          }else{
              $post = \App\TmsAccessController::create(array(
  				'controller_no' => $request->input('controller_no'),
  				'controller_name'=> $request->input('controller_name'),
  				'controller_ip'  => $request->input('controller_ip')
  			));

              if($post->save()){
                  return \Redirect::to('tms/accesscontroller');
              }

          }*/
      }

      /**
       * Display the specified resource.
       *
       * @param  int  $id
       * @return Response
       */
      public function show($id)
      {
          //
      }

      /**
       * Show the form for editing the specified resource.
       *
       * @param  int  $id
       * @return Response
       */
      public function edit($id)
      {
          //
          $post = \App\User::with('roles')->find($id);
          $roles = \App\Role::all();
          return view('user_form', ['post'=>$post, 'roles'=>$roles]);
      }

      /**
       * Update the specified resource in storage.
       *
       * @param  int  $id
       * @return Response
       */
      public function update($id, Request $request)
      {
          //
          $validator = \Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.$id,
          ]);
          if ($validator->fails()) {
              return redirect('auth/user/'.$id.'/edit')
                          ->withErrors($validator)
                          ->withInput();
          }else{
            $user = \App\User::find($id);
            $user->name = $request->input('name');
            $user->email = $request->input('email');

            if($request->input('password') != ''){
              $user->password = bcrypt($request->input('password'));
            }
            $user->roles()->sync([]);
            foreach($request->input('roles') as $role_id){
              $role = \App\Role::findOrFail($role_id);
              $user->attachRole($role);
            }
            if($user->save()){
                return \Redirect::to('auth/user');
            }

          }
      }

      /**
       * Remove the specified resource from storage.
       *
       * @param  int  $id
       * @return Response
       */
      public function destroy($id)
      {
          //
          $user = \App\User::find($id);
          $user->delete();
          return \Redirect::to('auth/user');
      }

}
