<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmsPower extends Model
{
    protected $table = 'tms_t_p_power';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'import_id', 'power_name', 'power_number'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
