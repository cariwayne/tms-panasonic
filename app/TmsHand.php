<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmsHand extends Model
{
    protected $table = 'tms_t_h_hand';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'import_id', 'hvalue', 'hvalue_en'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
