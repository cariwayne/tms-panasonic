<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmsEmpWorkSchedule extends Model
{
    protected $table = 'tms_emp_workschedule';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'import_id', 'reference_type', 'reference', 'workschedule_id', 'workschedule_date'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
