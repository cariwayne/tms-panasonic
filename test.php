SET foreign_key_checks = 0;# MySQL returned an empty result set (i.e. zero rows).


TRUNCATE `tms_emp_group`;# MySQL returned an empty result set (i.e. zero rows).

Truncate `tms_door_record_raw`;# MySQL returned an empty result set (i.e. zero rows).


TRUNCATE `tms_emp_workschedule`;# MySQL returned an empty result set (i.e. zero rows).



TRUNCATE `tms_sync`;# MySQL returned an empty result set (i.e. zero rows).


TRUNCATE `tms_t_c_cardpower`;# MySQL returned an empty result set (i.e. zero rows).


TRUNCATE `tms_workschedule`;# MySQL returned an empty result set (i.e. zero rows).

SET foreign_key_checks = 1;# MySQL returned an empty result set (i.e. zero rows).

---

SET foreign_key_checks = 0;

TRUNCATE `tms_emp_group`;

Truncate `tms_door_record_raw`;

Truncate `tms_emp_badge_card`;

Truncate `tms_employee`;

Truncate `tms_emp_department`;

INSERT INTO `tms_emp_department` (`id`, `import_id`, `code`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(0, NULL, 'No Department', 'No Department', NULL, NULL, NULL);
UPDATE `tms`.`tms_emp_department` SET `id` = '0' WHERE `tms_emp_department`.`id` = 1; 


TRUNCATE `tms_emp_workschedule`;


TRUNCATE `tms_sync`;

TRUNCATE `tms_t_c_cardpower`;

TRUNCATE `tms_workschedule`;

SET foreign_key_checks = 1;

