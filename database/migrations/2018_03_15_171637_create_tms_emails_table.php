<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmsEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tms_emails', function (Blueprint $table) {
          $table->increments('id');
          $table->string('emp_emails')->unique()->nullable();
          $table->string('emp_reports')->index();
          $table->string('emp_times')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tms_emails');
    }
}