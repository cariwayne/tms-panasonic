<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 
        Schema::create('tms_emp_plant', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('import_id')->unique()->nullable();
            $table->string('code')->index();
            $table->string('description');
            $table->timestamps();
            $table->softDeletes(); 
        });


        Schema::create('tms_emp_department', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('import_id')->unique()->nullable();
            $table->string('code')->index();
            $table->string('description');
            $table->timestamps();
            $table->softDeletes(); 
        });


        Schema::create('tms_emp_group', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('import_id')->unique()->nullable();
            $table->string('code')->index();
            $table->string('description');
            $table->timestamps();
            $table->softDeletes(); 
        });


        Schema::create('tms_workschedule', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('import_id')->unique()->nullable();
            $table->string('code')->index();
            $table->string('description');
            $table->time('time_in');
            $table->time('time_in_from');
            $table->time('time_in_to');
            $table->time('time_out');
            $table->time('time_out_from');
            $table->time('time_out_to');
            $table->boolean('next_day');
            $table->timestamps();
            $table->softDeletes(); 
        });


        Schema::create('tms_employee', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('import_id')->unique()->nullable();
            $table->string('emp_no')->unique();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->integer('plant_id')->unsigned()->nullable();
            $table->integer('department_id')->unsigned()->nullable();
            $table->integer('group_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes(); 
        }); 
        Schema::table('tms_employee', function (Blueprint $table) {
            $table->foreign('plant_id')
                  ->references('id')
                  ->on('tms_emp_plant')
                  ->onDelete('cascade');
            $table->foreign('department_id')
                  ->references('id')
                  ->on('tms_emp_department')
                  ->onDelete('cascade');
            $table->foreign('group_id')
                  ->references('id')
                  ->on('tms_emp_group')
                  ->onDelete('cascade');
        });
       
        Schema::create('tms_emp_workschedule', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('import_id')->unique()->nullable();
            $table->enum('reference_type', ['emp_no', 'plant_code', 'department_code', 'group_code']);
            $table->string('reference_no')->index();
            $table->integer('workschedule_id')->unsigned();
            $table->date('workschedule_date'); 
            $table->timestamps();
            $table->softDeletes(); 
        });
        Schema::table('tms_emp_workschedule', function (Blueprint $table) {
            $table->foreign('workschedule_id')
                  ->references('id')
                  ->on('tms_workschedule')
                  ->onDelete('cascade');
        }); 
  
        Schema::create('tms_emp_badge_card', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('import_id')->unique()->nullable();
            $table->string('emp_no')->index();
            $table->string('card_no')->index();
            $table->timestamps();
            $table->softDeletes(); 
        });

        Schema::create('tms_door_record_raw', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('import_id')->unique()->nullable();
            $table->string('card_no')->index();
            $table->string('door_no')->index();
            $table->string('controller_no')->index();
            $table->datetime('record_time')->index();
            $table->integer('record_state');
            $table->integer('open_type');
            $table->integer('pass_flag');
            $table->integer('hand_value');
            $table->integer('lfeet_value');
            $table->integer('rfeet_value');
            $table->timestamps();
            $table->softDeletes(); 
        });
        Schema::create('tms_t_h_hand', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('import_id')->unique()->nullable();
            $table->string('hvalue');
            $table->string('hvalue_en');
            $table->timestamps();
            $table->softDeletes(); 
        });
        Schema::create('tms_t_p_power', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('import_id')->unique()->nullable();
            $table->string('power_name');
            $table->string('power_number');
            $table->timestamps();
            $table->softDeletes(); 
        });
        Schema::create('tms_t_c_cardpower', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('import_id')->unique()->nullable();
            $table->string('card_no')->index();
            $table->string('door_no')->index();
            $table->string('controller_no')->index();
            $table->string('door_begin_zone');
            $table->string('door_end_zone');
            $table->string('card_door_zone');
            $table->string('card_door_state');
            $table->string('report_state');
            $table->string('remarks');
            $table->timestamps();
            $table->softDeletes(); 
        });
        
        Schema::create('tms_sync', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference')->unique();
            $table->string('last_record')->index();
            $table->timestamps();
            $table->softDeletes(); 
        });

        Schema::create('tms_access_controller', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('import_id')->unique()->nullable();
            $table->string('controller_no')->unique();
            $table->string('controller_name');
            $table->string('controller_ip')->unique();
            $table->dateTime('last_online');
            $table->timestamps();
            $table->softDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('tms_emp_workschedule', function(Blueprint $table)
        {
            $table->dropForeign('tms_emp_workschedule_workschedule_id_foreign');
        });
        Schema::table('tms_employee', function(Blueprint $table)
        {
            $table->dropForeign('tms_employee_plant_id_foreign');
            $table->dropForeign('tms_employee_department_id_foreign');
            $table->dropForeign('tms_employee_group_id_foreign');
        });
        Schema::drop('tms_emp_plant');
        Schema::drop('tms_emp_department');
        Schema::drop('tms_emp_group');
        Schema::drop('tms_workschedule');
        Schema::drop('tms_employee');
        Schema::drop('tms_emp_workschedule');
        Schema::drop('tms_emp_badge_card');
        Schema::drop('tms_door_record_raw');
        Schema::drop('tms_t_h_hand');
        Schema::drop('tms_t_p_power');
        Schema::drop('tms_t_c_cardpower');
        Schema::drop('tms_sync');
        Schema::drop('tms_access_controller');
        
    }
}
