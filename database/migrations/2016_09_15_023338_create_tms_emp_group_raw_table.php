<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmsEmpGroupRawTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //


        Schema::create('tms_emp_group_raw', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('import_id')->unique()->nullable();
            $table->string('group_code')->index();
            $table->string('emp_no')->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('tms_emp_group_raw');
    }
}
