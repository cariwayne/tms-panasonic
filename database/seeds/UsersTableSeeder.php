<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'caritech',
            'email' => 'support@caritech.com',
            'password' => bcrypt('caritech'),
        ]);

        // Roles Initialise
        $admin = new \App\Role();
        $admin->name         = 'admin';
        $admin->display_name = 'User Administrator'; // optional
        $admin->description  = 'User is allowed to access to all functions in this system';
        $admin->save();

        $qa = new \App\Role();
        $qa->name         = 'QA';
        $qa->display_name = 'Quality Assurance'; // optional
        $qa->description  = 'User is allowed to access to ESD functions in this system';
        $qa->save();

        $hr = new \App\Role();
        $hr->name         = 'HR';
        $hr->display_name = 'Human Resource'; // optional
        $hr->description  = 'User is allowed to access to HR functions in this system';
        $hr->save();

        // Permission Initialise
        $perm_hrms_report = new \App\Permission();
        $perm_hrms_report->name         = 'access.report.hrms';
        $perm_hrms_report->display_name = 'Access HRMS Report'; // optional
        $perm_hrms_report->description  = 'Access HRMS Report'; // optional
        $perm_hrms_report->save();

        $perm_esd_report = new \App\Permission();
        $perm_esd_report->name         = 'access.report.esd';
        $perm_esd_report->display_name = 'Access ESD Report'; // optional
        $perm_esd_report->description  = 'Access ESD Report'; // optional
        $perm_esd_report->save();

        $perm_maintenance = new \App\Permission();
        $perm_maintenance->name         = 'access.maintenance';
        $perm_maintenance->display_name = 'Access System Maintenance'; // optional
        $perm_maintenance->description  = 'Access System Maintenance'; // optional
        $perm_maintenance->save();


        // Roles Permisssion Initialise
        $admin->attachPermissions(array($perm_hrms_report, $perm_esd_report, $perm_maintenance));

        $caritech = \App\User::where('name', '=', 'caritech')->first();
        $caritech->attachRole($admin); // parameter can be an Role object, array, or id

        // $qa = \App\Role::where('name', '=', 'QA')->first();
        $sh = \App\User::where('name', '=', 'Sook Huey, Cheang')->first();
        $sh->attachRole($qa);

        // $hr = \App\Role::where('name', '=', 'HR')->first();
        $cm = \App\User::where('name', '=', 'CM, Wong')->first();
        $cm->attachRole($hr);
    }
}
