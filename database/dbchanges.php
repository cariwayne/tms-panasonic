
CREATE TABLE IF NOT EXISTS `tms_emp_attendance_group` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `import_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tms_emp_group_import_id_unique` (`import_id`),
  KEY `tms_emp_group_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `tms_employee` ADD `attendance_group_id` INT NOT NULL AFTER `group_id`;

CREATE TABLE IF NOT EXISTS `tms_access_controller_attendance_group` (
  `controller_id` int(11) NOT NULL,
  `attendance_group_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`controller_id`,`attendance_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS  `tms_interplant_log` (
  `id` int(11) NOT NULL,
  `batch` int(11) NOT NULL,
  `controller_id` int(11) DEFAULT NULL,
  `controller_ip` varchar(50) DEFAULT NULL,
  `record_time` datetime DEFAULT NULL,
  `card_no` varchar(255) DEFAULT NULL,
  `pass` tinyint(1) DEFAULT NULL,
  `sync_datetime` datetime DEFAULT NULL,
  `sync_request_ip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `tms_door_record_raw` ADD INDEX `card_no_record_time` (`card_no`, `record_time`);

ALTER TABLE `tms_interplant_log` ADD PRIMARY KEY(`id`);
ALTER TABLE `tms_interplant_log` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES (NULL, 'access.employee', 'Access Employee', 'Access Employee', '2016-07-12 15:44:06', '2016-07-12 15:44:06');

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES (NULL, 'access.report.interplant', 'Access Interplant Report', 'Access Interplant Report', '2016-07-12 15:44:06', '2016-07-12 15:44:06')

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES (NULL, 'access.report.graph', 'Access Graph Report', 'Access Graph Report', '2016-07-12 15:44:06', '2016-07-12 15:44:06')


ALTER TABLE `tms_door_record_raw` ADD `emp_no` VARCHAR(50) NULL AFTER `card_no`;
ALTER TABLE `tms_door_record_raw` ADD INDEX(`emp_no`);

---
UPDATE mysql.user SET Password=PASSWORD('Hottemp123') WHERE User='root'
FLUSH PRIVILEGES;

In Phpmyadmin, just change this line on config.inc.php

$cfg['Servers'][$i]['auth_type'] = 'config';
to
$cfg['Servers'][$i]['auth_type'] = 'cookie';