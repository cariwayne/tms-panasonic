@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">Users</div>
        <div class="panel-body">
          <form class="form-horizontal" role="form" method="GET" action="">
            {!! csrf_field() !!}
            <div class="row">
                <div class="col-md-11 form-group">
                </div>
                <div class="col-md-1 form-group text-right">
                    <a href="{{ url('auth/user/create')}}" class="right btn btn-primary">
                        New
                    </a>
                </div>
            </div>
          </form>
          <div class="table-responsive">
            <table class="table table-bordered table-hover table-condensed">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Roles</th>
                  <th colspan="2">Options</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($tbl_records as $record)
                  <tr>
                    <td>{{ $record->name }}</td>
                    <td>{{ $record->email }}</td>
                    <td>{{ $record->roles->implode('name', ', ') }}</td>
                    <td><a class="btn btn-success" href="{{ url('auth/user/'.$record->id.'/edit') }}">Edit</a></td>
                    <td>
                      <form class='form-inline form-delete'  action="{{ url('auth/user/'.$record->id) }}" method="post">
                        {!! method_field('delete') !!}
                        {!! csrf_field() !!}
                        <input data-toggle="modal" data-target="#modalDeleteConfirmation" class="btn btn-danger" type="submit" value="Delete">
                      </form>
                    </td>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            @if (COUNT($tbl_records)>0)
              {!!
                  $tbl_records->appends([])->render()
              !!}
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_js')
<script>
$('table[data-form="deleteForm"]').on('click', '.form-delete', function(e){
    e.preventDefault();
    var $form=$(this);
    $('#confirm').modal({ backdrop: 'static', keyboard: false })
        .on('click', '#delete-btn', function(){
            $form.submit();
        });
});
</script>
@endsection
