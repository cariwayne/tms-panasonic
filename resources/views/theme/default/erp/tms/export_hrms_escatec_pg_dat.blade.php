<?php
 
  header('Content-Description: File Transfer');
  header('Content-Type: application/octet-stream');
  header('Content-Disposition: attachment; filename='.$title.'.dat'); 
  header('Content-Transfer-Encoding: binary');
  header('Connection: Keep-Alive');
  header('Expires: 0');
  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  header('Pragma: public'); 
?>@foreach ($tbl_records as $record)
{{ $record->door_no }}{{ date('dmyHi',strtotime(( $record->record_time ))) }}{{ sprintf('%06s',$record->card_no) }}000{!! ($record->record_state==1)?'<':'>' !!}
@endforeach