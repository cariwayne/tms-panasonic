<?php
  header('Content-Description: File Transfer');
  header('Content-Type: application/octet-stream');
  header('Content-Disposition: attachment; filename='.$title.'.xls');
  header('Content-Transfer-Encoding: binary');
  header('Connection: Keep-Alive');
  header('Expires: 0');
  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  header('Pragma: public');
?><html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=us-ascii">
<meta name=ProgId content=Excel.Sheet>
<!--[if gte mso 9]><xml>
 <o:OfficeDocumentSettings>
  <o:DownloadComponents/>
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<style>
<!--table
   {mso-displayed-decimal-separator:"\.";
   mso-displayed-thousand-separator:"\,";}
@page
   {margin:1.0in .75in 1.0in .75in;
   mso-header-margin:.5in;
   mso-footer-margin:.5in;}
tr
   {mso-height-source:auto;}
col
   {mso-width-source:auto;}
br
   {mso-data-placement:same-cell;}
.style0
   {mso-number-format:General;
   text-align:general;
   vertical-align:bottom;
   white-space:nowrap;
   mso-rotate:0;
   mso-background-source:auto;
   mso-pattern:auto;
   color:windowtext;
   font-size:10.0pt;
   font-weight:400;
   font-style:normal;
   text-decoration:none;
   font-family:Arial;
   mso-generic-font-family:auto;
   mso-font-charset:0;
   border:none;
   mso-protection:locked visible;
   mso-style-name:Normal;
   mso-style-id:0;}
td
   {mso-style-parent:style0;
   padding-top:1px;
   padding-right:1px;
   padding-left:1px;
   mso-ignore:padding;
   color:windowtext;
   font-size:10.0pt;
   font-weight:400;
   font-style:normal;
   text-decoration:none;
   font-family:Arial;
   mso-generic-font-family:auto;
   mso-font-charset:0;
   mso-number-format:General;
   text-align:general;
   vertical-align:bottom;
   border:none;
   mso-background-source:auto;
   mso-pattern:auto;
   mso-protection:locked visible;
   white-space:nowrap;
   mso-rotate:0;}
.xl24
   {mso-style-parent:style0;
   white-space:normal;}
-->
</style>
<!--[if gte mso 9]><xml>
 <x:ExcelWorkbook>
  <x:ExcelWorksheets>
   <x:ExcelWorksheet>
   <x:Name>Results</x:Name>
   <x:WorksheetOptions>
    <x:Selected/>
    <x:ProtectContents>False</x:ProtectContents>
    <x:ProtectObjects>False</x:ProtectObjects>
    <x:ProtectScenarios>False</x:ProtectScenarios>
   </x:WorksheetOptions>
   </x:ExcelWorksheet>
  </x:ExcelWorksheets>
  <x:WindowHeight>10005</x:WindowHeight>
  <x:WindowWidth>10005</x:WindowWidth>
  <x:WindowTopX>120</x:WindowTopX>
  <x:WindowTopY>135</x:WindowTopY>
  <x:ProtectStructure>False</x:ProtectStructure>
  <x:ProtectWindows>False</x:ProtectWindows>
 </x:ExcelWorkbook>
</xml><![endif]-->
</head>
<body>

<table class="table table-bordered table-hover table-condensed">
<thead>
  <tr>
    <tr>
      <th>Direction</th>
      <th>Record Date & Time</th>
      <th>Card No</th>
      <th>Employee No</th>
      <th>Employee Name</th>
      <th>Shift</th>
      <th>Department</th>
      <th>Attendance</th>
      <th>Location</th>
    </tr>
  </tr>
</thead>
<tbody>
    @foreach ($tbl_records as $record)
      <tr class="{{ ($record->record_state==1)?'row-in':'row-out' }}">
        <td>{{ ($record->record_state==1)?"IN":"OUT" }}</td>
        <td x:str="{{ $record->record_time }}">{{ $record->record_time }}</td>
        <td x:str="{{ $record->card_no }}">{{ $record->card_no }}</td>
        <td x:str="{{ $record->emp_no }}">{{ $record->emp_no }}</td>
        <td>{{ $record->employee_name }}</td>
        <td>{{ $record->shift_code }}</td>
        <td>{{ $record->department_code }}</td>
		<td>{{ $record->attendance_group_code }}</td>
        <td x:str="{{ $record->controller_name }}">{{ $record->controller_name }}</td>
        </td>
      </tr>
    @endforeach
</tbody>
</table>

</body>
</html>