@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">Access Controller</div>
        <div class="panel-body">

                <div class="row">
                    <div class="col-md-12 form-group">
                        <label class="col-md-2 control-label">Controller</label>
                        <div class="col-md-10">
                        {{ $controller->controller_name }} -
                        {{ $controller->controller_ip }}
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <label class="col-md-2 control-label">Return Msg</label>
                        <div class="col-md-10">
                            <textarea cols="100" rows="10" id="return-msg"></textarea>
                        </div>
                    </div>


                    <div class="col-md-12 form-group">
                        <label class="col-md-2 control-label"></label>
                        <div class="col-md-10">
                            <a href="{{ url('tms/accesscontroller') }}" class="btn btn-primary">Back</a>
                        </div>
                    </div>
                </div>

            @if(isset($post['id']))
                <form id="deleteform" style="display:none;" action="{{ route('tms.accesscontroller.destroy', $post['id']) }}" method="POST">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="submit" value="Delete">
                </form>
            @endif
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_js')
<script>
  $(document).ready(function(){
	$('#return-msg').html("");
    $.ajax({
        'method':'GET',
        'dataType': 'json',
        'url': '{{ url("/tms/accesscontroller/".$controller->id."/updateEmpData") }}',
        'complete': function(msg){
            console.log(msg);
            $('#return-msg').html(msg.responseJSON.return +"\n\n"+ $('#return-msg').html());
        }
    });
    $.ajax({
        'method':'GET',
        'dataType': 'json',
        'url': '{{ url("/tms/accesscontroller/".$controller->id."/updateEmpCardData") }}',
        'complete': function(msg){
            console.log(msg);
            $('#return-msg').html(msg.responseJSON.return +"\n\n"+ $('#return-msg').html());
        }
    });
  });

  $('.datepicker').daterangepicker({
        showDropdowns: true,
        timePicker: true,
        locale: {
          // format: 'DD/MM/YYYY H:mm'
          format: 'DD/MM/YYYY H:mm'
        },
    },
    function(start, end, label) {
        // var years = moment().diff(start, 'years');
        // console.log("You are " + years + " years old.");
    });
</script>
@endsection