@extends('layouts.app')

@section('content')

        <div class="panel-body">
          <form class="form-horizontal" role="form" method="GET" action="{{ url('/tms/report/graph/pie') }}">
            {!! csrf_field() !!}
            <div class="row">
              <div class="col-md-3 form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                <label class="col-md-2 control-label">Date</label>
                <div class="col-md-10">
                  <input type="text" class="form-control datepicker" name="date" value="{{ request('date') }}">
                  @if ($errors->has('date'))
                      <span class="help-block">
                          <strong>{{ $errors->first('date') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <?php /*
              <div class="col-md-2 form-group">
                <label class="col-md-4 control-label">CardNo</label> 
                <div class="col-md-8">
                   <input type="text" class="form-control" name="card_no" value="{{ request('card_no') }}">
                </div>
              </div>
              <div class="col-md-2 form-group">
                <label class="col-md-4 control-label">EmpNo</label> 
                <div class="col-md-8">
                   <input type="text" class="form-control" name="emp_no" value="{{ request('emp_no') }}">
                </div>
              </div>
              <div class="col-md-2 form-group">
                <label class="col-md-4 control-label">DoorNo</label> 
                <div class="col-md-8">
                   <input type="text" class="form-control" name="door_no" value="{{ request('door_no') }}">
                </div>
              </div>
              */?>
              <div class="col-md-3 form-group">
                <div class="">
                  <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-search"></i> Search
                  </button>
                    <a class="btn btn-default" onclick="toggleFull()">Full Screen
                      <i class="fa fa-btn fa-arrows-alt"></i> 
                    </a>
                </div>
              </div>
            </form>
          </div>

<div class="outer">
  <div class="middle">
    <div class="inner">
      <div class="col-md-12">
        <div id="piechart"></div>
      </div>
    </div>
  </div>
</div> 
<script>
// $.getScript("http://localhost/tms/public/assets/widget/highcharts.js")
/*$.getScript("{{ asset('/assets/widget/highcharts.js') }}")
.fail(function( jqxhr, settings, exception ) {
console.log(exception);
})
.done(function( script, textStatus ) {*/

/**/
$(function(){
  $('#piechart').highcharts({
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Panasonic Automotive Systems Malaysia Sdn Bhd<br><br>Total Employees Passing Rate',
      useHTML:true
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b><br>Total: {point.y:.f}'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f}%',
          style: {
            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
          }
        }
      }
    },
    series: [{
      name: 'Passing Rate',
      colorByPoint: true,
      data: [
      <?php 
      $pass = 0;
      $fail = 0;
      ?>
      @foreach ($tbl_records_total as $record)
      <?php
        if($record->pass_flag == 1){ 
          $pass += $record->cnt;
        }else{
          $fail += $record->cnt;
        } 
      ?>   
      @endforeach
// pie chart data shown here
{!! '{ name:"Pass", color:"#ADFF2F", y:'.$pass.'},' !!}
{!! '{ name:"Fail", color:"#FF4500", y:'.$fail.'},' !!}
]
}]
});
});
</script>
@endsection

@section('custom_js')
<script>

  function cancelFullScreen(el) {
    var requestMethod = el.cancelFullScreen||el.webkitCancelFullScreen||el.mozCancelFullScreen||el.exitFullscreen;    $('body').removeClass('fullscreen');
if (requestMethod) { // cancel full screen.
  requestMethod.call(el);    
} else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
  var wscript = new ActiveXObject("WScript.Shell");
  if (wscript !== null) {
    wscript.SendKeys("{F11}");
  }
}
}

function requestFullScreen(element) {
// Supports most browsers and their versions.
var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullscreen;
$('body').addClass('fullscreen');
if (requestMethod) { // Native full screen.
  requestMethod.call(element);
} else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
  var wscript = new ActiveXObject("WScript.Shell");
  if (wscript !== null) {
    wscript.SendKeys("{F11}");
  }
}
} 
function toggleFull() {
var elem = document.body; // Make the body go full screen.
var isInFullScreen = (document.fullScreenElement && document.fullScreenElement !== null) ||  (document.mozFullScreen || document.webkitIsFullScreen);

if (isInFullScreen) {
  cancelFullScreen(document);
} else {
  requestFullScreen(elem);
}
return false;
}

$('.datepicker').daterangepicker({
  showDropdowns: true,
  locale: {
// format: 'DD/MM/YYYY H:mm'
format: 'DD/MM/YYYY'
},  
},
function(start, end, label) {
// var years = moment().diff(start, 'years');
// console.log("You are " + years + " years old.");
});

$(function(){
  setInterval(function(){ location.reload(); }, 1000 * 60 * 5);
})
</script>
@endsection