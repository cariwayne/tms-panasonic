@extends('layouts.app')

@section('content')


<div class="container">

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">Graph Report</div>
        <div class="panel-body">
          <form class="form-horizontal" role="form" method="GET" action="{{ url('/tms/report/graph/search') }}">
            {!! csrf_field() !!}
            <div class="row">
              <div class="col-md-3 form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                <label class="col-md-2 control-label">Date</label>
                <div class="col-md-10">
                  <input type="text" class="form-control datepicker" name="date" value="{{ request('date') }}">
                  @if ($errors->has('date'))
                      <span class="help-block">
                          <strong>{{ $errors->first('date') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="col-md-2 form-group">
                <label class="col-md-4 control-label">CardNo</label> 
                <div class="col-md-8">
                   <input type="text" class="form-control" name="card_no" value="{{ request('card_no') }}">
                </div>
              </div>
              <div class="col-md-2 form-group">
                <label class="col-md-4 control-label">EmpNo</label> 
                <div class="col-md-8">
                   <input type="text" class="form-control" name="emp_no" value="{{ request('emp_no') }}">
                </div>
              </div>
              <div class="col-md-2 form-group">
                <label class="col-md-4 control-label">DoorNo</label> 
                <div class="col-md-8">
                   <input type="text" class="form-control" name="door_no" value="{{ request('door_no') }}">
                </div>
              </div>
              <div class="col-md-3 form-group">
                <div class="">
                  <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-search"></i> Search
                  </button>
                  <?php if(isset($_GET['date'])){ ?>
                    <a class="btn btn-default" onclick="toggleFull()" id="btn-fullscreen">Full Screen
                      <i class="fa fa-btn fa-arrows-alt"></i> 
                    </a> 
                  <?php } ?>
                </div>
              </div>
            </form>
          </div>
          <div class="table-responsive" id="fullscreen-contents">
            <div class="col-md-4">
              <div id="piechart"></div>
            </div>
            <div class="col-md-8">
              <div id="barchart"></div>
            </div>
            <!--<script src="{{ asset('/assets/js/highcharts.js') }}"></script>-->
      <?php
        $BASE_URL = url('');
      ?>
            <script>
            $.getScript("{{$BASE_URL}}/assets/widget/highcharts.js")
            .fail(function( jqxhr, settings, exception ) {
               console.log(exception);
            })
            .done(function( script, textStatus ) {
               $('#piechart').highcharts({
                      chart: {
                          plotBackgroundColor: null,
                          plotBorderWidth: null,
                          plotShadow: false,
                          type: 'pie'
                      },
                      title: {
                          text: 'Total Employees Passing Rate'
                      },
                      tooltip: {
                          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b><br>Total: {point.y:.f}'
                      },
                      plotOptions: {
                          pie: {
                              allowPointSelect: true,
                              cursor: 'pointer',
                              dataLabels: {
                                  enabled: true,
                                  format: '<b>{point.name}</b>: {point.percentage:.1f}%',
                                  style: {
                                      color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                  }
                              }
                          }
                      },
                      series: [{
                          name: 'Passing Rate',
                          colorByPoint: true,
                          data: [
                          <?php 
                              $pass = 0;
                              $fail = 0;
                          ?>
                          @foreach ($tbl_records as $record)
                            <?php
                              if($record->pass_flag == 1){ 
                                $pass += $record->cnt;
                              }else{
                                $fail += $record->cnt;
                              } 
                            ?>   
                          @endforeach
                          {!! '{ name:"Pass", y:'.$pass.'},' !!}
                          {!! '{ name:"Fail", y:'.$fail.'},' !!}
                         ]
                      }]
                  });


                   /**/
                  $('#barchart').highcharts({
                      chart: {
                          plotBackgroundColor: null,
                          plotBorderWidth: null,
                          plotShadow: false,
                          type: 'column'
                      },
                      title: {
                          text: 'Individual Line Employee Passing Rate'
                      },
                      tooltip: {
                          pointFormat: '{series.name}: <b>{point.y:.f}</b>'
                      },
                      xAxis: {
                          categories: [
                            <?php $group_by = ''; ?>
                            @foreach ($tbl_records as $record)
                              <?php 
                              if($record->department_name != $group_by){ 
                                $group_by = $record->department_name; ?>
                                '{!! $record->department_name !!}',
                              <?php } ?>
                            @endforeach
                          ],
                          crosshair: true
                      },
                      yAxis: {
                          min: 0,
                          title: {
                              text: 'Number of test'
                          }
                      },
            plotOptions: {
            series: {
              stacking: 'normal',
              minPointLength:3,
              dataLabels: {
                enabled: true,
                allowOverlap:true,
              }
            }
            },
                      series: [{
                          name: 'Pass',
                          data: [
                          @foreach ($tbl_records as $record)
                            <?php if($record->pass_flag == 1){ ?>
                              {!!  $record->cnt.',' !!}
                            <?php } ?>
                          @endforeach
                         ]
                      },{
                          name: 'Fail',
                          data: [
                          @foreach ($tbl_records as $record)
                            <?php if($record->pass_flag == 0){ ?>
                              {!!  $record->cnt.',' !!}
                            <?php } ?>
                          @endforeach
                         ]
                      }]
                  });

              });
            </script>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_js')
<script>

  function cancelFullScreen(el) {
      var requestMethod = el.cancelFullScreen||el.webkitCancelFullScreen||el.mozCancelFullScreen||el.exitFullscreen;    $('body').removeClass('fullscreen');
      if (requestMethod) { // cancel full screen.
          requestMethod.call(el);    
      } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
          var wscript = new ActiveXObject("WScript.Shell");
          if (wscript !== null) {
              wscript.SendKeys("{F11}");
          }
      }
  }

  function requestFullScreen(element) {
    // Supports most browsers and their versions.
    var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullscreen;
    $('body').addClass('fullscreen');
    if (requestMethod) { // Native full screen.
        requestMethod.call(element);
    } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
        var wscript = new ActiveXObject("WScript.Shell");
        if (wscript !== null) {
            wscript.SendKeys("{F11}");
        }
    }
  } 
  function toggleFull() {
      var elem = document.body; // Make the body go full screen.
      var isInFullScreen = (document.fullScreenElement && document.fullScreenElement !== null) ||  (document.mozFullScreen || document.webkitIsFullScreen);
    
      if (isInFullScreen) {
          cancelFullScreen(document);
      } else {
          requestFullScreen(elem);
      }
      return false;
  }

  $('.datepicker').daterangepicker({
        showDropdowns: true,
        locale: {
          // format: 'DD/MM/YYYY H:mm'
          format: 'DD/MM/YYYY'
        },  
    },
    function(start, end, label) {
        // var years = moment().diff(start, 'years');
        // console.log("You are " + years + " years old.");
    });
</script>
@endsection