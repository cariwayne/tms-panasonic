@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div id="block-import-accdb" class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                  IMPORT <span title="{{$source_accdb}}">ACCDB</span>
                  <img class="loading" src="{{ asset('assets/img/loading.gif') }}" />
                </div>
                <div class="panel-body">
                  <p id="last-sync-import-accdb">Last Sync: <small  class="pull-right">Loading..</small></p>
                  <p>
                    <b>T_D_DoorRecord</b><br />
                    Source: {{ $T_D_DoorRecord }} <br>
                    Sync: <span id="T_D_DoorRecord_sync">{{ $T_D_DoorRecord_sync }}</span>
                  </p>
                  <p>
                    <b>T_C_CardInfo</b><br />
                    Source: {{ $T_C_CardInfo }} <br>
                    Sync: <span id="T_C_CardInfo_sync">{{ $T_C_CardInfo_sync }}</span>
                  </p>
                  <p>
                    <b>T_C_CardPower</b><br />
                    Source: {{ $T_C_CardPower }} <br>
                    Sync: <span id="T_C_CardPower_sync">{{ $T_C_CardPower_sync }}</span>
                  </p>
                  <p>
                    <b>T_P_Power</b><br />
                    Source: {{ $T_P_Power }} <br>
                    Sync: <span id="T_P_Power_sync">{{ $T_P_Power_sync }}</span>
                  </p>
                  <p style="display:none">
                    <b>Hand</b><br />
                    Source: {{ $T_H_Hand }} <br>
                    Sync: <span id="T_H_Hand_sync">{{ $T_H_Hand_sync }}</span>
                  </p>
                </div>
            </div>
        </div>
		<?php /*
        <div id="block-import-hrms" class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                  IMPORT DBF
                  <img class="loading" src="{{ asset('assets/img/loading.gif') }}" />
                </div>
                <div class="panel-body">
                  <p id="last-sync-import-hrms">Last Sync: <small  class="pull-right">Loading..</small></p>
                  <p>
                    <b title="tms_emp_workschedule">TMDS.DBF</b><br />
                    Source: {{ $TMDS }} <br>
                    Sync: <span id="TMDS_sync">{{ $TMDS_sync }}</span>
                  </p>
                  <p>
                    <b title="tms_emp_group">TMEMPGP.DBF</b><br />
                    Source: {{ $TMEMPGP }} <br>
                    Sync: <span id="TMEMPGP_sync">{{ $TMEMPGP_sync }}</span>
                  </p>
                  <p>
                    <b title="tms_workschedule">TMSCHTYP.DBF</b><br />
                    Source: {{ $TMSCHTYP }} <br>
                    Sync: <span id="TMEMPGP_sync">{{ $TMSCHTYP_sync }}</span>
                  </p>
                </div>
            </div>
        </div>
		*/?>
<?php /*        <div id="block-ping" class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Ping
                  <img class="loading" src="{{ asset('assets/img/loading.gif') }}" />
                </div>
                <div class="panel-body">
                  <p id="last-sync-ping">Last Sync: <small class="pull-right">Loading..</small></p>
                  @foreach($controllers as $controller)
                  <p>
                    <b>{{ $controller->controller_no }} - {{ $controller->controller_name }}</b><br />
                    {{ $controller->controller_ip }}
                  </p>
                  @endforeach
                </div>
            </div>
        </div>
*/?>

        <div id="block-reset-sync" class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                  ReSync
                  <img class="loading" src="{{ asset('assets/img/loading.gif') }}" style="display:none;" />
                </div>
                <div class="panel-body">
                  <form action="/" id="resync_attendance_schedule">
                    {!! csrf_field() !!}
                    
                    <div class="checkbox">
                      <label><input type="checkbox"  name="reset[TmsEmpBadgeCard,TmsEmployee]" /> T_C_CardInfo (Employee Master)</label>
                    </div>
					
					<!--
					<div class="checkbox">
                      <label><input type="checkbox"  name="reset[TmsEmpWorkSchedule]" /> TMDS (Emp Daily WorkSchedule)</label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox"  name="reset[TmsEmpGroup]" /> TMEMPGP (Employee Group)</label>
                    </div>
					<div class="checkbox">
                      <label><input type="checkbox"  name="reset[TmsWorkSchedule]" /> TMSCHTYP (WorkSchedule)</label>
                    </div>
					-->
                    
					<hr />
                    <div class="checkbox">
                      <label><input type="checkbox"  name="reset[TmsDoorRecordRaw]" /> T_D_DoorRecord (Door Record)</label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox"  name="reset[TmsCardPower]" /> T_C_CardPower (Emp ESD Test Method)</label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox"  name="reset[TmsPower]" /> T_P_Power (ESD Test Method)</label>
                    </div>
                    
                    <div><a onclick="resync()" class="btn btn-primary">Reset</a></div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script>

  var sync_accdb = setTimeout("func_sync_accdb()", 5000);

  function func_sync_accdb(){
    $('#block-import-accdb .loading').show();
    var url = '{{ url("tms/importdata/process/accdb") }}';
    $.getJSON(url,
      function (data) {
        $('#block-import-accdb .loading').hide();
        $('#T_D_DoorRecord_sync').html(data.tms_door_record_raw);
        $('#T_C_CardInfo_sync').html(data.tms_emp_badge_card);
        $('#T_C_CardPower_sync').html(data.tms_t_c_cardpower);
        $('#T_H_Hand_sync').html(data.tms_t_h_hand);
        $('#T_P_Power_sync').html(data.tms_t_p_power);
        $('#last-sync-import-accdb small').html(data.last_sync);

        sync_accdb = setTimeout("func_sync_accdb()", 5000);
    })
    .error(function() {
        $('#block-import-accdb .loading').hide();
        $('#last-sync-import-accdb small').html("Error!");
        sync_accdb = setTimeout("func_sync_accdb()", 5000);
    });
    return false;
  }

  
  var sync_hrms = setTimeout("func_sync_hrms()", 5000);

  function func_sync_hrms(){
	  return '';
    $('#block-import-hrms .loading').show();
    var url = '{{ url("tms/importdata/process/hrms") }}';
    $.getJSON(url,
      function (data) {
        $('#block-import-hrms .loading').hide();
        $('#TMDS_sync').html(data.tms_emp_workschedule);
        $('#TMEMPGP_sync').html(data.tms_emp_group);
        $('#TMSCHTYP_sync').html(data.tms_workschedule);
        $('#last-sync-import-hrms small').html(data.last_sync);

        sync_hrms = setTimeout("func_sync_hrms()", 5000);
    })
    .error(function() {
        $('#block-import-hrms .loading').hide();
        $('#last-sync-import-hrms small').html("Error!");
        sync_hrms = setTimeout("func_sync_hrms()", 5000);
    });
    return false;
  }

  var sync_ping = setTimeout("func_sync_ping()", 30000);

  function func_sync_ping(){
    $('#block-ping .loading').show();
    var url = '{{ url("tms/importdata/process/ping") }}';
    $.getJSON(url,
      function (data) {
        $('#block-ping .loading').hide();
        $('#last-sync-ping small').html(data.last_sync);
        sync_ping = setTimeout("func_sync_ping()", 30000);
    })
    .error(function() {
        $('#block-ping .loading').hide();
        $('#last-sync-ping small').html("Error!");
        sync_ping = setTimeout("func_sync_ping()", 30000);
    });
    return false;
  }

  function auto_resync(){
      var date = new Date();

      if(date.getHours()  == '0' && date.getMinutes() == '0'){
        clearTimeout(sync_accdb);
        clearTimeout(sync_hrms);
        $('#block-reset-sync .loading').show();
          $.ajax({
              type: "POST",
              url: "{{ url('tms/importdata/process/reset') }}",
              data: 'reset%5BTmsEmpWorkSchedule%5D=on&reset%5BTmsEmpBadgeCard%2CTmsEmployee%5D=on&reset%5BTmsEmpGroup%5D=on&reset%5BTmsCardPower%5D=on',
              success: function( data ) {

                window.location = "{{ url('tms/importdata') }}";
              },
              dataType: 'json'
          });
      }
  }
  setInterval("auto_resync()", 1000);

  function resync(){
    clearTimeout(sync_accdb);
    clearTimeout(sync_hrms);
    $('#block-reset-sync .loading').show();

    $.ajax({
      type: "POST",
      url: "{{ url('tms/importdata/process/reset') }}",
      data: $("#resync_attendance_schedule").serialize(),
      success: function( data ) {
        console.log(data);
        window.location = "{{ url('tms/importdata') }}";
      },
      dataType: 'json'
    });
    return false;
  }
</script>
@endsection
