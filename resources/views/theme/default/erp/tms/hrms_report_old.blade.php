@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">{{ $page_title }}</div>
        <div class="panel-body">
          <form class="form-horizontal" role="form" method="GET" action="{{ $form_url }}">
            {!! csrf_field() !!}
            <div class="row">
              <div class="col-md-3 form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                <label class="col-md-2 control-label">Date</label>
                <div class="col-md-10">
                  <input type="text" class="form-control datepicker" name="date" value="{{ request('date') }}">
                  @if ($errors->has('date'))
                      <span class="help-block">
                          <strong>{{ $errors->first('date') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="col-md-2 form-group">
                <label class="col-md-6 control-label">Card No</label>
                <div class="col-md-6">
                   <input type="text" class="form-control" name="card_no" value="{{ request('card_no') }}">
                </div>
              </div>
              <div class="col-md-2 form-group">
                <label class="col-md-6 control-label">Emp No</label>
                <div class="col-md-6">
                   <input type="text" class="form-control" name="emp_no" value="{{ request('emp_no') }}">
                </div>
              </div>
              <div class="col-md-3 form-group">
                 <label class="col-md-5 control-label">Department</label>
                <div class="col-md-7">
                   <input type="text" class="form-control" name="department" value="{{ request('department') }}">
                </div>
              </div>
              <div class="col-md-2 form-group">
                 <label class="col-md-5 control-label">Plant</label>
                <div class="col-md-7">
                    {{ Form::select(
                        'plant_id',
                        [''=>'']+$plant->all(),
                        request('plant_id'),
                        ['class'=>'form-control'])
                    }}
                </div>
              </div>
              <div class="col-md-1 form-group" style="padding-right: 0px">
                <div class="">
                  <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-search"></i>
                  </button>
                  <?php if(isset($_GET['date'])){ ?>
                    <a class="btn btn-default" onclick="window.location='{{ $export_url }}?'+window.location.href.split('?')[1]">
                      <i class="fa fa-btn fa-download"></i>
                    </a>
                  <?php } ?>
                </div>
              </div>
            </form>
          </div>
          <div class="table-responsive">
            <table class="table table-bordered table-hover table-condensed">
              <thead>
                <tr>
                  <th>Direction</th>
                  <th>Record Date & Time</th>
                  <th>Card No</th>
                  <th>Employee No</th>
                  <th>Employee Name</th>
                  <th>Shift</th>
                  <th>Department</th>
                  <th>Controller</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($tbl_records as $record)
                  <tr class="{{ ($record->record_state==1)?'row-in':'row-out' }}">
                    <td>{{ ($record->record_state==1)?"IN":"OUT" }}</td>
                    <td>{{ $record->record_time }}</td>
                    <td>{{ $record->card_no }}</td>
                    <td>{{ $record->emp_no }}</td>
                    <td>{{ $record->employee_name }}</td>
                    <td>{{ $record->shift_code }}</td>
                    <td>{{ $record->department_code }}</td>
                    <td>{{ $record->controller_name }}</td>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            @if (COUNT($tbl_records)>0)
              {!!
                  $emps->appends([
                    'date'=>request('date'),
                    'card_no'=>request('card_no'),
                    'emp_no'=>request('emp_no'),
                    'department'=>request('department')
                  ])->render()
              !!}
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_js')
<script>
  $('.datepicker').daterangepicker({
        showDropdowns: true,
        timePicker: true,
        locale: {
          // format: 'DD/MM/YYYY H:mm'
          format: 'DD/MM/YYYY H:mm'
        },
    },
    function(start, end, label) {
        // var years = moment().diff(start, 'years');
        // console.log("You are " + years + " years old.");
    });
</script>
@endsection