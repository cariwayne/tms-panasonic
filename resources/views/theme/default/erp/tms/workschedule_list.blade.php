@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">Workschedule</div>
        <div class="panel-body">
          <form class="form-horizontal" role="form" method="GET" action="{{ url('/tms/workschedule') }}">
            {!! csrf_field() !!}
            <div class="row">

              <div class="col-md-1 ">
                   <input type="text" class="form-control" name="code" placeholder="Code" value="{{ request('code') }}">
              </div>
              <div class="col-md-1  form-group">
                <input type="text" class="form-control" name="time_in_from" placeholder="Time In" value="{{ request('time_in_from') }}">
              </div>
              <div class="col-md-1 ">
                <input type="text" class="form-control" name="time_out_to" placeholder="Time Out" value="{{ request('time_out_to') }}">
              </div>
              <div class="col-md-2 checkbox">
                <label>
                   <input type="checkbox" name="next_day" {{ request('next_day')?'checked':'' }} value="1">
                   Next Day
                </label>
              </div>
              <div class="col-md-3 form-group">
                <div class="pull-right">
                  <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-search"></i>
                  </button>

                </div>
              </div>
            </form>
          </div>
          <div class="table-responsive">
            <table class="table table-bordered table-hover table-condensed">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Code</th>
                  <th>Description</th>
                  <th>Time in From</th>
                  <th>Time out to</th>
                  <th>Next Day</th>
                  <th colspan="2">Options</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                @foreach ($tbl_records as $record)
                  <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $record->code }}</td>
                    <td>{{ $record->description }}</td>
                    <td>{{ $record->time_in_from }}</td>
                    <td>{{ $record->time_out_to }}</td>
                    <td>{{ $record->next_day }}</td>
                    <td><a class="btn btn-success" href="{{ url('tms/workschedule/'.$record->id.'/edit') }}">Edit</a></td>
                    <td>
                      <form class='form-inline form-delete'  action="{{ url('tms/workschedule/'.$record->id) }}" method="post">
                        {!! method_field('delete') !!}
                        {!! csrf_field() !!}
                        <input data-toggle="modal" data-target="#modalDeleteConfirmation" class="btn btn-danger" type="submit" value="Delete">
                      </form>
                    </td>
                    </td>
                  </tr>
                <?php $i++; ?>
                @endforeach

              </tbody>
            </table>
            @if (COUNT($tbl_records)>0)
              {!!
                  $tbl_records->appends([])->render()
              !!}
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_js')
<script>
  $('.datepicker').daterangepicker({
        showDropdowns: true,
        locale: {
          // format: 'DD/MM/YYYY H:mm'
          format: 'DD/MM/YYYY'
        },
    },
    function(start, end, label) {
        // var years = moment().diff(start, 'years');
        // console.log("You are " + years + " years old.");
    });
</script>
@endsection
