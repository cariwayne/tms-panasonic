@extends('layouts.app')

@section('content')

        <div class="panel-body">
          <form class="form-horizontal" role="form" method="GET" action="{{ url('/tms/report/graph/line') }}">
            {!! csrf_field() !!}
            <div class="row">
              <div class="col-md-3 form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                <label class="col-md-2 control-label">Date</label>
                <div class="col-md-10">
                  <input type="text" class="form-control datepicker" name="date" value="{{ request('date') }}">
                  @if ($errors->has('date'))
                      <span class="help-block">
                          <strong>{{ $errors->first('date') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <?php /*
              <div class="col-md-2 form-group">
                <label class="col-md-4 control-label">CardNo</label> 
                <div class="col-md-8">
                   <input type="text" class="form-control" name="card_no" value="{{ request('card_no') }}">
                </div>
              </div>
              <div class="col-md-2 form-group">
                <label class="col-md-4 control-label">EmpNo</label> 
                <div class="col-md-8">
                   <input type="text" class="form-control" name="emp_no" value="{{ request('emp_no') }}">
                </div>
              </div>
              <div class="col-md-2 form-group">
                <label class="col-md-4 control-label">DoorNo</label> 
                <div class="col-md-8">
                   <input type="text" class="form-control" name="door_no" value="{{ request('door_no') }}">
                </div>
              </div>
              */?>
              <div class="col-md-3 form-group">
                <div class="">
                  <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-search"></i> Search
                  </button>
                    <a class="btn btn-default" onclick="toggleFull()">Full Screen
                      <i class="fa fa-btn fa-arrows-alt"></i> 
                    </a>
                </div>
              </div>
            </form>
          </div>

          <div class="table-responsive">
            <div class="col-md-12">
              <div id="linechart"></div>
            </div>
          </div>

            <script>
            // $.getScript("http://localhost/tms/public/assets/widget/highcharts.js")
            /*$.getScript("{{ asset('/assets/widget/highcharts.js') }}")
            .fail(function( jqxhr, settings, exception ) {
               console.log(exception);
            })
            .done(function( script, textStatus ) {*/
               
                   /**/
              <?php
              $job = [];
              $group_by = '';
              foreach ($tbl_records_line as $record){  
                if($record->job_name != $group_by){ 
                  $group_by = $record->job_name;  
                  $job[$record->job_name] = ['fail'=>0,'pass'=>0,'not-tested'=>0];
                }
                if($record->pass_flag == 1){
                  $job[$record->job_name]['pass'] = $record->cnt;
                } else if($record->pass_flag == 2){
                  $job[$record->job_name]['not-tested'] = $record->cnt;
                }else{
                  $job[$record->job_name]['fail'] = $record->cnt;
                }
              }

              ?>     
              $(document).ready(function(){
                Highcharts.chart('linechart', {
                  chart: {
                    type: 'column'
                  },
                  title: {
                    text: 'Panasonic Automotive Systems Malaysia Sdn Bhd<br><br>Production Employee Passing Rate',
                    useHTML:true
                  },
                  xAxis: {
                      categories: [
                        "{!! implode('","', array_keys($job)) !!}"
                      ],
                      crosshair: true
                  },
                  yAxis: {
                    min: 0,
                    title: {
                      text: 'Number of Test'
                    }
                  },
                  tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                      '<td style="padding:0"><b>{point.y}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                  },

                  series: [{
                      name: 'Pass',
                      borderWidth: 0,
                      color: '#579000',
                      dataLabels: {
                        enabled: true,
                        format: '({point.y})',
                        color: '#579000',
                        style:{
                          textShadow: false
                        }
                      },
                      data: [
                        @foreach ($job as $record) 
                            {!!  $record['pass'].',' !!} 
                        @endforeach
                     ]
                  },{
                      name: 'Fail',
                      borderWidth: 0,
                      color: '#FF4500',
                      dataLabels: {
                        enabled: true,
                        format: '({point.y})',
                        color: '#FF4500',
                        style:{
                          textShadow: false
                        }
                      },
                      data: [
                        @foreach ($job as $record) 
                            {!!  $record['fail'].',' !!} 
                        @endforeach
                     ]
                  },{
                      name: 'Not tested',
                      borderWidth: 0,
                      color: '#848484',
                      dataLabels: {
                        enabled: true,
                        format: '({point.y})',
                        color: '#848484', 
                        style:{
                          textShadow: false
                        }
                      },
                      data: [
                        @foreach ($job as $record) 
                            {!!  $record['not-tested'].',' !!} 
                        @endforeach
                     ]
                  }]
                });
              });
            </script>
@endsection

@section('custom_js')
<script type="text/javascript">

  $('.datepicker').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    timePicker: true,
    locale: {
      format: 'DD/MM/YYYY H:mm'
    },
  }),

   function cancelFullScreen(el) {
      var requestMethod = el.cancelFullScreen||el.webkitCancelFullScreen||el.mozCancelFullScreen||el.exitFullscreen;    $('body').removeClass('fullscreen');
  if (requestMethod) { // cancel full screen.
    requestMethod.call(el);    
  } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
    var wscript = new ActiveXObject("WScript.Shell");
    if (wscript !== null) {
      wscript.SendKeys("{F11}");
    }
  }
  }

  function requestFullScreen(element) {
  // Supports most browsers and their versions.
  var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullscreen;
  $('body').addClass('fullscreen');
  if (requestMethod) { // Native full screen.
    requestMethod.call(element);
  } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
    var wscript = new ActiveXObject("WScript.Shell");
    if (wscript !== null) {
      wscript.SendKeys("{F11}");
    }
  }
  } 
  function toggleFull() {
  var elem = document.body; // Make the body go full screen.
  var isInFullScreen = (document.fullScreenElement && document.fullScreenElement !== null) ||  (document.mozFullScreen || document.webkitIsFullScreen);

  if (isInFullScreen) {
    cancelFullScreen(document);
  } else {
    requestFullScreen(elem);
  }
  return false;
  }

  $(function(){
    setInterval(function(){ 
      $.ajax({
          url: '{{ url("tms/report/graph/line/ajax") }}'
      }).done(function(data){

          var x_axis = [];
          var x_axis_group_by = '';
          var job = [];
          var pass_flag = [];
          var fail_flag = [];
          var not_tested = [];

          for(x in data){
            if(data[x].job_name != x_axis_group_by){
              x_axis_group_by = data[x].job_name;
              x_axis.push( data[x].job_name);
              job[data[x].job_name] = {'pass':0, 'fail':0, 'nottested':0 };
            }
            if(data[x].pass_flag == 1){
              job[data[x].job_name].pass = data[x].cnt;
            }else if(data[x].pass_flag == 2){
              job[data[x].job_name].nottested = data[x].cnt;
            }else{
              job[data[x].job_name].fail = data[x].cnt;
            }
          }
          for( x in job){
            pass_flag.push(job[x].pass);
            not_tested.push(job[x].nottested);
            fail_flag.push(job[x].fail);
          }
          Highcharts.chart('linechart', {
            chart: {
              type: 'column'
            },
            title: {
              text: 'Panasonic Automotive Systems Malaysia Sdn Bhd<br><br>Production Employee Passing Rate',
              useHTML:true
            },
            xAxis: {
                categories: x_axis,
                crosshair: true
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Number of Test'
              }
            },
            tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
            },

            series: [{
                name: 'Pass',
                borderWidth: 0,
                color: '#579000',
                dataLabels: {
                  enabled: true,
                  format: '({point.y})',
                  color: '#579000',
                  style:{
                    textShadow: false
                  }
                },
                data: pass_flag
            },{
                name: 'Fail',
                borderWidth: 0,
                color: '#FF4500',
                dataLabels: {
                  enabled: true,
                  format: '({point.y})',
                  color: '#FF4500',
                  style:{
                    textShadow: false
                  }
                },
                data: fail_flag
            },{
                name: 'Not tested',
                borderWidth: 0,
                color: '#848484',
                dataLabels: {
                  enabled: true,
                  format: '({point.y})',
                  color: '#848484',
                  style:{
                    textShadow: false
                  }
                },
                data: not_tested
            }]
          });
      });
    }, 1000 * 60 * 1);
  })

</script>
@endsection