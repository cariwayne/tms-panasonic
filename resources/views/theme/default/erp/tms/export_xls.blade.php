<?php
  header('Content-Description: File Transfer');
  header('Content-Type: application/octet-stream');
  header('Content-Disposition: inline; filename='.$title.'.xls');
  header('Content-Transfer-Encoding: binary');
  header('Connection: Keep-Alive');
  header('Expires: 0');
  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  header('Pragma: public');
?><html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=us-ascii">
<meta name=ProgId content=Excel.Sheet>
<!--[if gte mso 9]><xml>
 <o:OfficeDocumentSettings>
  <o:DownloadComponents/>
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<style>
<!--table
   {mso-displayed-decimal-separator:"\.";
   mso-displayed-thousand-separator:"\,";}
@page
   {margin:1.0in .75in 1.0in .75in;
   mso-header-margin:.5in;
   mso-footer-margin:.5in;}
tr
   {mso-height-source:auto;}
col
   {mso-width-source:auto;}
br
   {mso-data-placement:same-cell;}
.style0
   {mso-number-format:General;
   text-align:general;
   vertical-align:bottom;
   white-space:nowrap;
   mso-rotate:0;
   mso-background-source:auto;
   mso-pattern:auto;
   color:windowtext;
   font-size:10.0pt;
   font-weight:400;
   font-style:normal;
   text-decoration:none;
   font-family:Arial;
   mso-generic-font-family:auto;
   mso-font-charset:0;
   border:none;
   mso-protection:locked visible;
   mso-style-name:Normal;
   mso-style-id:0;}
td
   {mso-style-parent:style0;
   padding-top:1px;
   padding-right:1px;
   padding-left:1px;
   mso-ignore:padding;
   color:windowtext;
   font-size:10.0pt;
   font-weight:400;
   font-style:normal;
   text-decoration:none;
   font-family:Arial;
   mso-generic-font-family:auto;
   mso-font-charset:0;
   mso-number-format:General;
   text-align:general;
   vertical-align:bottom;
   border:none;
   mso-background-source:auto;
   mso-pattern:auto;
   mso-protection:locked visible;
   white-space:nowrap;
   mso-rotate:0;}
.xl24
   {mso-style-parent:style0;
   white-space:normal;}
-->
</style>
<!--[if gte mso 9]><xml>
 <x:ExcelWorkbook>
  <x:ExcelWorksheets>
   <x:ExcelWorksheet>
   <x:Name>Results</x:Name>
   <x:WorksheetOptions>
    <x:Selected/>
    <x:ProtectContents>False</x:ProtectContents>
    <x:ProtectObjects>False</x:ProtectObjects>
    <x:ProtectScenarios>False</x:ProtectScenarios>
   </x:WorksheetOptions>
   </x:ExcelWorksheet>
  </x:ExcelWorksheets>
  <x:WindowHeight>10005</x:WindowHeight>
  <x:WindowWidth>10005</x:WindowWidth>
  <x:WindowTopX>120</x:WindowTopX>
  <x:WindowTopY>135</x:WindowTopY>
  <x:ProtectStructure>False</x:ProtectStructure>
  <x:ProtectWindows>False</x:ProtectWindows>
 </x:ExcelWorkbook>
</xml><![endif]-->
</head>
<body>
<table class="table table-bordered table-hover table-condensed">
<thead>
  <tr>
    <th colspan="12">Panasonic Automotive Systems Malaysia Sdn Bhd</th>
  </tr>
  <tr><th></th></tr>
  <tr>
    <th>Record Date & Time</th>
    <th>Card No</th>
    <th>Employee No</th>
    <th>Employee Name</th>
    <th>Line</th>
    <th>Wrist Strap</th>
    <th>Left Foot</th>
    <th>Right Foot</th>
    <th>Result</th>
    <th>Test Method</th>
    <th>Controller</th>
    <th>Direction</th>
  </tr>
</thead>
<tbody>
  @foreach ($tbl_records as $record)
    <?php
    if($record->pass_flag == '1'){
        $record->pass_flag_name = 'Pass';
        $record->pass_flag_class = 'pass';
    } elseif($record->pass_flag == '0') {
        if($record->open_type == '255'){
            $record->pass_flag_name = 'Invalid Card';
            $record->pass_flag_class = 'fail';
        }else if($record->open_type == '2'){
            $record->pass_flag_name = 'Fail';
            $record->pass_flag_class = 'fail';
        }else{
            $record->pass_flag_name = 'Fail';
            $record->pass_flag_class = 'fail';
        }
    } else{
        $record->pass_flag_name = 'Not tested';
        $record->pass_flag_class = 'not-tested';
    }
    if($record->record_state==1){
        $direction = 'IN';
    }else if($record->record_state==2){
        $direction = 'OUT';
    }else{
        $direction = '';
    }
    ?>
    <tr>
        <td x:str="{{ $record->record_time }}">{{ $record->record_time }}</td>
        <td x:str="{{ $record->card_no }}">{{ $record->card_no }}</td>
        <td x:str="{{ $record->emp_no }}">{{ $record->emp_no }}</td>
        <td>{{ $record->emp_name }}</td>
        <td>{{ $record->job }}</td>
        <td>{{ $record->hvalue_en }}</td>
        <td>{{ $record->lhvalue_en }}</td>
        <td>{{ $record->rhvalue_en }}</td>
        <td>{{ $record->pass_flag_name }}</td>
        <td>{{ $record->power_name }}</td>
        <td x:str="{{ $record->controller_name }}">{{ $record->controller_name }}</td>
        <td>{{ $direction }} </td>
        </td>
    </tr>
  @endforeach
</tbody>
</table>
</body>
</html>