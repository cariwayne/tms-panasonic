@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">{{ $page_title }}</div>
        <div class="panel-body">
          <form class="form-horizontal" role="form" method="GET" action="{{ $form_url }}">
            {!! csrf_field() !!}
            <div class="form-group">
                <div class="col-md-3  {{ $errors->has('date') ? ' has-error' : '' }}">
                    <input type="text" class="form-control datepicker" placeholder="Date.." name="date" value="{{ request('date') }}">
                </div>
                <div class="col-md-7 row">
                    <div class="col-md-2 row">
                       <input type="text" class="form-control" placeholder="Card No.." name="card_no" value="{{ request('card_no') }}">
                    </div>
                    <div class="col-md-2 ">
                       <input type="text" class="form-control" placeholder="Emp No.." name="emp_no" value="{{ request('emp_no') }}">
                    </div>
                    <div class="col-md-8 row">
                        <div class="col-md-4 row">
                            <input type="text" class="form-control" placeholder="Department.." name="department" value="{{ request('department') }}">
                        </div>
                       <div class="col-md-3 ">
                        {{ Form::select(
                            'plant_id',
                            [''=>'Plant']+$plant->all(),
                            request('plant_id'),
                            ['class'=>'form-control'])
                        }}
                        </div>
                        <div class="col-md-3 ">
                        {{ Form::select(
                            'pass',
                            [''=>'Result', '1'=>'Pass', '0'=>'Invalid'],
                            request('pass'),
                            ['class'=>'form-control'])
                        }}
                        </div>

                    </div>
                </div>
                <div class="col-md-2 text-right pull-right">
                    <div class="">
                      <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-search"></i>
                      </button>
                      <?php if(isset($_GET['date'])){ ?>
                        <a class="btn btn-default" onclick="window.location='{{ $export_url }}?'+window.location.href.split('?')[1]">
                          <i class="fa fa-btn fa-download"></i>
                        </a>
                      <?php } ?>
                    </div>
                </div>
            </form>
          </div>
          <div class="table-responsive">
            <table class="table table-bordered table-hover table-condensed">
              <thead>
                <tr>
                  <th>Record Date & Time</a></th>
                  <th>Card No</th>
                  <th>Employee No</th>
                  <th>Employee Name</th>
                  <th>Department</th>
                  <th>Result</th>
                  <th>Controller</th>
                  <th>Time Difference</th>
                  <th>Alert</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($tbl_records as $record)
                  <?php

                  /*
                  if($record->emp_no != ''){
                    $record->pass_flag_name = 'Pass';
                    $record->pass_flag_class = 'pass';
                  } else {
                    $record->pass_flag_name = 'Invalid';
                    $record->pass_flag_class = 'fail';
                  }*/

                  if(!isset($pass_card_no) || $pass_card_no == '' || $pass_card_no != $record->card_no){
                    $pass_record_time = '';
                    $pass_card_no = $record->card_no;
                  }

                  if(!isset($pass_record_time) || $pass_record_time == ''){
                    $pass_record_time = $record->record_time;
                    $record_time_diff = '';
					$record_time_diff_mins = '';
                  } else {
                    $c_pass_record_time = new \Carbon\Carbon($pass_record_time);
                    $c_current_record_time = new \Carbon\Carbon($record->record_time);

                    if($c_pass_record_time->format('Ymd') != $c_current_record_time->format('Ymd')){
                      $pass_record_time = $record->record_time;
                      $record_time_diff = '';
					  $record_time_diff_mins = '';
                    }else{
                      $pass_record_time = $record->record_time;
                      $record_time_diff = $c_current_record_time->diffForHumans($c_pass_record_time);
                      $record_time_diff_mins = $c_current_record_time->diffInMinutes($c_pass_record_time);
                    }
                  }
				  
                  if($record->pass == '1'){
                    $record->pass_flag_name = 'Pass';
                    $record->pass_flag_class = 'pass';
                  } else {
                    $record->pass_flag_name = 'Invalid';
                    $record->pass_flag_class = 'warning';
                  }
				  
				  if($record_time_diff_mins > 20){
					$alert = true;
                    $record->pass_flag_class = 'fail';
				  }else {
					$alert = false;
				  }
				  
                  ?>
                  <tr class="{{ $record->pass_flag_class }}">
                    <td>{{ $record->record_time }}</td>
                    <td>{{ $record->card_no }}</td>
                    <td>{{ $record->emp_no }}</td>
                    <td>{{ $record->emp_name }}</td>
                    <td>{{ $record->department }}</td>
                    <td>{{ $record->pass_flag_name }}</td>
                    <td>{{ $record->controller_name }}</td>
                    <td>{{ $record_time_diff }}</td>
                    <td>{!! $alert?'<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>':'' !!}</td>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            @if (COUNT($tbl_records)>0)
              {!!
                  $tbl_records->appends([
                    'date'=>request('date'),
                    'card_no'=>request('card_no'),
                    'emp_no'=>request('emp_no'),
                    'department'=>request('department'),
                    'sort'=>request('sort'),
                    'order'=>request('order')
                  ])->render()
              !!}
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_js')
<script>
  $('.datepicker').daterangepicker({
    @if(url('tms/report/esd/status') == $form_url)
        singleDatePicker: true,
    @endif
        showDropdowns: true,
        timePicker: true,
        locale: {
          // format: 'DD/MM/YYYY H:mm'
          format: 'DD/MM/YYYY H:mm'
        },
    });
</script>
@endsection
