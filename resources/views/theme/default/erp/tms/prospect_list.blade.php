@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">HRMS Report</div>
        <div class="panel-body">
          <form class="form-horizontal" role="form" method="GET" action="{{ url('/tms/report/hrms') }}">
            {!! csrf_field() !!}
            <div class="row">
              <div class="col-md-3 form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                <label class="col-md-2 control-label">Date</label>
                <div class="col-md-10">
                  <input type="text" class="form-control datepicker" name="date" value="{{ request('date') }}">
                  @if ($errors->has('date'))
                      <span class="help-block">
                          <strong>{{ $errors->first('date') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="col-md-2 form-group">
                <label class="col-md-4 control-label">CardNo</label> 
                <div class="col-md-8">
                   <input type="text" class="form-control" name="card_no" value="{{ request('card_no') }}">
                </div>
              </div>
              <div class="col-md-2 form-group">
                <label class="col-md-4 control-label">EmpNo</label> 
                <div class="col-md-8">
                   <input type="text" class="form-control" name="emp_no" value="{{ request('emp_no') }}">
                </div>
              </div>
              <div class="col-md-2 form-group">
                <label class="col-md-4 control-label">DoorNo</label> 
                <div class="col-md-8">
                   <input type="text" class="form-control" name="door_no" value="{{ request('door_no') }}">
                </div>
              </div>
              <div class="col-md-3 form-group">
                <div class="">
                  <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-search"></i> Search
                  </button> 
                   
                </div>
              </div> 
            </form>
          </div>
          <div class="table-responsive">
            <table class="table table-bordered table-hover table-condensed">
              <thead>
                <tr>
                  <th>DoorNO</th>
                  <th>RecordTime</th>
                  <th>CardNO</th>
                  <th>EmployeeNo.</th>
                  <th>EmployeeName</th>
                  <th>Shift.</th>
                  <th>RecordState</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($tbl_records as $record)
                  <tr>
                    <td>{{ $record->name }}</td>
                    <td>{{ $record->phone }}</td>
                    <td>{{ $record->email }}</td>
                    <td>{{ $record->number }}</td>
                    <td><a href="prospect/update/{{ $record->id }}">Edit</a></td>
                     
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
             
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_js')
<script>
  $('.datepicker').daterangepicker({
        showDropdowns: true,
        timePicker: true,
        locale: {
          // format: 'DD/MM/YYYY H:mm'
          format: 'DD/MM/YYYY H:mm'
        },  
    },
    function(start, end, label) {
        // var years = moment().diff(start, 'years');
        // console.log("You are " + years + " years old.");
    });
</script>
@endsection