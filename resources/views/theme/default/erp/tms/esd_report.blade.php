@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">{{ $page_title }}</div>
        <div class="panel-body">
          <form class="form-horizontal" role="form" method="GET" action="{{ $form_url }}">
            {!! csrf_field() !!}
            <div class="form-group">
                <div class="col-md-3  {{ $errors->has('date') ? ' has-error' : '' }}">
                    <input type="text" class="form-control datepicker" placeholder="Date.." name="date" value="{{ request('date')!=''?request('date'):date('d/m/Y H:i') }}">
                </div>
                <div class="col-md-7 row">
                    <div class="col-md-2 row">
                       <input type="text" class="form-control" placeholder="Card No.." name="card_no" value="{{ request('card_no') }}">
                    </div>
                    <div class="col-md-2 ">
                       <input type="text" class="form-control" placeholder="Emp No.." name="emp_no" value="{{ request('emp_no') }}">
                    </div>
                    <div class="col-md-8 row">
                        <div class="col-md-4 row">
                          {{ Form::select(
                              'job',
                              [''=>'Line']+$job->all(),
                              request('job'),
                              ['class'=>'form-control'])
                          }}
                            <!--<input type="text" class="form-control" placeholder="Line" name="job" value="{{ request('job') }}">-->
                        </div>

                        <div class="col-md-3" style="width: 150px;">
                        {{ Form::select(
                            'department',
                            [''=>'Department']+$department->all(),
                            request('department'),
                            ['class'=>'form-control'])
                        }}
                        </div>

                       <div class="col-md-3 row">
                        {{ Form::select(
                            'test_result',
                            [''=>'All', '-1'=>'Not Tested', '0'=>'Fail', '1'=>'Pass'],
                            request('test_result'),
                            ['class'=>'form-control'])
                        }}
                        </div>
                        <!--<div class="col-md-1 checkbox">
                            <label ><input type="checkbox" name="out" value="1" {{ (request('out'))?'checked="checked"':''  }}> Out</label>
                        </div>-->
                    </div>
                </div>
                <div class="col-md-2 text-right pull-right">
                    <div class="">
                      <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-search"></i>
                      </button>
                      <?php if(isset($_GET['date'])){ ?>
                        <a class="btn btn-default" onclick="window.location='{{ $export_url }}?'+window.location.href.split('?')[1]">
                          <i class="fa fa-btn fa-download"></i>
                        </a>
                      <?php } ?>
                    </div>
                </div>
            </form>
          </div>
          <div class="table-responsive">
            <table class="table table-bordered table-hover table-condensed">
              <thead>
                <tr>
                  <th>@sortablelink('dr.record_time', 'Record Date & Time')</a></th>
                  <th>@sortablelink('dr.card_no', 'Card No')</th>
                  <th>@sortablelink('e.emp_no', 'Employee No')</th>
                  <th>@sortablelink('e.name', 'Employee Name')</th>
                  <th>@sortablelink('e.job', 'Line')</th>
                    <!-- Reverse left foot and wriststrap col due to Access Control Software -->
                  <th>@sortablelink('lh.hvalue_en', 'Wrist Strap')</th>
                  <th>@sortablelink('h.hvalue_en', 'Left Foot')</th>
                  <th>@sortablelink('rh.hvalue_en', 'Right Foot')</th>
                  <th>@sortablelink('dr.pass_flag', 'Result')</th>
                  <!--<th>@sortablelink('p.power_name', 'Test Method')</th>-->
                  <th>@sortablelink('ac.controller_name', 'Controller')</th>
                  <!--<th>@sortablelink('dr.record_state', 'Direction')</th>-->
                  <th>@sortablelink('dr.record_state', 'Status')</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($tbl_records as $record)
                  <?php
                  if($record->pass_flag == '1'){
                    $record->pass_flag_name = 'Pass';
                    $record->pass_flag_class = 'pass';
                  } elseif($record->pass_flag == '0') {
                    if($record->open_type == '255'){
                        $record->pass_flag_name = 'Invalid Card';
                        $record->pass_flag_class = 'fail';
                    }else if($record->open_type == '2'){
                        $record->pass_flag_name = 'Fail';
                        $record->pass_flag_class = 'fail';
                    }else{
                        $record->pass_flag_name = 'Fail';
                        $record->pass_flag_class = 'fail';
                    }
                  } else{
                    $record->pass_flag_name = 'Not tested';
                    $record->pass_flag_class = 'not-tested';
                  }
                  if($record->record_state==1){
                    $direction = 'IN';
                  }else if($record->record_state==2){
                    $direction = 'OUT';
                  }else{
                    $direction = '';
                  }
                  ?>
                  <tr class="{{ $record->pass_flag_class }}">
                    <td>{{ $record->record_time }}</td>
                    <td>{{ $record->card_no }}</td>
                    <td>{{ $record->emp_no }}</td>
                    <td>{{ $record->emp_name }}</td>
                    <td>{{ $record->job }}</td>
                    <!-- Reverse left foot and wriststrap col due to Access Control Software -->
                    <td>{{ $record->lhvalue_en }}</td>
                    <td>{{ $record->hvalue_en }}</td>
                    <td>{{ $record->rhvalue_en }}</td>
                    <td>{{ $record->pass_flag_name }}</td>
                    <!--<td>{{ $record->power_name }}</td>-->
                    <td>{{ $record->controller_name }}</td>
                    <td>{{ $direction }} </td>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            @if (COUNT($tbl_records)>0)
              {!!
                  $tbl_records->appends(Request::except('page'))->render()
              !!}
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_js')
<script>
  $('.datepicker').daterangepicker({
    @if(url('tms/report/esd/status') == $form_url)
        singleDatePicker: true,
    @endif
        showDropdowns: true,
        timePicker: true,
        locale: {
          // format: 'DD/MM/YYYY H:mm'
          format: 'DD/MM/YYYY H:mm'
        },
    });
</script>
@endsection
