@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">{{ $page_title }}</div>
        <div class="panel-body">
          <form class="form-horizontal" role="form" method="GET" action="{{ $form_url }}">
            {!! csrf_field() !!}
            <div class="form-group">
                <div class="col-md-3  {{ $errors->has('date') ? ' has-error' : '' }}">
                    <input type="text" class="form-control datepicker" placeholder="Date.." name="date" value="{{ request('date') }}">
                </div>
                <div class="col-md-7 row">
                    <div class="col-md-2 row">
                       <input type="text" class="form-control" placeholder="Card No.." name="card_no" value="{{ request('card_no') }}">
                    </div>
                    <div class="col-md-2 ">
                       <input type="text" class="form-control" placeholder="Emp No.." name="emp_no" value="{{ request('emp_no') }}">
                    </div>
                    <div class="col-md-8 row">
                        <div class="col-md-4 row">
                            <input type="text" class="form-control" placeholder="Department.." name="department" value="{{ request('department') }}">
                        </div>
                       <div class="col-md-3 ">
                        {{ Form::select(
                            'plant_id',
                            [''=>'Plant']+$plant->all(),
                            request('plant_id'),
                            ['class'=>'form-control'])
                        }}
                        </div>
                        <div class="col-md-4 checkbox">
                            <label ><input type="checkbox" {{ request('attendance_group') || request('date')==''?'checked':'' }} name="attendance_group">Attendance</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 text-right pull-right">
                    <div class="">
                      <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-search"></i>
                      </button>
                      <?php if(isset($_GET['date'])){ ?>
                        <a class="btn btn-default"  title="Download Dat File" onclick="window.location='{{ $export_url }}/exportdat?'+window.location.href.split('?')[1]">
                          <i class="fa fa-btn fa-download"></i>
                        </a>
                        <a class="btn btn-default" title="Download XLS" onclick="window.location='{{ $export_url }}/exportxls?'+window.location.href.split('?')[1]">
                          <i class="fa fa-btn fa-file-excel-o"></i>
                        </a>
                      <?php } ?>
                    </div>
                </div>
            </form>
          </div>
          <div class="table-responsive">
            <table class="table table-bordered table-hover table-condensed">
              <thead>
                <tr>
                  <th>@sortablelink('tdr.record_state', 'Direction')</th>
                  <th>@sortablelink('tdr.record_time', 'Record Date & Time')</th>
                  <th>@sortablelink('tdr.card_no', 'Card No')</th>
                  <th>@sortablelink('tdr.emp_no', 'Employee No')</th>
                  <th>@sortablelink('tdr.employee_name', 'Employee Name')</th>
                  <th>@sortablelink('tdr.shift_code', 'Shift')</th>
                  <th>@sortablelink('tdr.department_code', 'Department')</th>
                  <th>@sortablelink('tdr.attendance_group_code', 'Attendance')</th>
                  <th>@sortablelink('tdr.controller_name', 'Controller')</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($tbl_records as $record)
                  <tr class="{{ ($record->record_state==1)?'row-in':'row-out' }}">
                    <td>{{ ($record->record_state==1)?"IN":"OUT" }}</td>
                    <td>{{ $record->record_time }}</td>
                    <td>{{ $record->card_no }}</td>
                    <td>{{ $record->emp_no }}</td>
                    <td>{{ $record->employee_name }}</td>
                    <td>{{ $record->shift_code }}</td>
                    <td>{{ $record->department_code }}</td>
                    <td>{{ $record->attendance_group_code }}</td>
                    <td>{{ $record->controller_name }}</td>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            @if (COUNT($tbl_records)>0)
              {!!
                  $tbl_records->appends(Request::except('page'))->render()
              !!}
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_js')
<script>
  $('.datepicker').daterangepicker({
        showDropdowns: true,
        timePicker: true,
        locale: {
          // format: 'DD/MM/YYYY H:mm'
          format: 'DD/MM/YYYY H:mm'
        },
    },
    function(start, end, label) {
        // var years = moment().diff(start, 'years');
        // console.log("You are " + years + " years old.");
    });
</script>
@endsection