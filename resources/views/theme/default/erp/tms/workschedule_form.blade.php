@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">Workschedule</div>
        <div class="panel-body">
        @if(isset($post['id']))
            {!! Form::model($post, ['url' => array('tms/workschedule', $post['id']), 'method' => 'PUT']) !!}
        @else
            {!! Form::open(['url' => 'tms/workschedule']) !!}
        @endif
                <div class="row">
                    <div class="col-md-12 form-group">
                        <label class="col-md-2 control-label">Code</label>
                        <div class="col-md-10">
                            {{ Form::text(
                                'code',
                                null, ['class'=>'form-control'])
                            }}
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <label class="col-md-2 control-label">Description</label>
                        <div class="col-md-10">

                            {{ Form::text(
                                'description',
                                null, ['class'=>'form-control'])
                            }}

                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <label class="col-md-2 control-label">Time In From</label>
                        <div class="col-md-10">

                            {{ Form::text(
                                'time_in_from',
                                null, ['class'=>'form-control'])
                            }}
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <label class="col-md-2 control-label">Time Out To</label>
                        <div class="col-md-10">
                            {{ Form::text(
                                'time_out_to',
                                null, ['class'=>'form-control'])
                            }}
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <label class="col-md-2 control-label">Nextday</label>
                        <div class="col-md-10">
                            <label>
                                {{ Form::checkbox('next_day', 1) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <label class="col-md-2 control-label"></label>
                        <div class="col-md-10">
                            <input type="submit" class="btn btn-primary" name="submit" value="Save">
                            @if(isset($post['id']))
                                <a class="btn btn-danger pull-right"  onclick="document.getElementById('deleteform').submit();return false;">Delete</a>
                            @endif
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
            @if(isset($post['id']))
                <form id="deleteform" style="display:none;" action="{{ route('tms.accesscontroller.destroy', $post['id']) }}" method="POST">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="submit" value="Delete">
                </form>
            @endif
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_js')
<script>
  $('.datepicker').daterangepicker({
        showDropdowns: true,
        timePicker: true,
        locale: {
          // format: 'DD/MM/YYYY H:mm'
          format: 'DD/MM/YYYY H:mm'
        },
    },
    function(start, end, label) {
        // var years = moment().diff(start, 'years');
        // console.log("You are " + years + " years old.");
    });
</script>
@endsection