@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">Access Controller</div>
        <div class="panel-body">
          <form class="form-horizontal" role="form" method="GET" action="">
            {!! csrf_field() !!}
            <div class="row">
                <div class="col-md-12 form-group text-right">
                    <a data-toggle="modal" data-target="#pi-push-log" class="right btn btn-warning">
                        PI Push Emp
                    </a>
                    <a href="{{ url('tms/accesscontroller/create')}}" class="right btn btn-primary">
                        New
                    </a>
                </div>
            </div>
          </form>
          <div class="table-responsive">
            <table class="table table-bordered table-hover table-condensed">
              <thead>
                <tr>
                  <th style="width:25px"></th>
                  <th>Controller No</th>
                  <th>Controller Name</th>
                  <th>Controller IP</th>
                  <th>Attendance</th>
                  <th>Options</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($tbl_records as $record)
				  <?php
				    if(strpos(strtolower($record->controller_name), 'interplant') !== false){
						$display_push_btn = true;
					}else{ 
						$display_push_btn = false;
					}
				  ?>
                  <tr>
                    <td> 
					@if($display_push_btn)<input type="checkbox" class="controller-checkbox" name="controller[{{$record->id}}]" value="{{$record->id}}" />@endif</td>
                    <td>{{ $record->controller_no }}</td>
                    <td>{{ $record->controller_name }}</td>
                    <td>{{ $record->controller_ip }}</td>
                    <td>{{ $record->attendance_group->implode('code', ', ') }}</td>
                    <td>
					@if($display_push_btn)
						<a class="btn btn-xs btn-warning" href="{{ url('tms/accesscontroller/'.$record->id.'') }}">PI Push Emp</a>
					@endif
                    <a class="btn btn-xs btn-success" href="{{ url('tms/accesscontroller/'.$record->id.'/edit') }}">Edit</a></td>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            @if (COUNT($tbl_records)>0)
              {!!
                  $tbl_records->appends([
                    'date'=>request('date'),
                    'card_no'=>request('card_no'),
                    'emp_no'=>request('emp_no'),
                    'door_no'=>request('door_no')
                  ])->render()
              !!}
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="pi-push-log" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Pi Push Log</h4>
      </div>
      <div class="modal-body">
        <textarea id="return-msg" cols="78" rows="10"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('custom_js')
<script>
  $('#pi-push-log').on('shown.bs.modal', function () {
        if($('.controller-checkbox:checked').length == 0){
            $('#return-msg').html("Please select a controller from the table list");
        }else{
            $('#return-msg').html("");
            $('.controller-checkbox:checked').each(function(){
                $.ajax({
                    'method':'GET',
                    'dataType': 'json',
                    'url': '{{ url("/tms/accesscontroller") }}/' + $(this).val() + '/updateEmpData',
                    'complete': function(msg){
                        $('#return-msg').html(msg.responseJSON.return +"\n\n"+ $('#return-msg').html());
                    }
                });
                $.ajax({
                    'method':'GET',
                    'dataType': 'json',
                    'url': '{{ url("/tms/accesscontroller") }}/' + $(this).val()+ '/updateEmpCardData',
                    'complete': function(msg){
                        $('#return-msg').html(msg.responseJSON.return +"\n\n"+ $('#return-msg').html());
                    }
                });
            });
        }
  });
  $('.datepicker').daterangepicker({
        showDropdowns: true,
        timePicker: true,
        locale: {
          // format: 'DD/MM/YYYY H:mm'
          format: 'DD/MM/YYYY H:mm'
        },
    },
    function(start, end, label) {
        // var years = moment().diff(start, 'years');
        // console.log("You are " + years + " years old.");
    });
</script>
@endsection