<?php
  header('Content-Description: File Transfer');
  header('Content-Type: application/octet-stream');
  header('Content-Disposition: attachment; filename=export_interplant.xls');
  header('Content-Transfer-Encoding: binary');
  header('Connection: Keep-Alive');
  header('Expires: 0');
  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  header('Pragma: public');
?><html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=us-ascii">
<meta name=ProgId content=Excel.Sheet>
<!--[if gte mso 9]><xml>
 <o:OfficeDocumentSettings>
  <o:DownloadComponents/>
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<style>
<!--table
   {mso-displayed-decimal-separator:"\.";
   mso-displayed-thousand-separator:"\,";}
@page
   {margin:1.0in .75in 1.0in .75in;
   mso-header-margin:.5in;
   mso-footer-margin:.5in;}
tr
   {mso-height-source:auto;}
col
   {mso-width-source:auto;}
br
   {mso-data-placement:same-cell;}
.style0
   {mso-number-format:General;
   text-align:general;
   vertical-align:bottom;
   white-space:nowrap;
   mso-rotate:0;
   mso-background-source:auto;
   mso-pattern:auto;
   color:windowtext;
   font-size:10.0pt;
   font-weight:400;
   font-style:normal;
   text-decoration:none;
   font-family:Arial;
   mso-generic-font-family:auto;
   mso-font-charset:0;
   border:none;
   mso-protection:locked visible;
   mso-style-name:Normal;
   mso-style-id:0;}
td
   {mso-style-parent:style0;
   padding-top:1px;
   padding-right:1px;
   padding-left:1px;
   mso-ignore:padding;
   color:windowtext;
   font-size:10.0pt;
   font-weight:400;
   font-style:normal;
   text-decoration:none;
   font-family:Arial;
   mso-generic-font-family:auto;
   mso-font-charset:0;
   mso-number-format:General;
   text-align:general;
   vertical-align:bottom;
   border:none;
   mso-background-source:auto;
   mso-pattern:auto;
   mso-protection:locked visible;
   white-space:nowrap;
   mso-rotate:0;}
.xl24
   {mso-style-parent:style0;
   white-space:normal;}
-->
</style>
<!--[if gte mso 9]><xml>
 <x:ExcelWorkbook>
  <x:ExcelWorksheets>
   <x:ExcelWorksheet>
   <x:Name>Results</x:Name>
   <x:WorksheetOptions>
    <x:Selected/>
    <x:ProtectContents>False</x:ProtectContents>
    <x:ProtectObjects>False</x:ProtectObjects>
    <x:ProtectScenarios>False</x:ProtectScenarios>
   </x:WorksheetOptions>
   </x:ExcelWorksheet>
  </x:ExcelWorksheets>
  <x:WindowHeight>10005</x:WindowHeight>
  <x:WindowWidth>10005</x:WindowWidth>
  <x:WindowTopX>120</x:WindowTopX>
  <x:WindowTopY>135</x:WindowTopY>
  <x:ProtectStructure>False</x:ProtectStructure>
  <x:ProtectWindows>False</x:ProtectWindows>
 </x:ExcelWorkbook>
</xml><![endif]-->
</head>
<body>
<table class="table table-bordered table-hover table-condensed">
  <thead>
    <tr>
      <th>Record Date & Time</a></th>
      <th>Card No</th>
      <th>Employee No</th>
      <th>Employee Name</th>
      <th>Department</th>
      <th>Result</th>
      <th>Controller</th>
      <th>Time Difference</th>
	  <th>Alert</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($tbl_records as $record)
      <?php 
      /*
	  if($record->emp_no != ''){
		$record->pass_flag_name = 'Pass';
		$record->pass_flag_class = 'pass';
	  } else {
		$record->pass_flag_name = 'Invalid';
		$record->pass_flag_class = 'fail';
	  }*/

	  if(!isset($pass_card_no) || $pass_card_no == '' || $pass_card_no != $record->card_no){
		$pass_record_time = '';
		$pass_card_no = $record->card_no;
	  }

	  if(!isset($pass_record_time) || $pass_record_time == ''){
		$pass_record_time = $record->record_time;
		$record_time_diff = '';
		$record_time_diff_mins = '';
	  } else {
		$c_pass_record_time = new \Carbon\Carbon($pass_record_time);
		$c_current_record_time = new \Carbon\Carbon($record->record_time);

		if($c_pass_record_time->format('Ymd') != $c_current_record_time->format('Ymd')){
		  $pass_record_time = $record->record_time;
		  $record_time_diff = '';
		  $record_time_diff_mins = '';
		}else{
		  $pass_record_time = $record->record_time;
		  $record_time_diff = $c_current_record_time->diffForHumans($c_pass_record_time);
		  $record_time_diff_mins = $c_current_record_time->diffInMinutes($c_pass_record_time);
		}
	  }
	  
	  if($record->pass == '1'){
		$record->pass_flag_name = 'Pass';
		$record->pass_flag_class = 'pass';
	  } else {
		$record->pass_flag_name = 'Invalid';
		$record->pass_flag_class = 'warning';
	  }
	  
	  if($record_time_diff_mins > 20){
		$alert = true;
		$record->pass_flag_class = 'fail';
	  }else {
		$alert = false;
	  }
      ?>
      <tr class="{{ $record->pass_flag_class }}">
        <td x:str="{{ $record->record_time }}">{{ $record->record_time }}</td>
        <td x:str="{{ $record->card_no }}">{{ $record->card_no }}</td>
        <td x:str="{{ $record->emp_no }}">{{ $record->emp_no }}</td>
        <td>{{ $record->emp_name }}</td>
        <td>{{ $record->department }}</td>
        <td>{{ $record->pass_flag_name }}</td>
        <td>{{ $record->controller_name }}</td>
        <td>{{ $record_time_diff }}</td> 
		<td>{!! $alert?'!!!':'' !!}</td>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
</body>
</html>