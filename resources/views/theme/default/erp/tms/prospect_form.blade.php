@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">HRMS Report</div>
        <div class="panel-body">
          <form class="form-horizontal" role="form" method="POST" action="{{ url('/tms/prospect/create') }}">
            {!! csrf_field() !!}
            <div class="row">
              <div class="col-md-3 form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                <label class="col-md-2 control-label">name</label>
                <div class="col-md-10">
                  <input type="text" class="form-control" name="name" value="{{ request('name') }}">
                  @if ($errors->has('date'))
                      <span class="help-block">
                          <strong>{{ $errors->first('date') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              {{$obj}}
              <div class="col-md-2 form-group">
                <label class="col-md-4 control-label">email</label> 
                <div class="col-md-8">
                   <input type="text" class="form-control" name="email" value="{{ request('email') }}">
                </div>
              </div>
              <div class="col-md-2 form-group">
                <label class="col-md-4 control-label">phone</label> 
                <div class="col-md-8">
                   <input type="text" class="form-control" name="phone" value="{{ request('phone') }}">
                </div>
              </div> 
              <div class="col-md-2 form-group">
                <label class="col-md-4 control-label">number</label> 
                <div class="col-md-8">
                   <input type="text" class="form-control" name="number" value="{{ request('number') }}">
                </div>
              </div> 
              <div class="col-md-3 form-group">
                <div class="">
                  <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-search"></i> Save
                  </button>  
                </div>
              </div> 
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_js')
<script>
  $('.datepicker').daterangepicker({
        showDropdowns: true,
        timePicker: true,
        locale: {
          // format: 'DD/MM/YYYY H:mm'
          format: 'DD/MM/YYYY H:mm'
        },  
    },
    function(start, end, label) {
        // var years = moment().diff(start, 'years');
        // console.log("You are " + years + " years old.");
    });
</script>
@endsection