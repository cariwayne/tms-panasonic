@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">Access Controller</div>
        <div class="panel-body">
        @if(isset($post['id']))
            {!! Form::model($post, ['url' => array('tms/accesscontroller', $post['id']), 'method' => 'PUT']) !!}
        @else
            {!! Form::open(['url' => 'tms/accesscontroller']) !!}
        @endif
                <div class="row">
                    <div class="col-md-12 form-group">
                        <label class="col-md-2 control-label">Controller No</label>
                        <div class="col-md-10">
                            {{ Form::text(
                                'controller[controller_no]',
                                null, ['class'=>'form-control'])
                            }}
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <label class="col-md-2 control-label">Controller Name</label>
                        <div class="col-md-10">

                            {{ Form::text(
                                'controller[controller_name]',
                                null, ['class'=>'form-control'])
                            }}

                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <label class="col-md-2 control-label">Controller IP</label>
                        <div class="col-md-10">

                            {{ Form::text(
                                'controller[controller_ip]',
                                null, ['class'=>'form-control'])
                            }}
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <label class="col-md-2 control-label">Attendance Group</label>
                        <div class="col-md-10">
                            @foreach($attendancegroup as $attngroup)
                                <label>
                                    {{ Form::checkbox('attn['.$attngroup->id.']', $attngroup->id) }}
                                    {{ $attngroup->code }}
                                </label>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <label class="col-md-2 control-label"></label>
                        <div class="col-md-10">
                            <input type="submit" class="btn btn-primary" name="submit" value="Save">
                            @if(isset($post['id']))
                                <a class="btn btn-danger pull-right"  onclick="document.getElementById('deleteform').submit();return false;">Delete</a>
                            @endif
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
            @if(isset($post['id']))
                <form id="deleteform" style="display:none;" action="{{ route('tms.accesscontroller.destroy', $post['id']) }}" method="POST">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="submit" value="Delete">
                </form>
            @endif
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_js')
<script>
  $('.datepicker').daterangepicker({
        showDropdowns: true,
        timePicker: true,
        locale: {
          // format: 'DD/MM/YYYY H:mm'
          format: 'DD/MM/YYYY H:mm'
        },
    },
    function(start, end, label) {
        // var years = moment().diff(start, 'years');
        // console.log("You are " + years + " years old.");
    });
</script>
@endsection