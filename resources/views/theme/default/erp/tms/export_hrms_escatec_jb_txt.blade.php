<?php

  header('Content-Description: File Transfer');
  header('Content-Type: application/octet-stream');
  header('Content-Disposition: attachment; filename='.$title.'.txt');
  header('Content-Transfer-Encoding: binary');
  header('Connection: Keep-Alive');
  header('Expires: 0');
  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  header('Pragma: public');
?>@foreach ($tbl_records as $record)
000,{{ sprintf('%010s',$record->card_no) }},{!! ($record->record_state==1)?'1':'0' !!},{{ date('Y/m/d,H:i:s',strtotime(( $record->record_time ))) }}
@endforeach