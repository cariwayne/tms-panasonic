@extends('layouts.app')

@section('content')  


<?php
  $id = 0;
  $email = '';
  $search_email = '';
  $report_type = 0;
  $days = '';
  $time = '';
  $line_type = '';
  $department_type = '';
  if(isset($tms_emails)){
    $id = $tms_emails->id;
    $email = $tms_emails->emp_emails;
    $report_type = $tms_emails->emp_reports;
    $days = $tms_emails->emp_days;
    $time = $tms_emails->emp_times;
  }
?>

@if (Session::has('message_error'))
   <div class="container alert alert-danger">{{ Session::get('message_error') }}</div>
@endif

@if (Session::has('message_error1'))
   <div class="container alert alert-warning">{{ Session::get('message_error1') }}</div>
@endif

@if (Session::has('message_success'))
   <div class="container alert alert-success">{{ Session::get('message_success') }}</div>
@endif

<div class="container" style="background-color: #f2f2f2;">
  <form class = "form-horizontal" role = "form" method = "POST" action = "{{ url('/tms/manageemail') }}">
  {!! csrf_field() !!}
    <div class="container">
      <h2 style="font-weight: bold;">Add new E-mail</h2>
    </div>
    <div class="container">
  		<label for="email" class="col-sm-2 control-label">E-mail:</label>
  		<div class="col-sm-10">
  			{{ Form::text('email', $email, ['required'=>'required'])}}
  		</div>
    </div>
    {{ Form::hidden('id', $id) }}
    <div class="container">
      <label for="reporttype" class="col-sm-2 control-label">Report Type:</label>
  		<div class="col-sm-10" style="padding-top:7px">
        {{ Form::select('reporttype', $reporttype, $report_type) }}
  		</div>
    </div>

    <div class="container">
      <label for="line" class="col-sm-2 control-label">Line:</label>
      <div class="col-sm-10" style="padding-top:7px">
          {{ Form::select('line', $line, $line_type) }}
      </div>
    </div>

    <div class="container">
      <label for="department" class="col-sm-2 control-label">Department:</label>
      <div class="col-sm-10" style="padding-top:7px">
          {{ Form::select('department', $department, $department_type) }}
      </div>
    </div>

    <div class="container" style="padding-top:7px">
      <label for="daytime" class="col-sm-2 control-label">Days:</label>
  		<div class="col-sm-10">
        {{ Form::select('day[]', $day, $days, ['id'=>'day', 'class'=>'form-control', 'multiple'=>'multiple', 'required'=>'required']) }}
  		</div>
    </div>

    <div class="container" style="padding-top:7px">
      <label for="daytime" class="col-sm-2 control-label">Times:</label>
  		<div class="col-sm-10">
        <div style="position: relative">
          <input name="time" id="time" value="{{ $time }}" class="timepicker form-control" type="text" required="required">
        </div>
  		</div>
    </div>

    <div class="container" style="padding-top:20px; padding-bottom:20px;">
      <div class="col-sm-10 pull-right" style="padding-top:7px">
        {{ Form::submit('Add', ['class'=>'btn btn-warning']) }}
        <a class="btn btn-danger" href="{{url('/tms/manageemail')}}">Clear</a>
      </div>
  	</div>
  </form>
</div>

  <div class="container">
    <h2 style="font-weight: bold;">Search E-mail</h2>
    <div class="container">
      <form class = "form-horizontal" role = "form" method = "GET" action = "{{ url('/tms/manageemail') }}">
        {!! csrf_field() !!}
        <span style="font-weight: bold;">
          E-Mail: 
        </span>
        {{ Form::text('search_email', $search_email) }}
        {{ Form::submit('Search', ['class'=>'btn btn-info']) }}
      </form>
    </div>
    <table class="table">
      <thead>
        <tr>
          <th>Id</th>
          <th>E-mail</th>
          <th>Report Type</th>
          <th>Line</th>
          <th>Department</th>
          <th>Day</th>
          <th>Time</th>
          <th>Date Created</th>
          <th>Updated At</th>
          <th>Change</th>
        </tr>
      </thead>
      <tbody>

      <?php
        if(isset($tms_emails)){
        }else{
      ?>
        @foreach ($db_search_email as $dse)
          <tr>
            <td>{{ $dse->id }}</td>
            <td>{{ $dse->emp_emails }}</td>
            <td style="text-align: center">{{ $dse->emp_reports }}</td>
            <td>{{ $dse->emp_line }}</td>
            <td style="text-align: center">{{ $dse->emp_department }}</td>
            <td>{{ $dse->emp_days }}</td>
            <td>{{ $dse->emp_times }}</td>
            <td>{{ $dse->created_at }}</td>
            <td>{{ $dse->updated_at }}</td>
            <td>
              <a class="btn btn-success" href="{{url('/tms/manageemail?tms_email='.$dse->id)}}">Edit</a>
              <a class="btn btn-danger" href="{{url('/tms/manageemail/delete?tms_del_id='.$dse->id)}}">Delete</a>
            </td>
          </tr>
          @endforeach
      <?php
        }
      ?>
      </tbody>
    </table>
  </div>

<div class="container" style="background-color: #f2f2f2;">
  <h2 style="font-weight: bold;">E-mail list</h2>
  <table class="table">
    <thead>
      <tr>
        <th>Id</th>
        <th>E-mail</th>
        <th>Report Type</th>
        <th>Line</th>
        <th>Department</th>
        <th>Day</th>
        <th>Time</th>
        <th>Date Created</th>
        <th>Updated At</th>
        <th>Change</th>
      </tr>
    </thead>
    <tbody>

      @foreach ($users as $user)
        <tr>
          <td>{{ $user->id }}</td>
          <td>{{ $user->emp_emails }}</td>
          <td style="text-align: center">{{ $user->emp_reports }}</td>
          <td>{{ $user->emp_line }}</td>
          <td style="text-align: center">{{ $user->emp_department }}</td>
          <td>{{ $user->emp_days }}</td>
          <td>{{ $user->emp_times }}</td>
          <td>{{ $user->created_at }}</td>
          <td>{{ $user->updated_at }}</td>
          <td>
            <a class="btn btn-success" href="{{url('/tms/manageemail?tms_email='.$user->id)}}">Edit</a>
            <a class="btn btn-danger" href="{{url('/tms/manageemail/delete?tms_del_id='.$user->id)}}">Delete</a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
  {{ $users->links() }}
</div>
@endsection

@section('custom_js')
<script>
  $(document).ready(function(){
    $('#day').select2({
      placeholder : 'Please select days',
      tags: true
    });
  });
  
  $('.timepicker').datetimepicker({
    format: 'HH:mm'
  }); 
</script>
@endsection