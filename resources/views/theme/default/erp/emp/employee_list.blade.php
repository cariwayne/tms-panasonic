@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">Employee Master</div>
        <div class="panel-body">
          <form class="form-horizontal" role="form" method="GET" action="{{ url('/emp/employee') }}">
            {!! csrf_field() !!}
            <div class="row">

              <div class="col-md-2 form-group">
                <label class="col-md-4 control-label">CardNo</label>
                <div class="col-md-8">
                   <input type="text" class="form-control" name="card_no" value="{{ request('card_no') }}">
                </div>
              </div>
              <div class="col-md-2 form-group">
                <label class="col-md-4 control-label">EmpNo</label>
                <div class="col-md-8">
                   <input type="text" class="form-control" name="emp_no" value="{{ request('emp_no') }}">
                </div>
              </div>
              <div class="col-md-5 form-group">
                <label class="col-md-4 control-label">EmpName</label>
                <div class="col-md-8">
                   <input type="text" class="form-control" name="emp_name" value="{{ request('emp_name') }}">
                </div>
              </div>
              <div class="col-md-3 form-group">
                <div class="">
                  <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-search"></i> Search
                  </button>

                </div>
              </div>
            </form>
          </div>
          <div class="table-responsive">
            <table class="table table-bordered table-hover table-condensed">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Employee No</th>
                  <th>Employee Name</th>
                  <th>Card No</th>
                  <th>Email</th>
                  <th>Department</th>
                  <th>Group</th>
                  <th colspan="2">Options</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                @foreach ($tbl_records as $record)
                  <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $record->emp_no }}</td>
                    <td>{{ $record->name }}</td>
                    <td>{{ $record->card_no }}</td>
                    <td>{{ $record->email }}</td>
                    <td>{{ isset($record->department)?$record->department->code:'' }}</td>
                    <td>{{ isset($record->empgroup)?$record->empgroup->code:'' }}</td>
                    <td><a class="btn btn-success" href="{{ url('emp/employee/'.$record->id.'/edit') }}">Edit</a></td>
                    <td>
                      <form class='form-inline form-delete'  action="{{ url('emp/employee/'.$record->id) }}" method="post">
                        {!! method_field('delete') !!}
                        {!! csrf_field() !!}
                        <input data-toggle="modal" data-target="#modalDeleteConfirmation" class="btn btn-danger" type="submit" value="Delete">
                      </form>
                    </td>
                    </td>
                  </tr>
                <?php $i++; ?>
                @endforeach

              </tbody>
            </table>
            @if (COUNT($tbl_records)>0)
              {!!
                  $tbl_records->appends([])->render()
              !!}
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_js')
<script>
  $('.datepicker').daterangepicker({
        showDropdowns: true,
        locale: {
          // format: 'DD/MM/YYYY H:mm'
          format: 'DD/MM/YYYY'
        },
    },
    function(start, end, label) {
        // var years = moment().diff(start, 'years');
        // console.log("You are " + years + " years old.");
    });
</script>
@endsection
