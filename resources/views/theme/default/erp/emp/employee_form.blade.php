@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">User</div>
        <div class="panel-body">
        @if(isset($post['id']))
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/emp/employee/'.$post['id']) }}">
            <input type="hidden" name="_method" value="PUT">
        @else
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/emp/employee/') }}">
            <input type="hidden" name="_method" value="POST">
        @endif
            {!! csrf_field() !!}
            <div class="row">
                <div class="col-md-12 form-group">
                    <label class="col-md-2 control-label">Employee No</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" name="emp_no" value="{{ isset($post['emp_no'])?$post['emp_no']:request('emp_no') }}">
                        @if ($errors->has('emp_no'))
                            <span class="help-block">
                                <strong>{{ $errors->first('emp_no') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <label class="col-md-2 control-label">Employee Name</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" name="name" value="{{ isset($post['name'])?$post['name']:request('name') }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <label class="col-md-2 control-label">Card No</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" name="badgeCard" value="{{ isset($post->badgeCard)?$post->badgeCard->implode('card_no'):request('badgeCard') }}">
                        @if ($errors->has('badgeCard'))
                            <span class="help-block">
                                <strong>{{ $errors->first('badgeCard') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <label class="col-md-2 control-label">Email</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" name="email" value="{{ isset($post['email'])?$post['email']:request('email') }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <label class="col-md-2 control-label">Group</label>
                    <div class="col-md-10">
                        <select class="form-control" name="group_id" >
                          <option value=""></option>
                          <?php $groups = \App\TmsEmpGroup::all(); ?>
                          @foreach ($groups as $group)
                            @if (isset($post->empgroup->id) && $post->empgroup->id == $group->id)
                              <option selected value="{{ $group->id }}">{{ $group->code }}</option>
                            @elseif (request('department') == $group->id)
                              <option selected value="{{ $group->id }}">{{ $group->code }}</option>
                            @else
                              <option value="{{ $group->id }}">{{ $group->code }}</option>
                            @endif
                          @endforeach
                        </select>
                        @if ($errors->has('group'))
                            <span class="help-block">
                                <strong>{{ $errors->first('group') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <label class="col-md-2 control-label">Department</label>
                    <div class="col-md-10">
                        <select class="form-control" name="department_id" >
                          <option value=""></option>
                          <?php $departments = \App\TmsEmpDepartment::all(); ?>
                          @foreach ($departments as $department)
                            @if (isset($post->department->id) && $post->department->id == $department->id)
                              <option selected value="{{ $department->id }}">{{ $department->code }}</option>
                            @elseif (request('department') == $department->id)
                              <option selected value="{{ $department->id }}">{{ $department->code }}</option>
                            @else
                              <option value="{{ $department->id }}">{{ $department->code }}</option>
                            @endif
                          @endforeach
                        </select>
                        @if ($errors->has('department_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('department_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <label class="col-md-2 control-label"></label>
                    <div class="col-md-10">
                        <input type="submit" class="btn btn-primary" name="submit" value="Save">
                    </div>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_js')
<script>
  $('.datepicker').daterangepicker({
        showDropdowns: true,
        timePicker: true,
        locale: {
          // format: 'DD/MM/YYYY H:mm'
          format: 'DD/MM/YYYY H:mm'
        },
    },
    function(start, end, label) {
        // var years = moment().diff(start, 'years');
        // console.log("You are " + years + " years old.");
    });
</script>
@endsection
