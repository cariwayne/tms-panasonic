<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $page_title or "Admin Dashboard" }}</title>

    <!-- Fonts -->

    <link href="{{ asset('assets/style/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type='text/css' />
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css' />

    <!-- Styles -->
    <link href="{{ asset('assets/style/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/widget/daterangepicker/daterangepicker.css')  }}" rel="stylesheet" />
    <link href="{{ asset('assets/style/theme.css') }}" rel="stylesheet" />

    <!-- JavaScripts -->
    <script type="text/javascript" src="{{ asset('assets/widget/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/style/bootstrap/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/widget/moment.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/widget/jquery.pjax.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/widget/nanobar.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/widget/daterangepicker/daterangepicker.js') }}"></script>

    <script src="{{ asset('/assets/widget/highcharts.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('assets/widget/select2/select2.min.js') }}"></script>
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('assets/widget/select2/select2.min.css') }}">


    <link href="{{ asset('assets/widget/bootstraptimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <script src="{{ asset('assets/widget/bootstraptimepicker/bootstrap-datetimepicker.min.js') }}"></script>  


</head>
<body>
  <div id="page-wrapper">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    TMS
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                @if (!Auth::guest())
                  <ul class="nav navbar-nav">
                      <li><a href="{{ url('/home') }}"><i class="fa fa-home fa-fw"></i> Status</a></li>

                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-bolt fa-fw"></i> ESD <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="{{ url('/tms/report/esd') }}">ESD Test Log</a></li>
                              <li><a href="{{ url('/tms/report/esd/status') }}">Emp Test Status</a></li>
                            </ul>
                          </li>

<?php /*
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-calendar-check-o fa-fw"></i> HRMS <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="{{ url('/tms/report/hrms') }}">HRMS Report</a></li>
                              <li><a href="{{ url('/tms/report/hrms/rawdata') }}">HRMS Raw</a></li>

                            </ul>
                          </li>


                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user-plus fa-fw"></i> Employee <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="{{ url('/emp/employee') }}">Employee</a></li>
                        </ul>
                      </li>

                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-sitemap fa-fw"></i> Interplant <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="{{ url('/tms/report/interplant') }}">Interplant Report</a></li>
                        </ul>
                      </li>
*/?>


                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-line-chart fa-fw"></i> Graph <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="{{ url('/tms/report/graph/line') }}"><i class="fa fa-bar-chart fa-fw"></i> Production</a></li>
                          <li><a href="{{ url('/tms/report/graph/department') }}"><i class="fa fa-bar-chart fa-fw"></i> Department</a></li>
                          <li><a href="{{ url('/tms/report/graph/pie') }}"><i class="fa fa-pie-chart fa-fw"></i> Passing rate</a></li>
                          <!--<li><a href="{{ url('/tms/report/graph/search') }}"><i class="fa fa-flag" aria-hidden="true"></i> Report</a></li>-->

                        </ul>
                      </li>                      

                      <li><a href="{{ url('/tms/manageemail') }}"><i class="fa fa-envelope" aria-hidden="true"></i> E-mail Management</a></li>


					            <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench fa-fw"></i> Maintenance <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="{{ url('/tms/importdata') }}">Data Sync</a></li>
                          <li><a href="{{ url('/tms/accesscontroller') }}">Controller</a></li>
                          <li><a href="{{ url('/auth/user') }}">User</a></li>

                        </ul>
                      </li>

                  </ul>
                @endif

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <!--
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="fa fa-bell fa-fw"></i>
                                <span class="label label-warning">10</span>
                                <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                                <li class="divider"></li>
                                <li>
                                    <a class="text-center" href="#">
                                        <strong>See All Alerts</strong>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </li>-->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <div id="main-content">
      <!-- Display Validation Errors -->
      @include('errors.errors')

      @yield('content')
    </div>
    <div id="footer">
        <div class="container">
          <div class="col-md-6">&copy; Caritech Sdn Bhd</div>
          <div class="col-md-6 text-right">Version {{ config('app.version') }}</div>
        </div>
    </div>


    @yield('custom_js')
  </div>

  <div class="modal fade" id="modalDeleteConfirmation" tabindex="-1" role="dialog" aria-labelledby="modalDeleteConfirmation">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="">Delete Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you, want to delete?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger">Delete</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    var options = {
        bg: '#acf',
        id: 'loading-nanobar'
    };
    var nanobar = new Nanobar( options );

    $(document).ready(function(){
      // pjax
      $(document).pjax('a', '#page-wrapper');
      $(document).on('pjax:send', function() {
        nanobar.go(30);
        setTimeout( nanobar.go(60), 1500 );
        setTimeout( nanobar.go(80), 6000 );
      });
      $(document).on('pjax:complete', function() {
        nanobar.go(100);
      });
      // does current browser support PJAX
      if ($.support.pjax) {
        $.pjax.defaults.timeout = 10000; // time in milliseconds
      }
    });
  </script>
  {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
