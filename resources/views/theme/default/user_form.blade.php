@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">User</div>
        <div class="panel-body">
        @if(isset($post['id']))
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/user/'.$post['id']) }}">
            <input type="hidden" name="_method" value="PUT">
        @else
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/user/') }}">
            <input type="hidden" name="_method" value="POST">
        @endif
            {!! csrf_field() !!}
            <div class="row">
                <div class="col-md-12 form-group">
                    <label class="col-md-2 control-label">Name</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" name="name" value="{{ isset($post['name'])?$post['name']:request('name') }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <label class="col-md-2 control-label">E-Mail Address</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" name="email" value="{{ isset($post['email'])?$post['email']:request('email') }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <label class="col-md-2 control-label">Password</label>
                    <div class="col-md-10">
                        <input type="password" class="form-control" name="password"  >
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <label class="col-md-2 control-label">Roles</label>
                    <div class="col-md-10">
                      <?php
                        $user_roles = array();
                        if(isset($post->roles)){
                          foreach($post->roles as $val) {
                            $user_roles[] = $val->id;
                          }
                        }
                      ?>
                      @foreach($roles as $role)

                        <div class="checkbox">
                          <label>
                            @if(in_array($role->id, $user_roles))
                            <input type="checkbox" checked name="roles[]" value="{{ $role->id }}">
                            @else
                            <input type="checkbox" name="roles[]" value="{{ $role->id }}">
                            @endif
                            {{ ucfirst($role->name) }}
                          </label>
                        </div>
                      @endforeach
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <label class="col-md-2 control-label"></label>
                    <div class="col-md-10">
                        <input type="submit" class="btn btn-primary" name="submit" value="Save">
                    </div>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_js')
<script>
  $('.datepicker').daterangepicker({
        showDropdowns: true,
        timePicker: true,
        locale: {
          // format: 'DD/MM/YYYY H:mm'
          format: 'DD/MM/YYYY H:mm'
        },
    },
    function(start, end, label) {
        // var years = moment().diff(start, 'years');
        // console.log("You are " + years + " years old.");
    });
</script>
@endsection
