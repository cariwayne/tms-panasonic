@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">Role</div>
        <div class="panel-body">
        @if(isset($post['id']))
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/role/'.$post['id']) }}">
            <input type="hidden" name="_method" value="PUT">
        @else
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/role/') }}">
            <input type="hidden" name="_method" value="POST">
        @endif
            {!! csrf_field() !!}
            <div class="row">
                <div class="col-md-12 form-group">
                    <label class="col-md-2 control-label">Name</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" name="name" value="{{ isset($post['name'])?$post['name']:request('name') }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <label class="col-md-2 control-label">Display Name</label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" name="display_name" value="{{ isset($post['display_name'])?$post['display_name']:request('display_name') }}">
                        @if ($errors->has('display_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('display_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <label class="col-md-2 control-label">Description</label>
                    <div class="col-md-10">
                        <input type="description" class="form-control" name="description" value="{{ isset($post['description'])?$post['description']:request('description') }}" >
                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <label class="col-md-2 control-label">Permissions</label>
                    <div class="col-md-10">
                      <?php
                        $role_perms = array();
                        if(isset($post->perms)){
                          foreach($post->perms as $val) {
                            $role_perms[] = $val->id;
                          }
                        }
                      ?>
                      @foreach($permissions as $permission)

                        <div class="checkbox">
                          <label>
                            @if(in_array($permission->id, $role_perms))
                            <input type="checkbox" checked name="perms[]" value="{{ $permission->id }}">
                            @else
                            <input type="checkbox" name="perms[]" value="{{ $permission->id }}">
                            @endif
                            {{ ucfirst($permission->display_name) }}
                          </label>
                        </div>
                      @endforeach

                    </div>
                </div>
                <div class="col-md-12 form-group">
                    <label class="col-md-2 control-label"></label>
                    <div class="col-md-10">
                        <input type="submit" class="btn btn-primary" name="submit" value="Save">
                    </div>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom_js')
<script>
  $('.datepicker').daterangepicker({
        showDropdowns: true,
        timePicker: true,
        locale: {
          // format: 'DD/MM/YYYY H:mm'
          format: 'DD/MM/YYYY H:mm'
        },
    },
    function(start, end, label) {
        // var years = moment().diff(start, 'years');
        // console.log("You are " + years + " years old.");
    });
</script>
@endsection
