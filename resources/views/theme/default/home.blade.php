@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" id="block-controller-status">
                <div class="panel-heading">Dashboard
                    <img class="loading" src="{{ asset('assets/img/loading.gif') }}" />
                </div>
                <div class="panel-body">
                    <ul class="list-group">

                    @foreach($controllers as $controller)
                        <li class="list-group-item controller-{{ $controller->id }}">
                            {{ $controller->controller_no }} -
                            {{ $controller->controller_name }}
                            <span class="label label-<?php
                            $current_time = \Carbon\Carbon::now();
                            $last_email_time = \Carbon\Carbon::parse($controller->last_online);
                            // 2 minutes
                            if($current_time->diffInMinutes($last_email_time) > 2){
                                echo 'danger';
                            }else{
                                echo 'success';
                            } ?>" style="float:right">Last Online: {{ $controller->last_online }} </span>
                        </li>
                    @endforeach

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
var sync_ping = setTimeout("func_sync_ping()", 5000);

  function func_sync_ping(){
    $('#block-controller-status .loading').show();
    var url = '{{ url("home/controller_status") }}';
    $.getJSON(url,
      function (data) {
        $('#block-controller-status .loading').hide();
        for (x in data.controllers) {
            var c = data.controllers[x];
            $('#block-controller-status li.controller-'+c.id+' .label').html("Last Online: "+c.last_online);

        }
        sync_ping = setTimeout("func_sync_ping()", 5000);
      })
    return false;
  }

</script>
@endsection
